<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

use App\Services\QrcodeService;
use App\Repositories\RoleRepositoryEloquent;
use App\Repositories\PermissionRepositoryEloquent;
use App\Repositories\RolePermissionRepositoryEloquent;

use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserRoleRepositoryEloquent;
use App\Repositories\UserProfileRepositoryEloquent;
use App\Repositories\UserCheckRepositoryEloquent;
use App\Repositories\UserRecruiterRepositoryEloquent;

use App\Repositories\BlogRepositoryEloquent;
use App\Repositories\BoardRepositoryEloquent;
use App\Repositories\TextRepositoryEloquent;

use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\QrcodesRepositoryEloquent;

use App\Repositories\CountryPropRepositoryEloquent;
use App\Repositories\LocalesRepositoryEloquent;
use App\Repositories\LocalesContentRepositoryEloquent;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        $this->ImportCountryPropTableSeeder();
        $this->ImportLocalesTableSeeder();
        $this->ImportRoleTableSeeder();

        $this->ImportUserTableSeeder();
    }


    public function ImportCountryPropTableSeeder()
    {

        $countryPropRepository = app(CountryPropRepositoryEloquent::class);
        $countryPropData = config('default_country_props');

        foreach ($countryPropData as $countryProp) {

            $item = $countryPropRepository->findByCode($countryProp['code']);
            if ($item->isNotEmpty()) {continue;}

            $insertData = [
                'code' => $countryProp['code'],
                'calling_code' => $countryProp['calling_code'],
                'tld' => $countryProp['tld'],
                'lat' => $countryProp['lat'],
                'lng' => $countryProp['lng']
            ];

            $countryPropRepository->create($insertData);
        } // END foreach
    } // END function


    public function ImportLocalesTableSeeder()
    {

        $localesRepository = app(LocalesRepositoryEloquent::class);
        $localesContentRepository = app(LocalesContentRepositoryEloquent::class);
        $localesData = config('default_locales');
        $localesContentData = config('default_locales_contents');

        foreach ($localesData as $locales) {
            $origLocalesId = $locales['id'];
            $item = $localesRepository->findByCode($locales['code']);
            if ($item->isEmpty()) {
                $insertData = [
                    'code' => $locales['code'],
                    'note' => $locales['note'],
                    'creater_id' => 0
                ];

                $item = $localesRepository->create($insertData);
                $localesId = $item->id;
            } else {
                $localesId = $item->first()->id;
            } // END if

            foreach ($localesContentData as $localesContent) {

                if ($localesContent['locales_id'] != $origLocalesId) {continue;}

                $item = $localesContentRepository->findByLangAndContent($localesContent['lang'], $localesContent['content']);
                if ($item->isEmpty()) {
                    $insertData = [
                        'locales_id' => $localesId,
                        'lang' => $localesContent['lang'],
                        'content' => $localesContent['content'],
                        'creater_id' => 0
                    ];

                    $localesContentRepository->create($insertData);
                } else {
                    $updateData = [
                        'locales_id' => $localesId
                    ];

                    $localesContentRepository->update($updateData, $item->first()->id);
                } // END if
            } // END foreach
        } // END foreach
    } // END function


    public function ImportRoleTableSeeder()
    {
        $roleRepository       = app(RoleRepositoryEloquent::class);
        $permissionRepository = app(PermissionRepositoryEloquent::class);
        $rolePermissionRepository = app(RolePermissionRepositoryEloquent::class);

        $roles = config('role_permissions');

        foreach ($roles as $role) {

            $role_type = $role['type'];
            $role_name = $role['name'];
            $role_description = $role['description'];

            $roleDatum = $roleRepository->findByTypeAndName($role_type, $role_name);

            if ($roleDatum->isEmpty()) {

                $insert_data = [
                    'type' => $role_type,
                    'name' => $role_name,
                    'description' => $role_description,
                    'creater_id' => 0
                ];

                $item = $roleRepository->create($insert_data);

                $roleId = $item->id;
            } else {
                $roleId = $roleDatum->first()->id;
            } // END if


            $permissions = $role['permissions'];

            foreach ($permissions as $permission) {
                $permission_type = $permission['type'];
                $permission_name = $permission['name'];
                $permission_description = $permission['description'];

                $permissionDatum = $permissionRepository->findByTypeAndName($permission_type, $permission_name);

                if ($permissionDatum->isEmpty()) {

                    $insert_data = [
                        'type' => $permission_type,
                        'name' => $permission_name,
                        'description' => $permission_description,
                        'creater_id' => 0
                    ];

                    $item = $permissionRepository->create($insert_data);
                    $permissionId = $item->id;
                } else {
                    $permissionId = $permissionDatum->first()->id;
                } // END if

                $RPDatum = $rolePermissionRepository->findByRoleIdAndPermissionId($roleId, $permissionId);

                if ($RPDatum->isEmpty()) {

                    $insert_data = [
                        'role_id' => $roleId,
                        'permission_id' => $permissionId,
                        'creater_id' => 0
                    ];

                    $rolePermissionRepository->create($insert_data);

                } // END if

            } // END foreach

        } // END foreach

    } // END function RolePermissionTableSeeder


    function ImportUserTableSeeder()
    {

        $roleRepository = app(RoleRepositoryEloquent::class);
        $userRepository = app(UserRepositoryEloquent::class);
        $userRoleRepository = app(UserRoleRepositoryEloquent::class);
        $userProfileRepository = app(UserProfileRepositoryEloquent::class);
        $userCheckRepository = app(UserCheckRepositoryEloquent::class);
        $userRecruiterRepository = app(UserRecruiterRepositoryEloquent::class);

        $blogRepository = app(BlogRepositoryEloquent::class);
        $boardRepository = app(BoardRepositoryEloquent::class);
        $textRepository = app(TextRepositoryEloquent::class);

        $directoryRepository = app(DirectoryRepositoryEloquent::class);
        $qrcodeRepository = app(QrcodesRepositoryEloquent::class);

        $qrcodeService = new QrcodeService;

        $account = 'admin@showhi.co';

        $user = $userRepository->findByAccount($account);

        if ($user->isNotEmpty()) {
            exit();
        } // END if

        $user = $userRepository->init($account, 'showhi!0105*', 0, 'member', true);

        $userRepository->update(['status' => 'enable'], $user->first()->id);

        $roleAdmin = $roleRepository->findByTypeAndName('general', 'admin');
        $roleMember = $roleRepository->findByTypeAndName('general', 'member');

        $roleInsertData = [
            'user_id' => $user->first()->id,
            'role_id' => $roleAdmin->first()->id,
            'creater_id' => 0
        ];

        $userRoleRepository->create($roleInsertData);

        $roleInsertData = [
            'user_id' => $user->first()->id,
            'role_id' => $roleMember->first()->id,
            'creater_id' => 0
        ];

        $userRoleRepository->create($roleInsertData);

        $userProfileRepository->init($user->first()->id, $account, 'Admin', 'Showhi', '886', '227569725');

        foreach(['email','mobile_phone','recruiter'] as $checkOption) {

            $insertData = [
                'user_id' => $user->first()->id,
                'option' => $checkOption,
                'code' => mt_rand(100000, 999999),
                'status' => 'checked'
            ];

            $userCheckRepository->create($insertData);
        }

        $userRecruiterRepository->init($user->first()->id, 0, 0, 'Admin');

        $blog  = $blogRepository->init($user->first()->id, 0);
        $board = $boardRepository->init($blog->first()->id, $user->first()->id);
        $text  = $textRepository->init($board->first()->id, $user->first()->id);

        $noDir  = $directoryRepository->initDefaultDir('blog', $blog->first()->id, $user->first()->id);
        $noPath = $noDir->first()->parent_type . '_' . $blog->first()->id . '/' . $noDir->first()->type;
        Storage::disk('gallery')->makeDirectory($noPath, 0755, true);
        Storage::disk('s3-gallery')->makeDirectory($noPath, 0755, true);

        $coverDir  = $directoryRepository->initDefaultAlbum('blog', $blog->first()->id, 'cover', $user->first()->id);
        $coverPath = $coverDir->first()->parent_type . '_' . $blog->first()->id . '/' . $coverDir->first()->type;
        Storage::disk('gallery')->makeDirectory($coverPath, 0755, true);
        Storage::disk('s3-gallery')->makeDirectory($coverPath, 0755, true);

        $avatarDir  = $directoryRepository->initDefaultAlbum('blog', $blog->first()->id, 'avatar', $user->first()->id);
        $avatarPath = $avatarDir->first()->parent_type . '_' . $blog->first()->id . '/' . $avatarDir->first()->type;
        Storage::disk('gallery')->makeDirectory($avatarPath, 0755, true);
        Storage::disk('s3-gallery')->makeDirectory($avatarPath, 0755, true);

        $now = Carbon::now();
        $expireTime = config('qrcodes.EXPIRETIME');

        $qrcodeFields = [
            'type' => 'user',
            'num_scan' => 0,
            'url' => '',
            'filename' => '',
            'creater_id' => $user->first()->id,
            'created_at' => $now,
            'updated_at' => $now,
            'expire_time' => $expireTime,
            'note' => '',
            'status' => 'enable'
        ];

        $qrcodes = $qrcodeRepository->create($qrcodeFields);

        $retult = $qrcodeService->create($qrcodes->id, $account);

        $updateData = [
            'url' => $retult['url'], 'filename' => $retult['fileName']
        ];

        $qrcodeRepository->update($updateData, $qrcodes->id);

    } // END function AdminUserTableSeeder
}
