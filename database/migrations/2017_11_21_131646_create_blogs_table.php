<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('blogs', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->text('intro');
            $table->char('cover', 50);
            $table->enum('status', ['enable', 'disable', 'delete'])->default('enable');

            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('creater_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index('status');
            $table->index('owner_id');
            $table->index('creater_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
