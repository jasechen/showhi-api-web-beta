<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenderToUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('user_profiles', function (Blueprint $table) {

            $table->enum('gender', ['', 'male', 'female', 'others'])->default('')->after('avatar');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('user_profiles', function (Blueprint $table) {

            $table->dropColumn('gender');

        });
    }
}
