<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('boards', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->enum('parent_type', ['blog', 'group'])->default('blog');
            $table->unsignedBigInteger('parent_id');
            $table->enum('privacy_status', ['public', 'private'])->default('public');
            $table->enum('status', ['enable', 'delete', 'disable'])->default('enable');
            $table->boolean('has_text')->default(true);
            $table->unsignedBigInteger('creater_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique(['parent_type', 'parent_id']);
            $table->index('privacy_status');
            $table->index('status');
            $table->index('creater_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boards');
    }
}
