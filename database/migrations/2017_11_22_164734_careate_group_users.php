<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareateGroupUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('group_users', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('user_id');
            $table->enum('role', ['owner', 'admin', 'member'])->default('owner');
            $table->enum('status', ['init','enable','disable'])->default('init');
            $table->enum('meeting_enable', ['enable', 'disable'])->default('enable')->comment('開啟可預約');
            $table->enum('live_enable', ['enable', 'disable'])->default('enable')->comment('開啟可直撥');
            $table->unsignedBigInteger('recruiter_id')->comment('邀請人id');
            $table->unsignedBigInteger('allower_id')->comment('同意他進來的id');;
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index('group_id');
            $table->index('user_id');
            $table->index('recruiter_id');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_users');
    }
}
