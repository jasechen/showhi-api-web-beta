<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('users', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->char('account', 50);
            $table->char('password', 255);
            $table->enum('status', ['init', 'enable', 'disable', 'delete'])->default('init');
            $table->char('token', 255);

            $table->enum('type', ['school', 'business', 'member'])->default('member');
            $table->boolean('generate_qrcode')->default(false);
            $table->unsignedBigInteger('introducer_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique('account');
            $table->index('status');
            $table->unique('token');
            $table->index('introducer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
