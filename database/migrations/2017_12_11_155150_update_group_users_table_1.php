<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGroupUsersTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('group_users', function (Blueprint $table) {
            $table->enum('role', ['owner', 'admin', 'recruiter', 'editor', 'member'])->default('member')->after('user_id');
            $table->boolean('publish_enable')->default(false)->comment('可發表')->after('status');
            $table->boolean('meeting_enable')->default(false)->comment('可預約')->after('publish_enable');
            $table->boolean('live_enable')->default(false)->comment('可直撥')->after('meeting_enable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('group_users', function (Blueprint $table) {
            $table->dropColumn(['role', 'publish_enable', 'meeting_enable', 'live_enable']);
            //
        });
    }
}
