<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('user_profiles', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('user_id');

            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('nickname', 50);

            $table->char('avatar', 50);
            $table->char('birth', 10);

            $table->char('email', 50);
            $table->char('mobile_country_code', 5);
            $table->char('mobile_phone', 50);
            $table->char('city', 10);

            $table->text('intro');
            $table->text('website');

            $table->text('languages');
            $table->text('interest_items');
            $table->text('countries_visited');
            $table->text('pets');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
