<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('devices', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->char('code', 255);
            $table->enum('os', ['ios', 'andriod', 'macos', 'win', 'others'])->default('others');
            $table->enum('type', ['phone', 'pad', 'desktop', 'notebook', 'others'])->default('others');
            $table->enum('lang', ['zh-tw', 'zh-cn', 'en', 'jp'])->default('en');
            $table->char('token', 255)->nullable();

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->unique('code');
            $table->index('os');
            $table->index('type');
            $table->index('lang');
            $table->unique('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
