<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('sender_id')->comment('發送者user_id');
            $table->unsignedBigInteger('receiver_id')->comment('接收者user_id');
            $table->enum('status', ['init', 'readed', 'delete','disable'])->default('init')->comment('delete 使用者自己刪除, disable 管理員改成看不見');
            $table->text('content')->comment('內容');
            $table->enum('parent_type', ['group', 'blog'])->comment('所屬類型');
            $table->unsignedBigInteger('parent_id')->comment('所屬id');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();

            $table->index('sender_id');
            $table->index('receiver_id');
            $table->index('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
