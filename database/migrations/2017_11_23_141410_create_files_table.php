<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('files', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();

            $table->unsignedBigInteger('directory_id');
            $table->unsignedBigInteger('board_id');

            $table->string('filename');
            $table->string('extension');
            $table->string('mime_type');
            $table->integer('size');

            $table->string('title');
            $table->text('description');
            $table->enum('type', ['image', 'video', 'audio', 'others'])->default('image');

            $table->enum('privacy_status', ['public', 'private'])->default('public');
            $table->enum('status', ['enable', 'delete', 'disable'])->default('enable');

            $table->boolean('is_cover')->default(false);

            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('creater_id');

            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('created_at')->useCurrent();


            $table->index('directory_id');
            $table->index('board_id');
            $table->unique('filename');
            $table->index('privacy_status');
            $table->index('status');
            $table->index('is_cover');
            $table->index('owner_id');
            $table->index('creater_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
