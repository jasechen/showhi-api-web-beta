<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeIndexToBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('boards', function (Blueprint $table) {
            //
            $table->index(['parent_type', 'parent_id']);
            $table->boolean('has_file')->after('directory_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('boards', function (Blueprint $table) {
            //
            $table->dropIndex('boards_parent_type_parent_id_index');
            $table->dropColumn('has_file');
        });
    }
}
