<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToGroupUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('group_users', function (Blueprint $table) {
            $table->dropColumn(['role', 'meeting_enable', 'live_enable']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_users', function (Blueprint $table) {
            $table->enum('meeting_enable', ['enable', 'disable'])->default('enable')->comment('開啟可預約')->after('status');
            $table->enum('live_enable', ['enable', 'disable'])->default('enable')->comment('開啟可直撥')->after('meeting_enable');
            $table->enum('role', ['owner', 'admin', 'member'])->default('owner')->after('user_id');

        });
    }
}
