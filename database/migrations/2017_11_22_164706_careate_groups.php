<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CareateGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('groups', function (Blueprint $table) {
                $table->unsignedBigInteger('id')->primary();
                $table->char('title', 50);
                $table->text('description');
                $table->char('logo', 255)->comment('群組大頭貼');
                $table->char('cover', 255)->comment('背景');
                $table->enum('status', ['enable', 'disable', 'delete'])->default('enable')->comment('disable 給後台用');
                $table->enum('privacy_status', ['public','private'])->default('private')->comment('群組是否公開');;
                $table->enum('type', ['school', 'dept', 'community','business']);
                $table->unsignedBigInteger('owner_id')->comment('擁有者');
                $table->unsignedBigInteger('creater_id')->comment('建立者可以給建立一個群組給別人擁有');
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
                $table->timestamp('created_at')->useCurrent();

                $table->index('owner_id');
                $table->index('creater_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
