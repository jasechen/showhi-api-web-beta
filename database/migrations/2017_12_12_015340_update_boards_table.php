<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->table('boards', function (Blueprint $table) {

            // $table->renameColumn('has_text', 'has_file');
            $table->dropColumn('has_text');
            $table->dropUnique('boards_parent_type_parent_id_unique');

            $table->bigInteger('text_id')->after('status');
            $table->bigInteger('directory_id')->after('text_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql')->table('boards', function (Blueprint $table) {
            // $table->renameColumn('has_file', 'has_text');
            $table->boolean('has_text');
            $table->unique(['parent_type', 'parent_id']);

            $table->dropColumn(['text_id', 'directory_id']);
        });
    }
}
