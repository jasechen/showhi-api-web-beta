<?php

namespace App\Http\Middleware;

use Closure;

use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\GroupUsersRepositoryEloquent;
use App\Repositories\PermissionRepositoryEloquent;

class CheckPermission
{
    use JsonResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissionType, $permissionName)
    {

        $userRepository = app(UserRepositoryEloquent::class);
        $permissionRepository = app(PermissionRepositoryEloquent::class);
        $groupUserRepository = app(GroupUsersRepositoryEloquent::class);

        $token = $request->header('token');

        if (!empty($token)) {

            $user = $userRepository->findByToken($token);

            if ($user->isEmpty()) {
                $code = 404;
                $comment = 'user error';

                $this->failResponse($comment, $code);
            } // END if

            if ($user->first()->status != 'enable') {
                $code = 403;
                $comment = 'user status error';

                $this->failResponse($comment, $code);
            } // END if

            $userRoleIds = [];

            if ($permissionType == config('role.PERMISSION_TYPE_GROUP')) {
                if ($request->method() == 'POST') {
                    $groupId = $request->input($request->route()[1]['as']);
                } else {
                    $groupId = $request->route()[2][$request->route()[1]['as']];
                }

                $groupUser = $groupUserRepository->findByGroupIdAndUserId($groupId, $user->first()->id);

                if ($groupUser->isEmpty()) {
                    $code = 404;
                    $comment = 'group user error';

                    $this->failResponse($comment, $code);
                } // END if

                array_push($userRoleIds, $groupUser->first()->role_id);
            } else {
                $userRoles = $user->first()->Roles;

                if ($userRoles->isEmpty()) {
                    $code = 404;
                    $comment = 'user role error';

                    $this->failResponse($comment, $code);
                } // END if

                foreach ($userRoles->all() as $role) {
                    array_push($userRoleIds, $role->id);
                } // END foreach
            } // END if

            if (empty($userRoleIds)) {
                $code = 400;
                $comment = 'user role empty';

                $this->failResponse($comment, $code);
            } // END if


            $permission = $permissionRepository->findByTypeAndName($permissionType, $permissionName);

            if ($permission->isEmpty()) {
                $code = 404;
                $comment = 'permission error';

                $this->failResponse($comment, $code);
            } // END if

            $permissionRoles = $permission->first()->Roles;

            if ($permissionRoles->isEmpty()) {
                $code = 404;
                $comment = 'permission role error';

                $this->failResponse($comment, $code);
            } // END if


            $permissionRoleIds = [];

            foreach ($permissionRoles->all() as $role) {
                array_push($permissionRoleIds, $role->id);
            } // END foreach

            if (empty($permissionRoleIds)) {
                $code = 400;
                $comment = 'permission role empty';

                $this->failResponse($comment, $code);
            } // END if


            $roleIds = array_intersect($userRoleIds, $permissionRoleIds);

            if (empty($roleIds)) {
                $code = 403;
                $comment = 'user role fail';

                $this->failResponse($comment, $code);
            } // END if

            return $next($request);
        } // END if

    } // END function
} // END class
