<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

use App\Mail\VerifyEmail;
use App\Mail\ForgotPassword;
use App\Traits\ImageToolTrait;
use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserRoleRepositoryEloquent;
use App\Repositories\UserCheckRepositoryEloquent;
use App\Repositories\UserProfileRepositoryEloquent;
use App\Repositories\UserRecruiterRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\BlogRepositoryEloquent;
use App\Repositories\BoardRepositoryEloquent;
use App\Repositories\TextRepositoryEloquent;
use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\FileRepositoryEloquent;
use App\Repositories\RoleRepositoryEloquent;

use Aloha\Twilio\Support\Laravel\Facade as Twilio;

class UsersController extends Controller
{
    use JsonResponseTrait, ImageToolTrait;

    protected $userRepository;
    protected $userRoleRepository;
    protected $userCheckRepository;
    protected $userProfileRepository;
    protected $userRecruiterRepository;
    protected $sessionRepository;
    protected $blogRepository;
    protected $boardRepository;
    protected $textRepository;
    protected $directoryRepository;
    protected $fileRepository;
    protected $roleRepository;


    public function __construct()
    {
        $this->userRepository      = app(UserRepositoryEloquent::class);
        $this->userRoleRepository      = app(UserRoleRepositoryEloquent::class);
        $this->userCheckRepository = app(UserCheckRepositoryEloquent::class);
        $this->userProfileRepository   = app(UserProfileRepositoryEloquent::class);
        $this->userRecruiterRepository = app(UserRecruiterRepositoryEloquent::class);

        $this->sessionRepository  = app(SessionRepositoryEloquent::class);

        $this->blogRepository  = app(BlogRepositoryEloquent::class);
        $this->boardRepository = app(BoardRepositoryEloquent::class);
        $this->textRepository  = app(TextRepositoryEloquent::class);

        $this->directoryRepository = app(DirectoryRepositoryEloquent::class);
        $this->fileRepository = app(FileRepositoryEloquent::class);

        $this->roleRepository = app(RoleRepositoryEloquent::class);
    } // END function


    /**
     * 啟動使用者帳號
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiParam {string}                                  id                      使用者 ID
     * @apiParam {string}                                  check_code                   驗證碼
     *
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     */
    public function verifyEmail(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $checkCode  = $request->input('check_code');

        if (empty($checkCode)) {
            $code = 400;
            $comment = 'code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'init') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if


        $userEmailCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'email');

        if ($userEmailCheck->isEmpty()) {
            $code = 404;
            $comment = 'user check error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userEmailCheck->first()->status != 'init') {
            $code = 403;
            $comment = 'user check status error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userEmailCheck->first()->code != $checkCode) {
            $code = 403;
            $comment = 'user check code error';

            $this->failResponse($comment, $code);
        } // END if


        $this->userCheckRepository->update(['status' => 'checked'], $userEmailCheck->first()->id);

        $userRecruiterCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'recruiter');
        $userMobileCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'mobile_phone');

        if ($userRecruiterCheck->isNotEmpty() and $userRecruiterCheck->first()->status == 'checked') {
            if ($userMobileCheck->isNotEmpty() and $userMobileCheck->first()->status == 'checked') {
                $this->userRepository->update(['status' => 'enable'], $id);
            } // END if
        } // END if

        $resultData = ['session' => $sessionCode];

        $this->successResponse('verify email success', $resultData);
    } // END function


    /**
     * 寄送啟動使用者帳號通知信
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiParam {string}                                  id                      使用者 ID
     *
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     */
    public function sendVerifyEmailNotify(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'init') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if


        $userEmailCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'email');

        if ($userEmailCheck->isEmpty()) {
            $code = 404;
            $comment = 'user check error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userEmailCheck->first()->status != 'init') {
            $code = 403;
            $comment = 'user check status error';

            $this->failResponse($comment, $code);
        } // END if

        $userCheckFields = ['code' => mt_rand(100000, 999999)];
        $this->userCheckRepository->update($userCheckFields, $userEmailCheck->first()->id);


        $to = $user->first()->account;

        $emailFields = [
            'account' => $to,
            'code' => $userCheckFields['code'],
            'link' => 'https://showhi.co/verify/'.$user->first()->id,
        ];

        Mail::to($to)->send(new VerifyEmail($emailFields));


        $resultData = ['session' => $sessionCode];

        $this->successResponse('send verify email success', $resultData);
    } // END function


    /**
     * 驗證使用者手機
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiParam {string}                                  id                      使用者 ID
     * @apiParam {string}                                  check_code                   驗證碼
     *
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     */
    public function verifyMobile(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $checkCode  = $request->input('check_code');

        if (empty($checkCode)) {
            $code = 400;
            $comment = 'code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'init') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if


        $userMobileCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'mobile_phone');

        if ($userMobileCheck->isEmpty()) {
            $code = 404;
            $comment = 'user check error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userMobileCheck->first()->status != 'init') {
            $code = 403;
            $comment = 'user check status error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userMobileCheck->first()->code != $checkCode) {
            $code = 403;
            $comment = 'user check code error';

            $this->failResponse($comment, $code);
        } // END if


        $this->userCheckRepository->update(['status' => 'checked'], $userMobileCheck->first()->id);


        $userRecruiterCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'recruiter');
        $userEmailCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'email');

        if ($userRecruiterCheck->isNotEmpty() and $userRecruiterCheck->first()->status == 'checked') {
            if ($userEmailCheck->isNotEmpty() and $userEmailCheck->first()->status == 'checked') {
                $this->userRepository->update(['status' => 'enable'], $id);
            } // END if
        } // END if

        $resultData = ['session' => $sessionCode];

        $this->successResponse('verify mobile success', $resultData);
    } // END function


    /**
     * 發送驗證使用者手機簡訊
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiParam {string}                                  id                      使用者 ID
     *
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     */
    public function sendVerifyMobileNotify(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'init') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if


        $userMobileCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'mobile_phone');

        if ($userMobileCheck->isEmpty()) {
            $code = 404;
            $comment = 'user check error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userMobileCheck->first()->status != 'init') {
            $code = 403;
            $comment = 'user check status error';

            $this->failResponse($comment, $code);
        } // END if

        $userCheckFields = ['code' => mt_rand(100000, 999999)];
        $this->userCheckRepository->update($userCheckFields, $userMobileCheck->first()->id);


        $profile = $this->userProfileRepository->findByUserId($user->first()->id);
        $mobileCountryCode = $profile->first()->mobile_country_code;
        $mobilePhone = $profile->first()->mobile_phone;

        $phone = '+' . $mobileCountryCode . $mobilePhone;
        $message = 'Phone check code => ' . $userCheckFields['code'];
        Twilio::message($phone, $message);


        $resultData = ['session' => $sessionCode];

        $this->successResponse('send verify mobile success', $resultData);
    } // END function


    /**
     * 啟動使用者帳號
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiParam {string}                                  id                      使用者 ID
     * @apiParam {string}                                  check_code                   驗證碼
     *
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     */
    public function verifyQRCode(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $userId  = $request->input('user_id');

        if (empty($userId)) {
            $code = 400;
            $comment = 'user_id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $recruiter = $this->userRepository->findById($id);

        if ($recruiter->isEmpty()) {
            $code = 404;
            $comment = 'recruiter error';

            $this->failResponse($comment, $code);
        } // END if

        if ($recruiter->first()->status != 'enable') {
            $code = 403;
            $comment = 'recruiter status error';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($recruiter->first()->generate_qrcode)) {
            $code = 403;
            $comment = 'recruiter function error';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($userId);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'init') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if


        $userRecruiter = $this->userRecruiterRepository->findByUserIdAndRecruiterid($user->first()->id, $recruiter->first()->id);

        if ($userRecruiter->isEmpty()) {
            $code = 404;
            $comment = 'user recruiter error';

            $this->failResponse($comment, $code);
        } // END if


        $userRecruiterCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'recruiter');

        if ($userRecruiterCheck->isEmpty()) {
            $code = 404;
            $comment = 'user check error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userRecruiterCheck->first()->status != 'init') {
            $code = 403;
            $comment = 'user check status error';

            $this->failResponse($comment, $code);
        } // END if


        $this->userCheckRepository->update(['status' => 'checked'], $userRecruiterCheck->first()->id);


        $userEmailCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'email');
        $userMobileCheck = $this->userCheckRepository->findByUserIdAndOption($id, 'mobile_phone');

        if ($userEmailCheck->isNotEmpty() and $userEmailCheck->first()->status == 'checked') {
            if ($userMobileCheck->isNotEmpty() and $userMobileCheck->first()->status == 'checked') {
                $this->userRepository->update(['status' => 'enable'], $user->first()->id);
            } // END if
        } // END if

        $resultData = ['session' => $sessionCode];

        $this->successResponse('verify qrcode success', $resultData);
    } // END function


    /**
     *  寄送忘記密碼通知信
     *
     * @header  string              $session
     *
     * @param   string              $account
     *
     * @return  string              $session
     */
    public function sendForgotPasswordNotify(Request $request)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $account  = $request->input('account');

        if (empty($account)) {
            $code = 400;
            $comment = 'account is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (!filter_var($account, FILTER_VALIDATE_EMAIL)) {
            $code = 403;
            $comment = 'account error (email format error)';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findByAccount($account);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'enable') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if


        $userCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'forgot_password');

        if ($userCheck->isEmpty()) {
            $userCheck = $this->userCheckRepository->initOption($user->first()->id, 'forgot_password');
        } else {
            $userCheck = $this->userCheckRepository->resetCodeAndStatus($userCheck->first()->id);
        } // END if


        $to = $user->first()->account;

        $emailFields = [
            'account' => $to,
            'code' => $userCheck->first()->code,
            'link' => 'https://showhi.co/rescue/'.$user->first()->id,
        ];

        Mail::to($to)->send(new ForgotPassword($emailFields));


        $resultData = ['session' => $sessionCode];

        $this->successResponse('send forgot password success', $resultData);
    } // END function


    /**
     * 使用者重設忘記密碼
     *
     * @header  string                  $session
     *
     * @param   string                  $account
     *          string                  $password
     *          string                  $password_repeat
     *          integer                 $check_code
     *
     * @return  string                  $session
     */
    public function resetPassword(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $account  = $request->input('account');

        if (empty($account)) {
            $code = 400;
            $comment = 'account is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (!filter_var($account, FILTER_VALIDATE_EMAIL)) {
            $code = 403;
            $comment = 'account error (email format error)';

            $this->failResponse($comment, $code);
        } // END if

        $password  = $request->input('password');
        $passwordRepeat  = $request->input('password_repeat');

        if (empty($password) or empty($passwordRepeat)) {
            $code = 400;
            $comment = 'password / password_repeat is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 403;
            $comment = 'password\' length < 8';

            $this->failResponse($comment, $code);
        } // END if

        if ($password != $passwordRepeat) {
            $code = 403;
            $comment = 'password / password_repeat is NOT equal';

            $this->failResponse($comment, $code);
        } // END if


        $checkCode  = $request->input('check_code');

        if (empty($checkCode)) {
            $code = 400;
            $comment = 'code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findByAccount($account);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'enable') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->id != $id) {
            $code = 403;
            $comment = 'user account error';

            $this->failResponse($comment, $code);
        } // END if


        $userCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'forgot_password');

        if ($userCheck->isEmpty()) {
            $code = 404;
            $comment = 'user check error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userCheck->first()->status != 'init') {
            $code = 403;
            $comment = 'user check status error';

            $this->failResponse($comment, $code);
        } // END if

        if ($userCheck->first()->code != $checkCode) {
            $code = 403;
            $comment = 'user check code error';

            $this->failResponse($comment, $code);
        } // END if


        $this->userCheckRepository->update(['status' => 'checked'], $userCheck->first()->id);


        $userFields = [
            'password' => password_hash($password, PASSWORD_BCRYPT),
        ];

        $this->userRepository->update($userFields, $user->first()->id);


        $resultData = ['session' => $sessionCode];

        $this->successResponse('reset forgot password success', $resultData);
    } // END function


    /**
     * 使用者改變密碼
     *
     * @header  string                  $session
     *          string                  $token
     *
     * @param   string                  $password
     *          string                  $password_repeat
     *
     * @return  string                  $session
     */
    public function changePassword(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $password  = $request->input('password');
        $passwordRepeat  = $request->input('password_repeat');

        if (empty($password) or empty($passwordRepeat)) {
            $code = 400;
            $comment = 'password / password_repeat is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 403;
            $comment = 'password\' length < 8';

            $this->failResponse($comment, $code);
        } // END if

        if ($password != $passwordRepeat) {
            $code = 403;
            $comment = 'password / password_repeat is NOT equal';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if


        $userFields = [
            'password' => password_hash($password, PASSWORD_BCRYPT),
        ];

        $this->userRepository->update($userFields, $user->first()->id);


        $resultData = ['session' => $sessionCode];

        $this->successResponse('change password success', $resultData);
    } // END function


    /**
     * 改變使用者角色
     *
     * @header  string                  $session
     *          string                  $token
     *
     * @param   string                  $role_id
     *
     * @return  string                  $session
     */
    public function updateRole(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $roleId  = $request->input('role_id');

        if (empty($roleId)) {
            $code = 400;
            $comment = 'role_id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        $role = $this->roleRepository->findById($roleId);

        if ($role->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            $this->failResponse($comment, $code);
        } // END if

        $usrRole = $this->userRoleRepository->findByUserIdAndRoleId($user->first()->id, $roleId);

        if ($usrRole->isNotEmpty()) {
            $code = 404;
            $comment = 'user role error';

            $this->failResponse($comment, $code);
        } // END if


        $updateData = [
            'role_id' => $roleId
        ];

        $this->userRoleRepository->update($updateData, $user->first()->id);


        $resultData = ['session' => $sessionCode];

        $this->successResponse('update role success', $resultData);
    } // END function


    /**
     * 查詢使用者
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiParam {string}    id    使用者 ID
     *
     */
    public function find(Request $request, $id)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        $userEmailCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'email');
        $userMobileCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'mobile_phone');
        $userRecruiterCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'recruiter');

        $blog = $this->blogRepository->findByOwnerId($user->first()->id);


        $userData = $user->first();
        unset($userData->password);
        unset($userData->token);

        $userData->email_status  = $userEmailCheck->isEmpty() ? '' : $userEmailCheck->first()->status;
        $userData->mobile_status = $userMobileCheck->isEmpty() ? '' : $userMobileCheck->first()->status;
        $userData->recruiter_status = $userRecruiterCheck->isEmpty() ? '' : $userRecruiterCheck->first()->status;

        $userData->blog_id = $blog->first()->id;

        $userData = (object) array_map("strval", $userData->toArray());

        $userData->profile = (object) array_map("strval", $user->first()->Profile->toArray());
        $userData->profile->languages = unserialize($userData->profile->languages);
        $userData->profile->interest_items = unserialize($userData->profile->interest_items);
        $userData->profile->countries_visited = unserialize($userData->profile->countries_visited);
        $userData->profile->pets = unserialize($userData->profile->pets);

        $userData->avatar_links = empty($userData->profile->avatar) ? [] : array_map("strval", $this->getImageLinks($userData->profile->avatar));

        $resultData = [
            'session' => $sessionCode,
            'user'  => $userData
        ];

        if (!empty($token)) {
            $resultData['token'] = $token;
        } // END if

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     * 查詢使用者 by token
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     *
     */
    public function findByToken(Request $request, $token)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findByToken($token);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        $userEmailCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'email');
        $userMobileCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'mobile_phone');
        $userRecruiterCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'recruiter');

        $blog = $this->blogRepository->findByOwnerId($user->first()->id);


        $userData = $user->first();
        unset($userData->password);
        unset($userData->token);

        $userData->email_status  = $userEmailCheck->isEmpty() ? '' : $userEmailCheck->first()->status;
        $userData->mobile_status = $userMobileCheck->isEmpty() ? '' : $userMobileCheck->first()->status;
        $userData->recruiter_status = $userRecruiterCheck->isEmpty() ? '' : $userRecruiterCheck->first()->status;

        $userData->blog_id = $blog->first()->id;

        $userData = (object) array_map("strval", $userData->toArray());

        $userData->profile = (object) array_map("strval", $user->first()->Profile->toArray());
        $userData->profile->languages = unserialize($userData->profile->languages);
        $userData->profile->interest_items = unserialize($userData->profile->interest_items);
        $userData->profile->countries_visited = unserialize($userData->profile->countries_visited);
        $userData->profile->pets = unserialize($userData->profile->pets);

        $userData->avatar_links = empty($userData->profile->avatar) ? [] : array_map("strval", $this->getImageLinks($userData->profile->avatar));

        $resultData = [
            'session' => $sessionCode,
            'token' => $token,
            'user'  => $userData
        ];

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     * 查詢使用者角色權限
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiParam {string}    id    使用者 ID
     *
     */
    public function findRole(Request $request, $id)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        $rolesData = [];

        foreach ($user->first()->Roles as $roleKey => $role) {
            unset($role->pivot);
            $rolesData[$roleKey] = (object) array_map('strval', $role->toArray());
            foreach ($role->Permissions as $permissionKey => $permission) {
                unset($permission->pivot);
                $rolesData[$roleKey]->permissions[$permissionKey] = (object) array_map('strval', $permission->toArray());
            } // END foreach
        } // END foreach

        $resultData = [
            'session' => $sessionCode,
            'roles'  => $rolesData
        ];

        if (!empty($token)) {
            $resultData['token'] = $token;
        } // END if

        $this->successResponse('find success', $resultData);
    } // END function


} // END class
