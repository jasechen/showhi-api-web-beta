<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\RoleRepositoryEloquent;
use App\Repositories\PermissionRepositoryEloquent;
use App\Repositories\RolePermissionRepositoryEloquent;

class RolesController extends Controller
{
    use JsonResponseTrait;

    protected $localesRepository, $localesContentRepository;


    public function __construct()
    {
        $this->userRepository    = app(UserRepositoryEloquent::class);
        $this->sessionRepository    = app(SessionRepositoryEloquent::class);

        $this->roleRepository    = app(RoleRepositoryEloquent::class);
        $this->permissionRepository = app(PermissionRepositoryEloquent::class);
        $this->rolePermissionRepository = app(RolePermissionRepositoryEloquent::class);
    } // END function


    /**
     * Delete the role By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function delete(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if

        $loginUser = $this->userRepository->findByToken($token);

        $role = $this->roleRepository->findById($id);

        if ($role->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            $this->failResponse($comment, $code);
        } // END if


        $result = $this->roleRepository->delete($id);

        if (empty($result)) {
            $code = 422;
            $comment = 'delete role fail';

            $this->failResponse($comment, $code);
        } // END if


        $rolePermissions = $this->rolePermissionRepository->findByRoleId($id);

        if ($rolePermissions->isNotEmpty()) {
            $deletePermissionIds = [];
            foreach ($rolePermissions as $rolePermission) {
                array_push($deletePermissionIds, $rolePermission->permission_id);
            } // END foreach

            $result = $this->rolePermissionRepository->deleteWhere(['role_id' => $id]);

            if (empty($result)) {
                $code = 422;
                $comment = 'delete permission fail';

                $this->failResponse($comment, $code);
            } // END if

            if (!empty($deletePermissionIds)) {
                foreach ($deletePermissionIds as $deletePermissionId) {

                    $deleteRolePermissions = $this->rolePermissionRepository->findByPermissionId($deletePermissionId);

                    if ($deleteRolePermissions->isEmpty()) {
                        $this->permissionRepository->delete($deletePermissionId);
                    } // END if
                } // END foreach
            } // END if

        } // END if


        $resultData = ['session' => $sessionCode, 'role_id' => strval($id)];

        $this->successResponse('delete success', $resultData);
    } // END function


    /**
     * Update the role By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function update(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $loginUser = $this->userRepository->findByToken($token);

        $role = $this->roleRepository->findById($id);

        if ($role->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            $this->failResponse($comment, $code);
        } // END if


        $roleType = $request->input('role_type');
        $roleName = $request->input('role_name');
        $roleDecription = $request->input('role_description');

        if (!empty($roleType) AND !empty($roleName)) {
            $roleCheck = $this->roleRepository->findByTypeAndName($roleType, $roleName);
            if ($roleCheck->isNotEmpty() AND $roleCheck->first()->id != $id) {
                $code = 403;
                $comment = 'role type name error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $updateData = [];

        if (!empty($roleType)) {
            $typeValidator = Validator::make(['type' => $roleType],
                ['type' => ['in:' . implode(',', config('role.ROLE_TYPES'))]]
            );

            if ($typeValidator->fails()) {
                $code = 403;
                $comment = 'role type error';

                $this->failResponse($comment, $code);
            } // END if

            $updateData['type'] = $roleType;
        } // END if

        if (!empty($roleName)) {
            $updateData['name'] = $roleName;
        } // END if

        if (!empty($roleDecription)) {
            $updateData['description'] = $roleDecription;
        } // END if

        if (!empty($updateData)) {
            $result = $this->roleRepository->update($updateData, $id);

            if (empty($result)) {
                $code = 422;
                $comment = 'update role error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $permissions = $request->input('permissions');

        if (!empty($permissions)) {

            $deletePermissionIds = [];
            $rolePermissions = $this->rolePermissionRepository->findByRoleId($id);
            if ($rolePermissions->isNotEmpty()) {
                foreach ($rolePermissions as $rolePermission) {
                    array_push($deletePermissionIds, $rolePermission->permission_id);
                } // END foreach
            } // END if

            $result = $this->rolePermissionRepository->deleteWhere(['role_id' => $id]);

            if (empty($result)) {
                $code = 422;
                $comment = 'delete role permissions error';

                $this->failResponse($comment, $code);
            } // END if

            if (!empty($deletePermissionIds)) {
                foreach ($deletePermissionIds as $deletePermissionId) {

                    $deleteRolePermissions = $this->rolePermissionRepository->findByPermissionId($deletePermissionId);

                    if ($deleteRolePermissions->isEmpty()) {
                        $this->permissionRepository->delete($deletePermissionId);
                    } // END if
                } // END foreach
            } // END if

            $createPermissionEmptyTypes = [];
            $createPermissionFailNames = $createRolePermissionFailNames = [];
            $createPermissionSuccessIds = [];
            foreach ($permissions as $key => $inputPermission) {
                $permissionType = $inputPermission['permission_type'];
                $permissionName = $inputPermission['permission_name'];
                $permissionDescription = $inputPermission['permission_description'];

                $typeValidator = Validator::make(['type' => $permissionType],
                    ['type' => ['in:' . implode(',', config('role.PERMISSION_TYPES'))]]
                );

                if ($typeValidator->fails()) {
                    array_push($createPermissionEmptyTypes, $permissionType);
                    continue;
                } // END if

                $permission = $this->permissionRepository->findByTypeAndName($permissionType, $permissionName);

                if ($permission->isEmpty()) {
                    $insertData = [
                        'type' => $permissionType,
                        'name' => $permissionName,
                        'description' => $permissionDescription,
                        'creater_id' => $loginUser->first()->id
                    ];

                    $result = $this->permissionRepository->create($insertData);

                    if (empty($result)) {
                        array_push($createPermissionFailNames, $permissionType."-".$permissionName);
                    } // END if

                    $permission = $this->permissionRepository->findByTypeAndName($permissionType, $permissionName);
                } // END if

                $insertData = [
                    'role_id' => $id,
                    'permission_id' => $permission->first()->id,
                    'creater_id' => $loginUser->first()->id
                ];

                $result = $this->rolePermissionRepository->create($insertData);

                if (empty($result)) {
                    array_push($createRolePermissionFailNames, $id."-".$permission->first()->id);
                } else {
                    array_push($createPermissionSuccessIds, $permission->first()->id);
                } // END if

            } // END foreach

            if (!empty($createPermissionEmptyTypes)) {
                $code = 400;
                $comment = 'permission type empty (#'.implode(', ', $createPermissionEmptyTypes).')';

                $this->failResponse($comment, $code);
            } // END if

            if (!empty($createPermissionFailNames)) {
                $code = 422;
                $comment = 'create permission error (#'.implode(', ', $createPermissionFailNames).')';

                $this->failResponse($comment, $code);
            } // END if

            if (!empty($createRolePermissionFailNames)) {
                $code = 422;
                $comment = 'create role permission error (#'.implode(', ', $createRolePermissionFailNames).')';

                $this->failResponse($comment, $code);
            } // END if


        } // END if


        $resultData = ['session' => $sessionCode, 'role_id' => strval($id)];

        if (!empty($createPermissionSuccessIds)) {
            $resultData['permission_ids'] = array_map('strval', $createPermissionSuccessIds);
        } // END if

        $this->successResponse('update success', $resultData);
    } // END function


    /**
     * Create the role By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function create(Request $request)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $loginUser = $this->userRepository->findByToken($token);

        $roleType = $request->input('role_type', config('role.DEFAULT_ROLE_TYPE'));
        $roleName = $request->input('role_name');
        $roleDescription = $request->input('role_description');

        if (empty($roleType)) {
            $code = 400;
            $comment = 'role type is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($roleName)) {
            $code = 400;
            $comment = 'role name is empty';

            $this->failResponse($comment, $code);
        } // END if

        $permissions = $request->input('permissions');
        if (!empty($permissions)) {
            foreach ($permissions as $key => $inputPermission) {
                $permissionType = $inputPermission['permission_type'];
                $permissionName = $inputPermission['permission_name'];

                if (empty($permissionType)) {
                    $code = 400;
                    $comment = 'permission type #'.$key.' is empty';

                    $this->failResponse($comment, $code);
                } // END if

                if (empty($permissionName)) {
                    $code = 400;
                    $comment = 'permission name #'.$key.' is empty';

                    $this->failResponse($comment, $code);
                } // END if

            } // END foreach
        } // END if


        $typeValidator = Validator::make(['type' => $roleType],
            ['type' => ['in:' . implode(',', config('role.ROLE_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 403;
            $comment = 'role type error';

            $this->failResponse($comment, $code);
        } // END if


        $role = $this->roleRepository->findByTypeAndName($roleType, $roleName);

        if ($role->isEmpty()) {
            $insertData = [
                'type' => $roleType,
                'name' => $roleName,
                'description' => $roleDescription,
                'creater_id' => $loginUser->first()->id
            ];

            $result = $this->roleRepository->create($insertData);

            if (empty($result)) {
                $code = 422;
                $comment = 'create role error';

                $this->failResponse($comment, $code);
            } // END if

            $role = $this->roleRepository->findByTypeAndName($roleType, $roleName);
        } // END if

        if (!empty($permissions)) {
            $createPermissionEmptyTypes = [];
            $createPermissionFailNames = $createRolePermissionFailNames = [];
            $createPermissionSuccessIds = [];
            foreach ($permissions as $key => $inputPermission) {
                $permissionType = empty($inputPermission['permission_type']) ? config('role.DEFAULT_PERMISSION_TYPE') : $inputPermission['permission_type'];
                $permissionName = $inputPermission['permission_name'];
                $permissionDescription = $inputPermission['permission_description'];

                $typeValidator = Validator::make(['type' => $permissionType],
                    ['type' => ['in:' . implode(',', config('role.PERMISSION_TYPES'))]]
                );

                if ($typeValidator->fails()) {
                    array_push($createPermissionEmptyTypes, $permissionType);
                    continue;
                } // END if

                $permission = $this->permissionRepository->findByTypeAndName($permissionType, $permissionName);

                if ($permission->isEmpty()) {
                    $insertData = [
                        'type' => $permissionType,
                        'name' => $permissionName,
                        'description' => $permissionDescription,
                        'creater_id'  => $loginUser->first()->id
                    ];

                    $result = $this->permissionRepository->create($insertData);

                    if (empty($result)) {
                        array_push($createPermissionFailNames, $permissionType."-".$permissionName);
                    } // END if

                    $permission = $this->permissionRepository->findByTypeAndName($permissionType, $permissionName);
                } // END if

                $insertData = [
                    'role_id' => $role->first()->id,
                    'permission_id' => $permission->first()->id,
                    'creater_id' => $loginUser->first()->id
                ];

                $result = $this->rolePermissionRepository->create($insertData);

                if (empty($result)) {
                    array_push($createRolePermissionFailNames, $role->first()->id."-".$permission->first()->id);
                } else {
                    array_push($createPermissionSuccessIds, $permission->first()->id);
                } // END if

            } // END foreach

            if (!empty($createPermissionEmptyTypes)) {
                $code = 400;
                $comment = 'permission type empty (#'.implode(', ', $createPermissionEmptyTypes).')';

                $this->failResponse($comment, $code);
            } // END if

            if (!empty($createPermissionFailNames)) {
                $code = 422;
                $comment = 'create permission error (#'.implode(', ', $createPermissionFailNames).')';

                $this->failResponse($comment, $code);
            } // END if

            if (!empty($createRolePermissionFailNames)) {
                $code = 422;
                $comment = 'create role permission error (#'.implode(', ', $createRolePermissionFailNames).')';

                $this->failResponse($comment, $code);
            } // END if

        } // END if


        $resultData = ['session' => $sessionCode, 'role_id' => strval($role->first()->id)];

        if (!empty($createPermissionSuccessIds)) {
            $resultData['permission_ids'] = array_map('strval', $createPermissionSuccessIds);
        } // END if

        $this->successResponse('create success', $resultData);
    } // END function


    /**
     * Find the role By type and name.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $type
     * @param  string  $name
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function findByTypeAndName(Request $request, $type, $name)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($type)) {
            $code = 400;
            $comment = 'type is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($name)) {
            $code = 400;
            $comment = 'name is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $typeValidator = Validator::make(['type' => $type],
            ['type' => ['in:' . implode(',', config('role.ROLE_TYPES'))]]
        );

        if ($typeValidator->fails()) {
            $code = 403;
            $comment = 'role type error';

            $this->failResponse($comment, $code);
        } // END if


        $role = $this->roleRepository->findByTypeAndName($type, $name);

        if ($role->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            $this->failResponse($comment, $code);
        } // END if


        $roleData = (object) array_map("strval", $role->first()->toArray());

        foreach ($role->first()->Permissions as $permission) {
            unset($permission->pivot);
            $roleData->permissions[] = array_map('strval', $permission->toArray());
        } // ENd foreach

        $resultData = ['session' => $sessionCode, 'role' => $roleData];

        $this->successResponse('find success', $resultData);
    } // END function



    /**
     * Find the role By name
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $name
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function findByName(Request $request, $name)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($name)) {
            $code = 400;
            $comment = 'name is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $roles = $this->roleRepository->findByName($name);

        if ($roles->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            $this->failResponse($comment, $code);
        } // END if

        $rolesData = [];

        foreach ($roles->all() as $role) {

            $roleData = (object) array_map("strval", $role->first()->toArray());

            foreach ($role->first()->Permissions as $permission) {
                unset($permission->pivot);
                $roleData->permissions[] = array_map('strval', $permission->toArray());
            } // ENd foreach

            array_push($rolesData, $roleData);
        } // END foreach

        $resultData = ['session' => $sessionCode, 'roles' => $rolesData];

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     * Find the role By type
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $type
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function findByType(Request $request, $type)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($type)) {
            $code = 400;
            $comment = 'type is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $roles = $this->roleRepository->findByType($type);

        if ($roles->isEmpty()) {
            $code = 404;
            $comment = 'role error';

            $this->failResponse($comment, $code);
        } // END if

        $rolesData = [];

        foreach ($roles->all() as $role) {

            $roleData = (object) array_map("strval", $role->first()->toArray());

            foreach ($role->first()->Permissions as $permission) {
                unset($permission->pivot);
                $roleData->permissions[] = array_map('strval', $permission->toArray());
            } // ENd foreach

            array_push($rolesData, $roleData);
        } // END foreach

        $resultData = ['session' => $sessionCode, 'roles' => $rolesData];

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     * Find the role By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function find(Request $request, $id)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $role = $this->roleRepository->findById($id);

        if ($role->isEmpty()) {
            $code = 404;
            $comment = 'id error';

            $this->failResponse($comment, $code);
        } // END if


        $roleData = (object) array_map("strval", $role->first()->toArray());

        foreach ($role->first()->Permissions as $permission) {
            unset($permission->pivot);
            $roleData->permissions[] = array_map('strval', $permission->toArray());
        } // ENd foreach

        $resultData = ['session' => $sessionCode, 'role' => $roleData];

        $this->successResponse('find success', $resultData);
    } // END function


} // END class
