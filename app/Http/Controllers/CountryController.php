<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\JsonResponseTrait;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\CountryPropRepositoryEloquent;
use App\Repositories\LocalesRepositoryEloquent;

class CountryController extends Controller
{
    use JsonResponseTrait;

    protected $sessionRepository;


    public function __construct()
    {
        $this->sessionRepository = app(SessionRepositoryEloquent::class);
        $this->countryPropRepository = app(CountryPropRepositoryEloquent::class);
        $this->localesRepository = app(LocalesRepositoryEloquent::class);
    } // END function


    /**
     *
     */
    public function findByLang(Request $request, $lang)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if

        if (!in_array($lang, config('locales.CURRENT_LOCALES_LANGS'))) {
            $code = 404;
            $comment = 'lang error';

            $this->failResponse($comment, $code);
        } // END if


        $countries = config('country');

        $countryNames = [];

        foreach ($countries as $countryCode) {
            $locales_code = 'country_name_' . $countryCode;

            $isCodeLangExist = $this->localesRepository->isCodeLangExist($locales_code, $lang);
            if (empty($isCodeLangExist)) {continue;}

            $localesContent = $this->localesRepository->findByCodeAndLang($locales_code, $lang);
            $countryName = $localesContent->first()->content;
            if (empty($countryName)) {continue;}

            array_push($countryNames, [
                'code' => $countryCode,
                'country_name' => $countryName
            ]);
        } // END foreach

        if (empty($countryNames)) {
            $code = 400;
            $comment = 'country name empty';

            $this->failResponse($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'countries' => $countryNames];

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     *
     */
    public function findCallingCodesByLang(Request $request, $lang)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if

        if (!in_array($lang, config('locales.CURRENT_LOCALES_LANGS'))) {
            $code = 404;
            $comment = 'lang error';

            $this->failResponse($comment, $code);
        } // END if


        $countryCallings = config('country_callings');

        $callingCodes = [];

        foreach ($countryCallings as $countryCode => $callingCode) {
            $locales_code = 'country_name_' . $countryCode;

            $isCodeLangExist = $this->localesRepository->isCodeLangExist($locales_code, $lang);
            if (empty($isCodeLangExist)) {continue;}

            $localesContent = $this->localesRepository->findByCodeAndLang($locales_code, $lang);
            $countryName = $localesContent->first()->content;
            if (empty($countryName)) {continue;}

            $callingCode = [
                'code' => $countryCode,
                'country_name' => $countryName,
                'calling_code' => $callingCode
            ];

            array_push($callingCodes, $callingCode);
        } // END foreach

        if (empty($callingCodes)) {
            $code = 400;
            $comment = 'calling code empty';

            $this->failResponse($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'callings' => $callingCodes];

        $this->successResponse('find success', $resultData);
    } // END function

} // END class
