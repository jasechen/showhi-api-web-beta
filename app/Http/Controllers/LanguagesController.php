<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\JsonResponseTrait;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\LocalesRepositoryEloquent;

class LanguagesController extends Controller
{
    use JsonResponseTrait;

    protected $sessionRepository;
    protected $localesRepository;


    public function __construct()
    {
        $this->sessionRepository = app(SessionRepositoryEloquent::class);
        $this->localesRepository = app(LocalesRepositoryEloquent::class);
    } // END function


    /**
     *
     */
    public function findByLang(Request $request, $lang)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if

        if (!in_array($lang, config('locales.CURRENT_LOCALES_LANGS'))) {
            $code = 404;
            $comment = 'lang error';

            $this->failResponse($comment, $code);
        } // END if


        $languages = [];
        foreach (config('languages') as $languageCode) {
           $locales_language = 'language_name_' . $languageCode;

            $isCodeLangExist = $this->localesRepository->isCodeLangExist($locales_language, $lang);
            if (empty($isCodeLangExist)) {continue;}

            $localesContent = $this->localesRepository->findByCodeAndLang($locales_language, $lang);
            $languageName = $localesContent->first()->content;
            if (empty($languageName)) {continue;}

            $language = [
                'code' => $languageCode,
                'language_name' => $languageName
            ];

            array_push($languages, $language);
        } // END foreach


        if (empty($languages)) {
            $code = 400;
            $comment = 'language empty';

            $this->failResponse($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'languages' => $languages];

        $this->successResponse('find success', $resultData);
    } // END function

} // END class
