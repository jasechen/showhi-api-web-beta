<?php namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Services\BaseFuncService;
use App\Services\GroupService;
use App\Traits\ImageToolTrait;
use App\Traits\JsonResponseTrait;

use App\Repositories\GroupsRepositoryEloquent;
use App\Repositories\GroupUsersRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\FileRepositoryEloquent;
use App\Repositories\RoleRepositoryEloquent;

class GroupsController extends Controller {
    use JsonResponseTrait ,ImageToolTrait;
    protected $userRepository;
    protected $groupsRepository;
    protected $groupUsersRepository;

    public function __construct(){
        $this->groupsRepository  = app(GroupsRepositoryEloquent::class);
        $this->groupUsersRepository  = app(GroupUsersRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
        $this->baseFuncService = new BaseFuncService;
        $this->groupService = new GroupService;
        $this->directoryRepository = app(DirectoryRepositoryEloquent::class);
        $this->fileRepository = app(FileRepositoryEloquent::class);
        $this->roleRepository = app(RoleRepositoryEloquent::class);
        $this->tmpIdArr =['id','owner_id','creater_id'];
    }

    /** 建立group
     *  Desc:建立group，要學校 or 仲介才能建立
     *  參考輸入範例
     * /groups/create
        {
            "type":"school",
            "title":"john school",
            "keywords":"john,大學",
            "description":"約翰大學"
        }
     */
    public function create(Request $request){
        //type 不能為空
        $type = $request->input('type');
        $code ='400';
        $data ='';
        if($type == null){
            $comment ='type is empty';
            $this->failResponse($comment,$code,$data);
        }
        //title 不能為空
        $title = $request->input('title');
        $code ='400';
        if($title == null){
            $comment ='title is empty';
            $this->failResponse($comment,$code);
        }
        $keywords = $request->input('keywords');
        //description
        $description = $request->input('description');


        if($description == null)
        $description = '';
        if($keywords == null)
        $keywords = '';
        //end input
        //own check
        $userArr = $this->baseFuncService->getSessionUserArr($request);
        //end check own
        if($userArr['0']['type'] == 'businese' && $type != 'business'){
            $code = 401;
            $comment = 'type no permissions';
            $this->failResponse($comment,$code);
        }
        unset($userArr['password']);

        //存進資料庫
            $groupsFields = [
                                'title' => $title,
                                'type' => $type,
                                'avatar' => '',
                                'cover' => '',
                                'keywords' => $keywords,
                                'description' => $description,
                                'owner_id' => $userArr['0']['id'],
                                'creater_id' => $userArr['0']['id'],
                            ];
            $groups = $this->groupsRepository->create($groupsFields);
            $groups = $groups->toArray();

            $tmpArr = $this->roleRepository->findByTypeAndName('group','owner');
            $role_id = $tmpArr->first()->id;
            $tmpArr =null;
            if(count($groups)  >1){
                $groupUsersFields = [
                                'group_id' => $groups['id'],
                                'user_id' => $userArr['0']['id'],
                                'role' => 'owner',
                                'role_id' => $role_id,
                                'status' => 'enable',
                                'meeting_enable' => true,
                                'live_enable' => true,
                                'recruiter_id' => $groups['id'],
                                'allower_id' => $groups['id'],
                            ];

                $groupUsers = $this->groupUsersRepository->create($groupUsersFields);
                $groupUsers = $groupUsers->toArray();

                $noDir  = $this->directoryRepository->initDefaultDir('group', $groups['id'], $userArr['0']['id']);
                $noPath = $noDir->first()->parent_type . '_' . $groups['id'] . '/' . $noDir->first()->type;
                Storage::disk('gallery')->makeDirectory($noPath, 0755, true);

                $coverDir  = $this->directoryRepository->initDefaultAlbum('group', $groups['id'], 'cover', $userArr['0']['id']);
                $coverPath = $coverDir->first()->parent_type . '_' . $groups['id'] . '/' . $coverDir->first()->type;
                Storage::disk('gallery')->makeDirectory($coverPath, 0755, true);

                $avatarDir  = $this->directoryRepository->initDefaultAlbum('group', $groups['id'], 'avatar', $userArr['0']['id']);
                $avatarPath = $avatarDir->first()->parent_type . '_' . $groups['id'] . '/' . $avatarDir->first()->type;
                Storage::disk('gallery')->makeDirectory($avatarPath, 0755, true);

                //qrcodes
                $qrcodesPath = $avatarDir->first()->parent_type . '_' . $groups['id'];
                Storage::disk('qrcode')->makeDirectory($qrcodesPath, 0755, true);

            }
            //回傳資料
            $data = [
                'id' =>(string) $groups['id'],
                'type' =>$groups['type'],
                'title' =>$groups['title'],
                'keywords' =>$groups['keywords'],
                'description' => $groups['description']
            ];
            $comment ='return create group info';
            $this->successResponse($comment,$data);

    }

    /** 修改 group
     *  Desc:修改group，admin,owner 才能修改
     *  參考輸入範例
     * /groups/update/{id}
        {
            "title":"john test1",
            "description":"test",
            "status":"enable",
            "privacy_status":"public",
            "type":"school",
            "owner_id":"14380081212428288"
        }

     */
    public function update($id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        //get update filed
        $filedArr = $request->all();
        if(count($filedArr) == 0){
            $comment ='no update filed data';
            $this->failResponse($comment,$code);
        }

        $tmpList ="enable,disable";
        if(array_key_exists('status',$filedArr))
        if(!$this->baseFuncService->checkTypeFiled($filedArr['status'],$tmpList)){
            $code ='403';
            $comment ='status is error';
            $this->failResponse($comment,$code);
        }

        $tmpList ="public,private";
        if(array_key_exists('privacy_status',$filedArr))
        if(!$this->baseFuncService->checkTypeFiled($filedArr['privacy_status'],$tmpList)){
            $code ='401';
            $comment ='privacy status is error';
            $this->failResponse($comment,$code);
        }

        $tmpList ="school,dept,community,business";
        if(array_key_exists('type',$filedArr))
        if(!$this->baseFuncService->checkTypeFiled($filedArr['type'],$tmpList)){
            $code ='400';
            $comment ='type is error';
            $this->failResponse($comment,$code);
        }
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        //checkGroupUser 改寫成抓role_id

        $groupOwnId = $this->groupService->getGroupOwnerId($id);
        //商業者不能修改成學校
        $user = $this->userRepository->findWhere([
            'id' =>$groupOwnId
            ]);
        $userArr = $user->toArray();
        if($userArr['0']['type'] == 'businese' && $filedArr['type'] != 'business'){
            $code = 401;
            $comment = 'type no permissions';
            $this->failResponse($comment,$code);
        }
        if($id !=null){
            $groups = $this->groupsRepository->update(
                $filedArr,
                $id
            );
            $data='';
            $comment ='update down';
            $this->successResponse($comment,$data);
        }

    }

    /** 列出所有開放 group
     *  Desc:列出所有開放 group
     *  參考輸入範例
     * /groups/list
        {
        }
     */
    public function list(Request $request){
        $this->changeRead();
        $code ='400';
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //先抓出要回傳的資料
        $groups = $this->groupsRepository->findWhere([
            'status' =>'enable',
            'privacy_status' =>'public'
            ]);
        $groupsArr = $groups->toArray();

        foreach ($groupsArr as $key =>$value ){
            $groupsArr[$key]['cover_links'] = empty($groupsArr[$key]['cover']) ? [] : $this->getImageLinks($groupsArr[$key]['cover']);
            $groupsArr[$key]['avatar_links'] = empty($groupsArr[$key]['avatar']) ? [] : $this->getImageLinks($groupsArr[$key]['avatar']);
        }
            $comment ='return grouplist info';
            $groupsArr = $this->baseFuncService->idToString($groupsArr,$this->tmpIdArr);
            $this->successResponse($comment,$groupsArr);
    }
    /** 列出所有我擁有的 group
     *  Desc:列出所有我擁有的 group
     *  參考輸入範例
     * /groups/MyOwnlist
        {
        }
     */
    public function MyOwnList(Request $request){
        $this->changeRead();
        $code ='400';
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession


        //先抓出要回傳的資料
        $groups = $this->groupsRepository->findWhere([
            ['status','!=','delete'],
            'owner_id' =>$userId
            ]);
        $groupsArr = $groups->toArray();

        //取得圖片連結
        foreach ($groupsArr as $key =>$value ){
            $groupsArr[$key]['cover_links'] = empty($groupsArr[$key]['cover']) ? [] : $this->getImageLinks($groupsArr[$key]['cover']);
            $groupsArr[$key]['avatar_links'] = empty($groupsArr[$key]['avatar']) ? [] : $this->getImageLinks($groupsArr[$key]['avatar']);
        }
        //end 取得圖片連結
        //end
            $comment ='return grouplist info';
            $groupsArr = $this->baseFuncService->idToString($groupsArr,$this->tmpIdArr);
            $this->successResponse($comment,$groupsArr);
    }
    /** 列出所有我加入的 group
     *  Desc:列出所有我加入的 group
     *  參考輸入範例
     * /groups/MyAddList
        {
        }
     */
    public function MyAddList(Request $request){
        $this->changeRead();
        $code ='400';
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //先抓出要回傳的資料
        $groups = $this->groupsRepository->findWhere([
            ['status','!=','delete']
            ]);
        $groupsArr = $groups->toArray();


        $i=0;
        foreach($groupsArr as $key=> $value){
            $userArr = $this->groupService->getGroupUser($value['id'],$userId);
            if(count($userArr) ==1){
                $newGroupsArr[$i] = $groupsArr[$i];
                $newGroupsArr[$i]['cover_links'] = empty($newGroupsArr[$i]['cover']) ? [] : $this->getImageLinks($newGroupsArr[$i]['cover']);
                $newGroupsArr[$i]['avatar_links'] = empty($newGroupsArr[$i]['avatar']) ? [] : $this->getImageLinks($newGroupsArr[$i]['avatar']);
                $i++;
            }
        }
        $i=0;

        if(empty($newGroupsArr)){
            $newGroupsArr ='';
        }

            $comment ='return grouplist info';
            $newGroupsArr = $this->baseFuncService->idToString($newGroupsArr,$this->tmpIdArr);
            $this->successResponse($comment,$newGroupsArr);
    }

    /** 刪除 group
     *  Desc:刪除 group
     *  參考輸入範例
     * /groups/delete/{id}
        {
        }
     */
    public function delete($id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        //checkGroupUser
        $userArr = $this->groupService->getGroupUser($id,$userId);
        //end checkGroupUser

        if($id !=null){
            $filedArr['status'] ='delete';
            $groups = $this->groupsRepository->update(
                $filedArr,
                $id
            );
            $data='';
            $comment ='delete down';
            $this->successResponse($comment,$data);
        }
    }

    /** 查詢 group
     *  Desc:查詢 group
     *  參考輸入範例
     * /groups/{id}
        {
        }
     */
    public function findIdInfo($id,Request $request){
        $this->changeRead();
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //getGroupUser
        $userArr = $this->groupService->getGroupUser($id,$userId);
        //end getGroupUser

        //擁有者id
        $groupOwnId = $this->groupService->getGroupOwnerId($id);

        //先抓出要回傳的資料
        $groups = $this->groupsRepository->findWhere([
            'id' =>$id
            ]);
        $groupsArr = $groups->toArray();
        //群組已經刪除
        if($groupsArr['0']['status'] =='delete'){
            $code = 401;
            $comment = 'group not fund or delete';
            $this->failResponse($comment, $code);
        }

        //群組關閉只有成員看得到
        if($groupsArr['0']['privacy_status'] =='private'){
            if(count($userArr) == 0){
                $code = 401;
                $comment = 'type no permissions';
                $this->failResponse($comment, $code);
            }
        }
        //end
        //回傳資料
        //取得圖片連結
        foreach ($groupsArr as $key =>$value ){
            $groupsArr[$key]['cover_links'] = empty($groupsArr[$key]['cover']) ? [] : $this->getImageLinks($groupsArr[$key]['cover']);
            $groupsArr[$key]['avatar_links'] = empty($groupsArr[$key]['avatar']) ? [] : $this->getImageLinks($groupsArr[$key]['avatar']);
        }
        //end 取得圖片連結

        $comment ='return group info';
        $groups = $this->baseFuncService->idToString($groupsArr,$this->tmpIdArr);
        $this->successResponse($comment,$groups);

    }


    public function uploadCover(Request $request, $id){
        $sessionCode  = $request->header('session');
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';
            $this->failResponse($comment, $code);
        } // END if

        if (empty($request->hasFile('cover'))) {
            $code = 400;
            $comment = 'cover is empty';

            $this->failResponse($comment, $code);
        } // END if

        $group = $this->groupsRepository->findByField('id', $id);

        if ($group->isEmpty()) {
            $code = 404;
            $comment = 'group error';

            $this->failResponse($comment, $code);
        } // END if

        if ($group->first()->status == 'delete') {
            $code = 403;
            $comment = 'group status error';

            $this->failResponse($comment, $code);
        } // END if
        $coverUploadFile = $request->file('cover');

        if (empty($coverUploadFile->isValid())) {
            $code = 403;
            $comment = 'cover error';

            $this->failResponse($comment, $code);
        }

        // END if

        $filename = $this->convertImage($coverUploadFile, $group->first()->id, 'group', 'cover');

        $fileExtension = $coverUploadFile->extension();
        $fileMimeType  = $coverUploadFile->getMimeType();
        $fileSize      = $coverUploadFile->getSize();
        $fileOriginalName = $coverUploadFile->getClientOriginalName();
        $coverDir  = $this->directoryRepository->findByParentTypeAndParentIdAndTypeAndOwnerId('group', $group->first()->id, 'cover', $group->first()->owner_id);
        if ($coverDir->isEmpty()) {
            $code = 422;
            $comment = 'create dir error';
            $this->failResponse($comment, $code);
        } // END if

        $coverFile = $this->fileRepository->initImageFile($coverDir->first()->id, 0, $filename, $fileExtension, $fileMimeType, $fileSize, true, $group->first()->owner_id, $fileOriginalName);

        if ($coverFile->isEmpty()) {
            $code = 422;
            $comment = 'create file error';

            $this->failResponse($comment, $code);
        } // END if


        $this->groupsRepository->update(['cover' => $filename], $group->first()->id);


        $resultData = [
            'session' => $sessionCode,
            'cover_links' => $this->getImageLinks($filename)
        ];
        $this->successResponse('upload cover success', $resultData);
    } // END function

    public function uploadAvatar(Request $request, $id){
        $sessionCode  = $request->header('session');
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($request->hasFile('avatar'))) {
            $code = 400;
            $comment = 'avatar is empty';

            $this->failResponse($comment, $code);
        } // END if

        $group = $this->groupsRepository->findByField('id', $id);

        if ($group->isEmpty()) {
            $code = 404;
            $comment = 'group error';

            $this->failResponse($comment, $code);
        } // END if

        if ($group->first()->status == 'delete') {
            $code = 403;
            $comment = 'group status error';

            $this->failResponse($comment, $code);
        } // END if


        $avatarUploadFile = $request->file('avatar');
        if (empty($avatarUploadFile->isValid())) {
            $code = 403;
            $comment = 'avatar error';
            $this->failResponse($comment, $code);
        }
        //END if

        $filename = $this->convertImage($avatarUploadFile, $group->first()->id, 'group', 'avatar');

        $fileExtension = $avatarUploadFile->extension();
        $fileMimeType  = $avatarUploadFile->getMimeType();
        $fileSize      = $avatarUploadFile->getSize();
        $fileOriginalName = $avatarUploadFile->getClientOriginalName();

        $avatarDir  = $this->directoryRepository->findByParentTypeAndParentIdAndTypeAndOwnerId('group', $group->first()->id, 'avatar', $group->first()->owner_id);

        if ($avatarDir->isEmpty()) {
            $code = 422;
            $comment = 'create dir error';

            $this->failResponse($comment, $code);
        } // END if

        $avatarFile = $this->fileRepository->initImageFile($avatarDir->first()->id, 0, $filename, $fileExtension, $fileMimeType, $fileSize, true, $group->first()->owner_id, $fileOriginalName);

        if ($avatarFile->isEmpty()) {
            $code = 422;
            $comment = 'create file error';

            $this->failResponse($comment, $code);
        } // END if

        $this->groupsRepository->update(['avatar' => $filename], $group->first()->id);
        $resultData = [
            'session' => $sessionCode,
            'avatar_links' => $this->getImageLinks($filename)
        ];
        $this->successResponse('upload avatar success', $resultData);
    } // END function

    //轉換為讀取資料庫
    private function changeRead(){
        $this->groupsRepository->changeRead();
        $this->groupUsersRepository->changeRead();
    }

}