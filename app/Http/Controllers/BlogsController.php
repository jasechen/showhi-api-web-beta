<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Traits\ImageToolTrait;
use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\BlogRepositoryEloquent;
use App\Repositories\BoardRepositoryEloquent;
use App\Repositories\TextRepositoryEloquent;
use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\FileRepositoryEloquent;

class BlogsController extends Controller
{
    use JsonResponseTrait, ImageToolTrait;

    protected $userRepository;
    protected $sessionRepository;
    protected $blogRepository;
    protected $boardRepository;
    protected $textRepository;


    public function __construct()
    {
        $this->userRepository    = app(UserRepositoryEloquent::class);
        $this->sessionRepository = app(SessionRepositoryEloquent::class);

        $this->blogRepository  = app(BlogRepositoryEloquent::class);
        $this->boardRepository = app(BoardRepositoryEloquent::class);
        $this->textRepository  = app(TextRepositoryEloquent::class);

        $this->directoryRepository = app(DirectoryRepositoryEloquent::class);
        $this->fileRepository = app(FileRepositoryEloquent::class);
    } // END function


    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function find(Request $request, $id)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $blog = $this->blogRepository->findById($id);

        if ($blog->isEmpty()) {
            $code = 404;
            $comment = 'blog error';

            $this->failResponse($comment, $code);
        } // END if


        $blogData = $blog->first();

        $blogData->id = strval($blogData->id);
        $blogData->owner_id = strval($blogData->owner_id);
        $blogData->creater_id = strval($blogData->creater_id);

        $blogData->cover_links = empty($blogData->cover) ? [] : $this->getImageLinks($blogData->cover);

        $boards = $this->boardRepository->findByParentTypeAndParentId('blog', $blog->first()->id);

        if ($boards->isNotEmpty()) {
            $boardsData = $boards->all();
            foreach ($boardsData as $board_idx => $board) {
                $boardsData[$board_idx]->id = strval($board->id);
                $boardsData[$board_idx]->parent_id = strval($board->parent_id);
                $boardsData[$board_idx]->creater_id = strval($board->creater_id);
                if (!empty($board->has_text)) {
                    $board->Text->id = strval($board->Text->id);
                    $board->Text->board_id = strval($board->Text->board_id);
                    $board->Text->creater_id = strval($board->Text->creater_id);
                    $boardsData[$board_idx]->text = $board->Text;
                }
            } // END foreach
            $blogData->boards = $boardsData;
        } // END if


        $resultData = ['session' => $sessionCode, 'blog' => $blogData];

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     * Change intro of the blog
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function changeIntro(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $intro  = $request->input('intro');

        if (empty($intro)) {
            $code = 400;
            $comment = 'intro is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $blog = $this->blogRepository->findById($id);

        if ($blog->isEmpty()) {
            $code = 404;
            $comment = 'blog error';

            $this->failResponse($comment, $code);
        } // END if

        if ($blog->first()->status != 'enable') {
            $code = 403;
            $comment = 'blog status error';

            $this->failResponse($comment, $code);
        } // END if


        $this->blogRepository->update(['intro' => $intro], $blog->first()->id);


        $resultData = ['session' => $sessionCode];

        $this->successResponse('change intro success', $resultData);
    } // END function


    /**
     * Upload cover file of the blog.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function uploadCover(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($request->hasFile('cover'))) {
            $code = 400;
            $comment = 'cover is empty';

            $this->failResponse($comment, $code);
        } // END if

        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $blog = $this->blogRepository->findById($id);

        if ($blog->isEmpty()) {
            $code = 404;
            $comment = 'blog error';

            $this->failResponse($comment, $code);
        } // END if

        if ($blog->first()->status != 'enable') {
            $code = 403;
            $comment = 'blog status error';

            $this->failResponse($comment, $code);
        } // END if


        $coverUploadFile = $request->file('cover');

        if (empty($coverUploadFile->isValid())) {
            $code = 403;
            $comment = 'cover error';

            $this->failResponse($comment, $code);
        } // END if

        $filename = $this->convertImage($coverUploadFile, $blog->first()->id, 'blog', 'cover');

        $fileExtension = $coverUploadFile->extension();
        $fileMimeType  = $coverUploadFile->getMimeType();
        $fileSize      = $coverUploadFile->getSize();
        $fileOriginalName = $coverUploadFile->getClientOriginalName();

        $coverDir  = $this->directoryRepository->findByParentTypeAndParentIdAndTypeAndOwnerId('blog', $blog->first()->id, 'cover', $blog->first()->owner_id);

        if ($coverDir->isEmpty()) {
            $code = 422;
            $comment = 'create dir error';

            $this->failResponse($comment, $code);
        } // END if

        $coverFile = $this->fileRepository->initImageFile($coverDir->first()->id, 0, $filename, $fileExtension, $fileMimeType, $fileSize, true, $blog->first()->owner_id, $fileOriginalName);

        if ($coverFile->isEmpty()) {
            $code = 422;
            $comment = 'create file error';

            $this->failResponse($comment, $code);
        } // END if


        $this->blogRepository->update(['cover' => $filename], $blog->first()->id);


        $coverPaths = $this->getImageLinks($filename);

        foreach ($coverPaths as $coverPath) {
            $sourceFilename = base_path('public'.$coverPath);
            $targetFolder   = mb_substr(pathinfo($coverPath, PATHINFO_DIRNAME), 9);
            $targetFilename = pathinfo($coverPath, PATHINFO_BASENAME);

            $sourceFilename = new File($sourceFilename);
            Storage::disk('s3-gallery')->putFileAs($targetFolder, $sourceFilename, $targetFilename, 'public');
        } // END foreach


        $resultData = [
            'session' => $sessionCode,
            'cover_links' => $coverPaths
        ];

        $this->successResponse('upload cover success', $resultData);
    } // END function
} // END class
