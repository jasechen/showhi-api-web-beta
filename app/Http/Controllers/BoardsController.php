<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Traits\ImageToolTrait;
use App\Traits\JsonResponseTrait;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\GroupsRepositoryEloquent;
use App\Repositories\BlogRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\BoardRepositoryEloquent;
use App\Repositories\TextRepositoryEloquent;
use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\FileRepositoryEloquent;

class BoardsController extends Controller
{
    use JsonResponseTrait, ImageToolTrait;

    protected $sessionRepository;


    public function __construct()
    {
        $this->sessionRepository = app(SessionRepositoryEloquent::class);
        $this->blogRepository = app(BlogRepositoryEloquent::class);
        $this->groupRepository = app(GroupsRepositoryEloquent::class);
        $this->userRepository = app(UserRepositoryEloquent::class);
        $this->boardRepository = app(BoardRepositoryEloquent::class);
        $this->textRepository = app(TextRepositoryEloquent::class);
        $this->directoryRepository = app(DirectoryRepositoryEloquent::class);
        $this->fileRepository = app(FileRepositoryEloquent::class);
    } // END function


    /**
     *
     */
    public function createText(Request $request)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        $parentType = $request->input('parent_type', 'blog');
        $parentId   = $request->input('parent_id');

        if (empty($parentId)) {
            $code = 400;
            $comment = 'parent_id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $boardId = $request->input('board_id');
        $privacyStatus = $request->input('privacy_status', 'public');
        $status = $request->input('status', 'enable');
        $content = $request->input('content');

        if (empty($content)) {
            $code = 400;
            $comment = 'content is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if

        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if

        $user = $this->userRepository->findByToken($token);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        $parentTypeValidator = Validator::make(['parent_type' => $parentType],
            ['parent_type' => ['in:blog,group']]
        );

        if ($parentTypeValidator->fails()) {
            $code = 403;
            $comment = 'parent_type error';

            $this->failResponse($comment, $code);
        } // END if

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:public,privacy']]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 403;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:enable,disable,delete']]
        );

        if ($statusValidator->fails()) {
            $code = 403;
            $comment = 'status error';

            $this->failResponse($comment, $code);
        } // END if

        if ($parentType == 'blog') {
            $parent = $this->blogRepository->findById($parentId);
        } else {
            $parent = $this->groupRepository->findById($parentId);
        } // END if

        if ($parent->isEmpty()) {
            $code = 404;
            $comment = 'parent error';

            $this->failResponse($comment, $code);
        } // END if

        if ($parent->first()->owner_id != $user->first()->id) {
            $code = 403;
            $comment = 'creater != owner';

            $this->failResponse($comment, $code);
        } // END if

        if (!empty($boardId)) {
            $board = $this->boardRepository->findById($boardId);

            if ($board->isEmpty()) {
                $code = 404;
                $comment = 'board error';

                $this->failResponse($comment, $code);
            } // END if

            $boardId = $board->first()->id;
        } else {
            $insertBoardData = [
                'parent_type' => $parentType,
                'parent_id' => $parentId,
                'privacy_status' => $privacyStatus,
                'status' => $status,
                'text_id' => 0,
                'directory_id' => 0,
                'has_file' => false,
                'creater_id' => $user->first()->id
            ];

            $board = $this->boardRepository->create($insertBoardData);

            if (empty($board)) {
                $code = 422;
                $comment = 'create board error';

                $this->failResponse($comment, $code);
            } // END if

            $boardId = $board->id;
        } // END if


        $insertTextData = [
            'board_id' => $boardId,
            'content'  => $content,
            'creater_id' => $user->first()->id
        ];

        $text = $this->textRepository->create($insertTextData);

        if (empty($text)) {
            $code = 422;
            $comment = 'create text error';

            $this->failResponse($comment, $code);
        } // END if

        $updateBoardData = [
            'text_id' => $text->id
        ];

        $this->boardRepository->update($updateBoardData, $boardId);


        $resultData = ['session' => $sessionCode, 'board_id' => strval($board->id), 'text_id' => strval($text->id)];

        $this->successResponse('create text success', $resultData);
    } // END function


    /**
     * Upload cover file of the blog.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function uploadImage(Request $request)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        $parentType = $request->input('parent_type', 'blog');
        $parentId   = $request->input('parent_id');

        if (empty($parentId)) {
            $code = 400;
            $comment = 'parent_id is empty';

            $this->failResponse($comment, $code);
        } // END if

        $boardId = $request->input('board_id');
        $privacyStatus = $request->input('privacy_status', 'public');
        $status = $request->input('status', 'enable');

        if (empty($request->hasFile('image'))) {
            $code = 400;
            $comment = 'image is empty';

            $this->failResponse($comment, $code);
        } // END if

        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if

        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if

        $user = $this->userRepository->findByToken($token);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        $parentTypeValidator = Validator::make(['parent_type' => $parentType],
            ['parent_type' => ['in:blog,group']]
        );

        if ($parentTypeValidator->fails()) {
            $code = 403;
            $comment = 'parent_type error';

            $this->failResponse($comment, $code);
        } // END if

        $privacyStatusValidator = Validator::make(['privacy_status' => $privacyStatus],
            ['privacy_status' => ['in:public,privacy']]
        );

        if ($privacyStatusValidator->fails()) {
            $code = 403;
            $comment = 'privacy_status error';

            $this->failResponse($comment, $code);
        } // END if

        $statusValidator = Validator::make(['status' => $status],
            ['status' => ['in:enable,disable,delete']]
        );

        if ($statusValidator->fails()) {
            $code = 403;
            $comment = 'status error';

            $this->failResponse($comment, $code);
        } // END if

        if ($parentType == 'blog') {
            $parent = $this->blogRepository->findById($parentId);
        } else {
            $parent = $this->groupRepository->findById($parentId);
        } // END if

        if ($parent->isEmpty()) {
            $code = 404;
            $comment = 'parent error';

            $this->failResponse($comment, $code);
        } // END if

        if ($parent->first()->owner_id != $user->first()->id) {
            $code = 403;
            $comment = 'creater != owner';

            $this->failResponse($comment, $code);
        } // END if

        if (!empty($boardId)) {
            $board = $this->boardRepository->findById($boardId);

            if ($board->isEmpty()) {
                $code = 404;
                $comment = 'board error';

                $this->failResponse($comment, $code);
            } // END if

            $boardId = $board->first()->id;
        } else {
            $insertBoardData = [
                'parent_type' => $parentType,
                'parent_id' => $parentId,
                'privacy_status' => $privacyStatus,
                'status' => $status,
                'text_id' => '',
                'directory_id' => '',
                'has_file' => false,
                'creater_id' => $user->first()->id
            ];

            $board = $this->boardRepository->create($insertBoardData);

            if (empty($board)) {
                $code = 422;
                $comment = 'create board error';

                $this->failResponse($comment, $code);
            } // END if

            $boardId = $board->id;
        } // END if


        $imageUploadFile = $request->file('image');

        if (empty($imageUploadFile->isValid())) {
            $code = 403;
            $comment = 'image error';

            $this->failResponse($comment, $code);
        } // END if

        $filename = $this->convertImage($imageUploadFile, $parent->first()->id, $parentType, 'no_dir');

        $fileExtension = $imageUploadFile->extension();
        $fileMimeType  = $imageUploadFile->getMimeType();
        $fileSize      = $imageUploadFile->getSize();
        $fileOriginalName = $imageUploadFile->getClientOriginalName();

        $imageDir  = $this->directoryRepository->findByParentTypeAndParentIdAndTypeAndOwnerId($parentType, $parent->first()->id, 'no_dir', $parent->first()->owner_id);

        if ($imageDir->isEmpty()) {
            $code = 422;
            $comment = 'create dir error';

            $this->failResponse($comment, $code);
        } // END if

        $imageFile = $this->fileRepository->initImageFile($imageDir->first()->id, $boardId, $filename, $fileExtension, $fileMimeType, $fileSize, false, $parent->first()->owner_id, $fileOriginalName);

        if ($imageFile->isEmpty()) {
            $code = 422;
            $comment = 'create file error';

            $this->failResponse($comment, $code);
        } // END if


        $updateBoardData = [
            'has_file' => true
        ];

        $this->boardRepository->update($updateBoardData, $boardId);


        $imagePaths = $this->getImageLinks($filename);

        foreach ($imagePaths as $imagePath) {
            $sourceFilename = base_path('public' . $imagePath);
            $targetFolder   = mb_substr(pathinfo($imagePath, PATHINFO_DIRNAME), 9);
            $targetFilename = pathinfo($imagePath, PATHINFO_BASENAME);

            $sourceFilename = new File($sourceFilename);
            Storage::disk('s3-gallery')->putFileAs($targetFolder, $sourceFilename, $targetFilename, 'public');
        } // END foreach


        $resultData = [
            'session'  => $sessionCode,
            'board_id' => strval($boardId),
            'file_id'  => strval($imageFile->first()->id),
            'image_links' => $imagePaths
        ];

        $this->successResponse('upload image success', $resultData);
    } // END function


} // END class
