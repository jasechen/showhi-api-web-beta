<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\JsonResponseTrait;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\LocalesRepositoryEloquent;

class InterestsController extends Controller
{
    use JsonResponseTrait;

    protected $sessionRepository;


    public function __construct()
    {
        $this->sessionRepository = app(SessionRepositoryEloquent::class);
        $this->localesRepository = app(LocalesRepositoryEloquent::class);
    } // END function


    /**
     *
     */
    public function findByLang(Request $request, $lang)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if

        if (!in_array($lang, config('locales.CURRENT_LOCALES_LANGS'))) {
            $code = 404;
            $comment = 'lang error';

            $this->failResponse($comment, $code);
        } // END if


        $interests = [];
        foreach (config('interest') as $interestCode) {
           $locales_interest = 'interest_name_' . $interestCode;

            $isCodeLangExist = $this->localesRepository->isCodeLangExist($locales_interest, $lang);
            if (empty($isCodeLangExist)) {continue;}

            $localesContent = $this->localesRepository->findByCodeAndLang($locales_interest, $lang);
            $interestName = $localesContent->first()->content;
            if (empty($interestName)) {continue;}

            $interest = [
                'code' => $interestCode,
                'interest_name' => $interestName
            ];

            array_push($interests, $interest);
        } // END foreach


        if (empty($interests)) {
            $code = 400;
            $comment = 'interest empty';

            $this->failResponse($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'interests' => $interests];

        $this->successResponse('find success', $resultData);
    } // END function

} // END class
