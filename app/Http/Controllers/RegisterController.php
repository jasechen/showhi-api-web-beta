<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

use App\Mail\VerifyEmail;
use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserCheckRepositoryEloquent;
use App\Repositories\UserProfileRepositoryEloquent;
use App\Repositories\UserRecruiterRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\BlogRepositoryEloquent;
use App\Repositories\BoardRepositoryEloquent;
use App\Repositories\TextRepositoryEloquent;

use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\QrcodesRepositoryEloquent;

use Aloha\Twilio\Support\Laravel\Facade as Twilio;

class RegisterController extends Controller
{
    use JsonResponseTrait;

    protected $userRepository;
    protected $userCheckRepository;
    protected $userProfileRepository;
    protected $userRecruiterRepository;
    protected $sessionRepository;
    protected $blogRepository;
    protected $boardRepository;
    protected $textRepository;
    protected $directoryRepository;
    protected $qrcodeRepository;


    public function __construct()
    {

        $this->userRepository      = app(UserRepositoryEloquent::class);
        $this->userCheckRepository = app(UserCheckRepositoryEloquent::class);
        $this->userProfileRepository   = app(UserProfileRepositoryEloquent::class);
        $this->userRecruiterRepository = app(UserRecruiterRepositoryEloquent::class);

        $this->sessionRepository  = app(SessionRepositoryEloquent::class);

        $this->blogRepository  = app(BlogRepositoryEloquent::class);
        $this->boardRepository = app(BoardRepositoryEloquent::class);
        $this->textRepository  = app(TextRepositoryEloquent::class);

        $this->directoryRepository = app(DirectoryRepositoryEloquent::class);
        $this->qrcodeRepository = app(QrcodesRepositoryEloquent::class);
    } // END function


    /**
     * 使用者註冊
     *
     * @param  \Illuminate\Http\Request  $request
     * @throws
     * @return
     */
    public function index(Request $request)
    {

        $sessionCode = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $account  = $request->input('account');

        if (empty($account)) {
            $code = 400;
            $comment = 'account is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (!filter_var($account, FILTER_VALIDATE_EMAIL)) {
            $code = 403;
            $comment = 'account error (email format error)';

            $this->failResponse($comment, $code);
        } // END if


        $password  = $request->input('password');
        $passwordRepeat  = $request->input('password_repeat');

        if (empty($password) or empty($passwordRepeat)) {
            $code = 400;
            $comment = 'password / password_repeat is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 403;
            $comment = 'password\' length < 8';

            $this->failResponse($comment, $code);
        } // END if

        if ($password != $passwordRepeat) {
            $code = 403;
            $comment = 'password / password_repeat is NOT equal';

            $this->failResponse($comment, $code);
        } // END if

        $firstName = $request->input('first_name');

        if (empty($firstName)) {
            $code = 400;
            $comment = 'first_name is empty';

            $this->failResponse($comment, $code);
        } // END if

        $lastName = $request->input('last_name');

        if (empty($lastName)) {
            $code = 400;
            $comment = 'last_name is empty';

            $this->failResponse($comment, $code);
        } // END if

        $mobileCountryCode = $request->input('mobile_country_code');

        if (empty($mobileCountryCode)) {
            $code = 400;
            $comment = 'mobile_country_code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $mobilePhone = $request->input('mobile_phone');

        if (empty($mobilePhone)) {
            $code = 400;
            $comment = 'mobile_phone is empty';

            $this->failResponse($comment, $code);
        } // END if

        $qrcodeId   = $request->input('qrcode_id');

        if (empty($qrcodeId)) {
            $code = 400;
            $comment = 'qrcode_id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $mobilePhoneCheck = '+'.$mobileCountryCode.$mobilePhone;
        $validatorMobilePhone = Validator::make(['mobile_phone' => $mobilePhoneCheck], [
            'mobile_phone' => 'phone:AUTO,mobile',
        ]);

        if ($validatorMobilePhone->fails()) {
            $code = 403;
            $comment = 'mobile_phone format error';

            $this->failResponse($comment, $code);
        } // END if


        $qrCode = $this->qrcodeRepository->findById($qrcodeId);

        if ($qrCode->isEmpty()) {
            $code = 404;
            $comment = 'qrcode error';

            $this->failResponse($comment, $code);
        } // END if

        $now = Carbon::now();
        if ($now->timestamp > $qrCode->first()->created_at->timestamp + $qrCode->first()->expire_time) {
            $code = 403;
            $comment = 'qrcode expire time';

            $this->failResponse($comment, $code);
        } // END if

        $recruiterId  = $qrCode->first()->creater_id;
        $introducerId = $request->input('introducer_id', $recruiterId);
        $generateQrcode = $qrCode->first()->type == 'agent' ? true : false;
        $type = $qrCode->first()->type == 'agent' ?  'business' : 'member';


        $user = $this->userRepository->findByAccount($account);

        if ($user->isNotEmpty()) {
            $code = 403;
            $comment = 'someone already use this account';

            $this->failResponse($comment, $code);
        } // END if

        $profile = $this->userProfileRepository->findByMobilePhone($mobilePhone);

        if ($profile->isNotEmpty()) {
            $code = 403;
            $comment = 'mobile phone already used';

            $this->failResponse($comment, $code);
        } // END if

        $user = $this->userRepository->init($account, $password, $introducerId, $type, $generateQrcode);

        if ($user->isEmpty()) {
            $code = 422;
            $comment = 'create user fail';

            $this->failResponse($comment, $code);
        } // END if


        $profile = $this->userProfileRepository->findByUserId($user->first()->id);

        if ($profile->isEmpty()) {
            $this->userProfileRepository->init($user->first()->id, $user->first()->account, $firstName, $lastName, $mobileCountryCode, $mobilePhone);
        } // END if


        $userRecruiters = $this->userRecruiterRepository->findByUserId($user->first()->id);

        if ($userRecruiters->isEmpty()) {
            $this->userRecruiterRepository->init($user->first()->id, $qrcodeId, $recruiterId, '');
        } // END if


        $userEmailCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'email');

        if ($userEmailCheck->isEmpty()) {
            $userEmailCheck = $this->userCheckRepository->initOption($user->first()->id, 'email');
        } // END if

        $userMobileCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'mobile_phone');

        if ($userMobileCheck->isEmpty()) {
            $userMobileCheck = $this->userCheckRepository->initOption($user->first()->id, 'mobile_phone');
        } // END if

        $userRecruiterCheck = $this->userCheckRepository->findByUserIdAndOption($user->first()->id, 'recruiter');

        if ($userRecruiterCheck->isEmpty()) {
            $userRecruiterCheck = $this->userCheckRepository->initOption($user->first()->id, 'recruiter');
        } // END if


        $blog = $this->blogRepository->findByOwnerId($user->first()->id);

        if ($blog->isEmpty()) {
            $blog  = $this->blogRepository->init($user->first()->id, $recruiterId);
            $board = $this->boardRepository->findByParentTypeAndParentId('blog', $blog->first()->id);

            if ($board->isEmpty()) {
                $board = $this->boardRepository->init($blog->first()->id, $user->first()->id);
                $text = $this->textRepository->findByBoardId($board->first()->id);

                if ($text->isEmpty()) {
                    $text = $this->textRepository->init($board->first()->id, $user->first()->id);
                    $this->boardRepository->update(['text_id' => $text->first()->id], $board->first()->id);
                } // END if
            } // END if
        } // END if


        $noDir  = $this->directoryRepository->initDefaultDir('blog', $blog->first()->id, $user->first()->id);
        $noPath = $noDir->first()->parent_type . '_' . $blog->first()->id . '/' . $noDir->first()->type;
        Storage::disk('gallery')->makeDirectory($noPath, 0755, true);
        Storage::disk('s3-gallery')->makeDirectory($noPath, 0755, true);

        $coverDir  = $this->directoryRepository->initDefaultAlbum('blog', $blog->first()->id, 'cover', $user->first()->id);
        $coverPath = $coverDir->first()->parent_type . '_' . $blog->first()->id . '/' . $coverDir->first()->type;
        Storage::disk('gallery')->makeDirectory($coverPath, 0755, true);
        Storage::disk('s3-gallery')->makeDirectory($coverPath, 0755, true);

        $avatarDir  = $this->directoryRepository->initDefaultAlbum('blog', $blog->first()->id, 'avatar', $user->first()->id);
        $avatarPath = $avatarDir->first()->parent_type . '_' . $blog->first()->id . '/' . $avatarDir->first()->type;
        Storage::disk('gallery')->makeDirectory($avatarPath, 0755, true);
        Storage::disk('s3-gallery')->makeDirectory($avatarPath, 0755, true);

        //qrcodes
        $qrcodesPath = $avatarDir->first()->parent_type . '_' . $blog->first()->id;
        Storage::disk('qrcode')->makeDirectory($qrcodesPath, 0755, true);

        $to = $user->first()->account;

        $emailFields = [
            'account' => $to,
            'code' => $userEmailCheck->first()->code,
            'link' => 'https://showhi.co/verify/'.$user->first()->id,
        ];

        Mail::to($to)->send(new VerifyEmail($emailFields));


        $phone = '+' . $mobileCountryCode . $mobilePhone;
        $message = 'Phone check code => ' . $userMobileCheck->first()->code;
        Twilio::message($phone, $message);


        $user = $this->userRepository->findById($user->first()->id);

        $resultData = ['session' => $sessionCode, 'token' => $user->first()->token];

        $this->successResponse('register success', $resultData);
    } // END function
} // END class
