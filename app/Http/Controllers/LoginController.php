<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserProfileRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\LoginLogRepositoryEloquent;

class LoginController extends Controller
{
    use JsonResponseTrait;

    protected $userRepository;
    protected $userProfileRepository;
    protected $sessionRepository;
    protected $loginLogRepository;


    public function __construct()
    {
        $this->userRepository        = app(UserRepositoryEloquent::class);
        $this->userProfileRepository = app(UserProfileRepositoryEloquent::class);

        $this->sessionRepository  = app(SessionRepositoryEloquent::class);
        $this->loginLogRepository = app(LoginLogRepositoryEloquent::class);
    } // END function


    /**
     * 使用者登入
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiParam {string}                                  account                      使用者帳號
     * @apiParam {string}                                  password                     密碼
     *
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     * @apiSuccess (Success) {string}   data.token          使用者 token
     *
     */
    public function index(Request $request)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $account  = $request->input('account');
        $mobileCountryCode  = $request->input('mobile_country_code');
        $mobilePhone  = $request->input('mobile_phone');

        if (empty($account)) {
            if (empty($mobileCountryCode) or empty($mobilePhone)) {
                $code = 400;
                $comment = 'account / mobile_country_code, mobile_phone is empty';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $password  = $request->input('password');

        if (empty($password)) {
            $code = 400;
            $comment = 'password is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (mb_strlen($password) < config('app.PASSWORD_LENGTH_LIMIT')) {
            $code = 403;
            $comment = 'password\' length < ' . config('app.PASSWORD_LENGTH_LIMIT');

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $session = $this->sessionRepository->findByCode($sessionCode);

        if (filter_var($account, FILTER_VALIDATE_EMAIL)) {
            $user = $this->userRepository->findByAccount($account);
        } else {

            $mobilePhoneCheck = '+' . $mobileCountryCode . $mobilePhone;
            $validatorMobilePhone = Validator::make(['mobile_phone' => $mobilePhoneCheck], [
                'mobile_phone' => 'phone:AUTO,mobile',
            ]);

            if ($validatorMobilePhone->fails()) {
                $code = 403;
                $comment = 'mobile_phone format error';

                $this->failResponse($comment, $code);
            } // END if

            $profile = $this->userProfileRepository->findByMobileCountryCodeAndPhone($mobileCountryCode, $mobilePhone);

            if ($profile->isEmpty()) {
                $code = 404;
                $comment = 'mobile country code / phone error';

                $this->failResponse($comment, $code);
            } // END if

            $user = $this->userRepository->findById($profile->first()->user_id);
        } // END if


        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'account error';

            $this->failResponse($comment, $code);
        } // END if

        if (!password_verify($password, $user->first()->password)) {
            $code = 403;
            $comment = 'password error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'enable') {
            $code = 403;
            $comment = 'user status error';
            $resultData = ['token' => $user->first()->token];

            $this->failResponse($comment, $code, $resultData);
        } // END if


        $sessionFields = [
            'status' => 'login',
            'owner_id' => $user->first()->id
        ];

        $this->sessionRepository->update($sessionFields, $session->first()->id);


        $loginLogFields = [
            'session_id' => $session->first()->id,
            'device_id'  => $session->first()->device_id,
            'creater_id' => $user->first()->id
        ];

        $this->loginLogRepository->create($loginLogFields);


        $resultData = ['session' => $sessionCode, 'token' => $user->first()->token];

        $this->successResponse('login success', $resultData);
    } // END function
} // END class
