<?php namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\QrcodeService;
use App\Traits\JsonResponseTrait;
use App\Repositories\QrcodesRepositoryEloquent;
use App\Repositories\GroupsRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\BlogRepositoryEloquent;

use App\Repositories\UserProfileRepository;
use App\Repositories\UserCheckRepositoryEloquent;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\BaseFuncService;
//use Illuminate\Routing\Redirector;
class QrcodesController extends Controller {
    use JsonResponseTrait;
    public function __construct(){
        $this->qrcodeService = new QrcodeService;
        $this->qrcodesRepository  = app(QrcodesRepositoryEloquent::class);
        $this->sessionRepository  = app(SessionRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
        $this->blogRepository  = app(BlogRepositoryEloquent::class);
        $this->groupsRepository  = app(GroupsRepositoryEloquent::class);

        $this->userProfileRepository  = app(UserProfileRepository::class);
        $this->userCheckRepository  = app(UserCheckRepositoryEloquent::class);

        $this->baseFuncService = new BaseFuncService;
        $this->tmpIdArr =['id','creater_id','parent_id'];
        $this->tmpRegisterIdArr =['id','user_id','recruiter_id','qrcode_id'];

    }


    /** 建立qrcode
     *  Desc:建立qrcode，要學校 or 仲介才能建立
     *  參考輸入範例
     * /qrcodes/create
        {
            "type":"agent"
        }
     */
    public function create(Request $request){
        $expireTime    = config('qrcodes.EXPIRETIME');
        //type 不能為空
        $type = $request->input('type');
        $parentType = $request->input('parentType');
        $parentId = $request->input('parentId');
        $code ='400';
        $data ='';
        if($type == null){
            $comment ='type is empty';
            $this->failResponse($comment,$code,$data);
        }
        if($parentType == null){
            $parentType ='blog';
        }

        if($parentType == 'group' && $parentId ==null){
            $comment ='parentId is empty';
            $this->failResponse($comment,$code,$data);
        }

        //end input
        //own check
        $userArr = $this->baseFuncService->getSessionUserArr($request);
        //end check own
        //取blog id
        if($parentId == null){
            $blog = $this->blogRepository->findWhere([
                'owner_id' =>$userArr['0']['id']
                ]);
            if($blog->isEmpty()){
                $comment ='blog is error';
                $this->failResponse($comment,$code);
            }
            $parentId = $blog->first()->id;
        }else{
          $group = $this->groupsRepository->findWhere([
                'id' =>$parentId
                ]);

            if($group->isEmpty()){
                $comment ='group is delete or empty';
                $this->failResponse($comment,$code);
            }
        }
        //end


        //仲介不能招仲介
        if($userArr['0']['type'] == 'businese' && $type == 'agent'){
            $code = 401;
            $comment = 'type no permissions';
            $this->failResponse($comment,$code);
        }

        unset($userArr['0']['password']);
        $account = $userArr['0']['account'];
        if($account == null){
            $comment ='account is empty';
            $this->failResponse($comment,$code,$data);
        }

        //存進資料庫
        $now = Carbon::now();
            $savePath = 'public/qrcode/'.$parentType.'_'.$parentId.'/';
            $qrcodeFields = [
                                'type' => $type,
                                'num_scan' => 0,
                                'url' => '',
                                'filename' => '',
                                'parent_type' => $parentType,
                                'parent_id' => $parentId,
                                'creater_id' => $userArr['0']['id'],
                                'created_at' => $now,
                                'updated_at' => $now,
                                'expire_time' => $expireTime,
                                'note' => '',
                                'status' => 'enable',
                            ];
            $qrcodes = $this->qrcodesRepository->create($qrcodeFields);

            //建立Qrcode
            $retult =$this->qrcodeService->create($qrcodes->id,$parentType,$parentId);
            $filedArr['url'] =$retult['url'];
            $filedArr['filename'] =$retult['fileName'];
            $tmp = $this->qrcodesRepository->update(
                $filedArr,
                $qrcodes->id
            );
            //建立Qrcode 指定內容 路徑 檔名 圖片大小 編碼
            $data = [
                'id' =>(string) $qrcodes->id,
                'creater_id' =>(string) $qrcodes->creater_id,
                'file_name' => $retult['fileName'],
                'created_at' => $now,
                'expire_time' => $expireTime,
                'url' => $retult['url']
            ];
            $comment ='return create qrcode info';
            $this->successResponse($comment,$data);

    }


    /** 確認連結有效性
     *  Desc:確認Qrcode連結有效性
     *  參考輸入範例
     * /qrcodes/{account}/{id}
        {
        }
     */

    public function check($id,Request $request){
        $this->changeRead();
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }

        $baseUrl   = $request->input('qrcodes_baseurl',   config('qrcodes.QRCODES_BASE_URL'));
        $url =$baseUrl.$id;

        //todo check login
        //確認qrcode是否過期
        $now = Carbon::now();
        $tmpQrcodes = $this->qrcodesRepository->findWhere([
            'url' =>$url,
            'status' => 'enable'
            ]);
            if($tmpQrcodes->isEmpty()){
                $code ='400';
                $comment ='this qrcode delete';
                $this->failResponse($comment,$code);
            }

            $expireSec = $tmpQrcodes['0']->created_at->timestamp +$tmpQrcodes['0']->expire_time;
            if($expireSec < $now->timestamp){
                $code ='404';
                $comment ='link expire or no link';
                $this->failResponse($comment,$code);
            }

         $qrcode =$tmpQrcodes->toArray();

        //掃描數加一
        $num_scan = $qrcode[0]['num_scan']+1;
        $tmpQrcodes = $this->qrcodesRepository->update(
            ['num_scan' =>$num_scan],
            $qrcode[0]['id']
        );
        $comment ='Link ok';
        $this->successResponse($comment,true);

    }

    /** 列出我的所有qrcode
     *  Desc:列出我的所有qrcode
     *  參考輸入範例
     * /qrcodes/list
        {
        }
     */
    public function list(Request $request){
        $this->changeRead();
        //own check
        $userArr = $this->baseFuncService->getSessionUserArr($request);
        //end check own

        $tmpQrcodes = $this->qrcodesRepository->findWhere([
            'creater_id' =>$userArr['0']['id'],
            'parent_type' =>'blog',
            'status' => 'enable'
            ]);

        //get regArr
        $tmpQrcodes->each(function ($item, $key) {
            if($item->RegisterArr->isNotEmpty()){
                //$item->RegisterArr->first()->user_id;
                $item->RegisterArr->each(function ($item1, $key1) {
                        $item1->Profile = $this->userProfileRepository->findWhere([
                        'user_id' =>$item1->user_id
                        ]);
                        $item1->userCheck = $this->userCheckRepository->findWhere([
                            'user_id' =>$item1->user_id,
                            'option' =>'recruiter',
                            'status' =>'checked'
                            ]);

                            if($item1->Profile->isNotEmpty()){
                                $item1->email =  $item1->Profile->first()->email;
                            }else{
                                $item1->email =  '';
                            }
                            if($item1->userCheck->isNotEmpty()){
                                $item1->recruiter = true;
                            }else{
                                $item1->recruiter = false;
                            }
                });




            }
        });

        $qrcode =$tmpQrcodes->toArray();
        foreach($qrcode as $key =>$value){
            foreach($value['register_arr'] as $key1 =>$value1){
                 unset($qrcode[$key]['register_arr'][$key1]['Profile']);
                 unset($qrcode[$key]['register_arr'][$key1]['userCheck']);
                 $qrcode[$key]['register_arr'] = $this->baseFuncService->idToString($qrcode[$key]['register_arr'],$this->tmpRegisterIdArr);
            }
        }

            $qrcode =$this->qrcodeService->getImageLink($qrcode);

            $qrcode = $this->baseFuncService->idToString($qrcode,$this->tmpIdArr);
            $comment ='return qrcodelist info';
            $this->successResponse($comment,$qrcode);
    }

    /** 列出我的所有在這group 裡的 qrcode
     *  Desc:列出我的所有group 裡的 qrcode
     *  參考輸入範例
     * /qrcodes/list
        {
        }
     */
    public function groupList($groupId,Request $request){
        $this->changeRead();
        //own check
        $userArr = $this->baseFuncService->getSessionUserArr($request);
        //end check own

        $tmpQrcodes = $this->qrcodesRepository->findWhere([
            'creater_id' =>$userArr['0']['id'],
            'parent_id' =>$groupId,
            'parent_type' =>'group',
            'status' => 'enable'
            ]);
            $qrcode =$tmpQrcodes->toArray();
            $qrcode =$this->qrcodeService->getImageLink($qrcode);
            $qrcode = $this->baseFuncService->idToString($qrcode,$this->tmpIdArr);
            $comment ='return qrcodelist info';
            $this->successResponse($comment,$qrcode);
    }


    /** 寫入note
     *  Desc:寫入note
     *  參考輸入範例
     * /updatenote/{id}
        {
            "note":"test"
        }
     */
    public function updateNote($id,Request $request){
        $note = $request->input('note');
        $code ='400';
        $data ='';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code,$data);
        }
        if($note == null){
            $comment ='note is empty';
            $this->failResponse($comment,$code);
        }
        //checkOwnId
        $this->checkOwnId($request);
        //end checkOwnId
        $tmp = $this->qrcodesRepository->findWhere([
            'id' =>$id,
            'status' => 'enable'
            ]);
        $tmpArr =$tmp->toArray();
        if(count($tmpArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        if($id !=null){
            $tmpQrcodes = $this->qrcodesRepository->update(
                ['note' =>$note],
                $id
            );
            $data='';
            $comment ='update note down';
            $this->successResponse($comment,$data);
        }
    }

    /** 刪除qrcode
     *  Desc:刪除qrcode
     *  參考輸入範例
     * /qrcodes/delete/{id}
        {
        }
     */
    public function delete($id,Request $request){

        $code ='400';
        $data ='';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code,$data);
        }

        $this->checkOwnId($request);
        $tmp = $this->qrcodesRepository->findWhere([
            'id' =>$id
            ]);
        $tmpArr =$tmp->toArray();
        if(count($tmpArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }
        if($id !=null){
            $filedArr =null;
            $filedArr['status']='delete';
            $tmpQrcodes = $this->qrcodesRepository->update(
                $filedArr,
                $id
            );
            $data='';
            $comment ='delete down';
            $this->successResponse($comment,$data);
        }




    }

    public function findIdInfo($id,Request $request){
        $this->changeRead();
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }

        //先抓出要回傳的資料
        $qrcodes = $this->qrcodesRepository->findWhere([
            'id' =>$id,
            'status' => 'enable'
            ]);

        $qrcodesArr = $qrcodes->toArray();

        //群組已經刪除
        if(count($qrcodesArr) == 0 ){
            $code = 401;
            $comment = 'qrcodes not fund or delete';
            $this->failResponse($comment, $code);
        }
        //end
        //回傳資料
        $comment ='return qrcodes info';
        $qrcodesArr = $this->baseFuncService->idToString($qrcodesArr,$this->tmpIdArr);
        $this->successResponse($comment,$qrcodesArr);

    }


    //轉換為讀取資料庫
    private function changeRead(){
        $this->qrcodesRepository->changeRead();
    }

    //check qrcodes id
    private function checkOwnId($request){
        //get seesion 不能為空
        $code = 400;
        $sessionCode  = $request->header('session');
        if($sessionCode == null){
            $comment ='session is empty';
            $this->failResponse($comment,$code);
        }
            //確認session 沒有過期
        $session = $this->sessionRepository->findByCode($sessionCode);
                if ($session->isEmpty()) {
                    $code = 404;
                    $comment = 'session error';
                    $this->failResponse($comment, $code);
                } // END if

            //owner_id
        $seesionArr =$session->toArray();
        $owner_id = $seesionArr['0']['owner_id'];


            //抓取account
        $user = $this->userRepository->findWhere([
            'id' =>$owner_id
            ]);
        if (count($user) ==0) {
            $code = 404;
            $comment = 'user error';
            $this->failResponse($comment, $code);
        } // END if
        $userArr =$user->toArray();
            //學生不能產生qrcodes
        if($userArr['0']['type'] =='member'){
            $code = 401;
            $comment = 'type no permissions';
            $this->failResponse($comment, $code);
        }



        //抓取qrcodes id
        $tmpQrcodes = $this->qrcodesRepository
        ->findWhere([
            'creater_id' =>$userArr['0']['id']
            ]);

        $qrcodesArr = $tmpQrcodes->toArray();

        if(count($qrcodesArr) == 0){
            $code ='401';
            $data='';
            $comment ='this qrcodes id not own this user';
            $this->failResponse($comment,$code,$data);
        }
    }

}