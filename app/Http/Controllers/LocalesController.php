<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\LocalesRepositoryEloquent;
use App\Repositories\LocalesContentRepositoryEloquent;

class LocalesController extends Controller
{
    use JsonResponseTrait;

    protected $localesRepository, $localesContentRepository;


    public function __construct()
    {
        $this->userRepository    = app(UserRepositoryEloquent::class);
        $this->sessionRepository    = app(SessionRepositoryEloquent::class);

        $this->localesRepository    = app(LocalesRepositoryEloquent::class);
        $this->localesContentRepository = app(LocalesContentRepositoryEloquent::class);
    } // END function


    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function delete(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $locales = $this->localesRepository->findById($id);

        if ($locales->isEmpty()) {
            $code = 404;
            $comment = 'locales error';

            $this->failResponse($comment, $code);
        } // END if


        $this->localesRepository->delete($id);

        $locales = $this->localesRepository->findById($id);

        if ($locales->isNotEmpty()) {
            $code = 422;
            $comment = 'delete fail';

            $this->failResponse($comment, $code);
        } // END if


        $localesContents = $this->localesContentRepository->findByLocalesId($id);

        if ($localesContents->isEmpty()) {
            $code = 403;
            $comment = 'locales contents error';

            $this->failResponse($comment, $code);
        } // END if

        $this->localesContentRepository->deleteWhere(['locales_id' => $id]);

        $localesContents = $this->localesContentRepository->findByLocalesId($id);

        if ($localesContents->isNotEmpty()) {
            $code = 422;
            $comment = 'delete content fail';

            $this->failResponse($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode, 'locales_id' => strval($id)];

        $this->successResponse('delete success', $resultData);
    } // END function

    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function update(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $locales = $this->localesRepository->findById($id);

        if ($locales->isEmpty()) {
            $code = 404;
            $comment = 'locales error';

            $this->failResponse($comment, $code);
        } // END if


        $code = $request->input('code');
        $note = $request->input('note');

        if (empty($code)) {
            $code = 400;
            $comment = 'code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $locales = $this->localesRepository->findByCode($code);

        if ($locales->isNotEmpty() AND $locales->first()->id != $id) {
            $code = 404;
            $comment = 'code ALREADY exist';

            $this->failResponse($comment, $code);
        } // END if

        $contents = [];
        $langs = config('locales.CURRENT_LOCALES_LANGS');

        foreach ($langs as $currentLang) {

            $lang = $request->input('lang_' . $currentLang);
            $content = $request->input('content_' . $currentLang);

            if (empty($lang) OR empty($content)) continue;
            if ($lang != $currentLang) continue;

            // $isCodeLangExist = $this->localesRepository->isCodeLangExist($code, $lang);
            // if (!empty($isCodeLangExist)) continue;

            $contents[$lang] = $content;

        } // END foreach

        if (empty($contents)) {
            $code = 400;
            $comment = 'contents empty';

            $this->failResponse($comment, $code);
        } // END if

        $user = $this->userRepository->findByToken($token);

        $updateData = [
            'code' => $code,
            'note' => $note,
            'creater_id' => $user->first()->id
        ];

        $this->localesRepository->update($updateData, $id);

        $locales = $this->localesRepository->findById($id);

        if ($locales->isEmpty()) {
            $code = 422;
            $comment = 'update locales  fail';

            $this->failResponse($comment, $code);
        } // END if

        $this->localesContentRepository->deleteWhere(['locales_id' => $locales->first()->id]);

        foreach ($contents as $lang => $content) {
            $insertData = [
                'locales_id' => $locales->first()->id,
                'lang'    => $lang,
                'content' => $content,
                'creater_id' => $user->first()->id
            ];
            $this->localesContentRepository->create($insertData);
        } // END foreach

        $this->makeFiles();

        $resultData = ['session' => $sessionCode, 'locales_id' => strval($locales->first()->id)];

        $this->successResponse('update success', $resultData);
    } // END function


    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function create(Request $request)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $code = $request->input('code');
        $note = $request->input('note');

        if (empty($code)) {
            $code = 400;
            $comment = 'code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $isCodeExist = $this->localesRepository->isCodeExist($code);

        if (!empty($isCodeExist)) {
            $code = 404;
            $comment = 'code ALREADY exist';

            $this->failResponse($comment, $code);
        } // END if

        $contents = [];
        $langs = config('locales.CURRENT_LOCALES_LANGS');

        foreach ($langs as $currentLang) {

            $lang = $request->input('lang_' . $currentLang);
            $content = $request->input('content_' . $currentLang);

            if (empty($lang) OR empty($content)) continue;
            if ($lang != $currentLang) continue;

            $isCodeLangExist = $this->localesRepository->isCodeLangExist($code, $lang);
            if (!empty($isCodeLangExist)) continue;

            $contents[$lang] = $content;

        } // END foreach

        if (empty($contents)) {
            $code = 400;
            $comment = 'contents is empty';

            $this->failResponse($comment, $code);
        } // END if

        $user = $this->userRepository->findByToken($token);

        $insertData = [
            'code' => $code,
            'note' => $note,
            'creater_id' => $user->first()->id
        ];
        $this->localesRepository->create($insertData);

        $locales = $this->localesRepository->findByCode($code);

        if ($locales->isEmpty()) {
            $code = 422;
            $comment = 'create locales fail';

            $this->failResponse($comment, $code);
        } // END if

        foreach ($contents as $lang => $content) {
            $insertData = [
                'locales_id' => $locales->first()->id,
                'lang'    => $lang,
                'content' => $content,
                'creater_id' => $user->first()->id
            ];
            $this->localesContentRepository->create($insertData);
        } // END foreach

        $this->makeFiles();

        $resultData = ['session' => $sessionCode, 'locales_id' => strval($locales->first()->id)];

        $this->successResponse('create success', $resultData);
    } // END function


    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function findLangs(Request $request)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $resultData = ['session' => $sessionCode, 'langs' => config('locales.CURRENT_LOCALES_LANGS')];

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function findByLangAndCode(Request $request, $lang, $code)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if

        if (!in_array($lang, config('locales.CURRENT_LOCALES_LANGS'))) {
            $code = 404;
            $comment = 'lang error';

            $this->failResponse($comment, $code);
        } // END if


        $isCodeLangExist = $this->localesRepository->isCodeLangExist($code, $lang);

        if (empty($isCodeLangExist)) {
            $code = 404;
            $comment = 'code and lang NOT exist';

            $this->failResponse($comment, $code);
        } // END if


        $locales = $this->localesRepository->findByCode($code);

        $localesData = (object) array_map('strval', $locales->first()->toArray());
        $localesData->contents = [array_map('strval', $locales->first()->Contents->toArray()[0])];

        $resultData = ['session' => $sessionCode, 'locales' => $localesData];

        $this->successResponse('find success', $resultData);
    } // END function

    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function findByCode(Request $request, $code)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($code)) {
            $code = 400;
            $comment = 'code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $isCodeExist = $this->localesRepository->isCodeExist($code);

        if (empty($isCodeExist)) {
            $code = 404;
            $comment = 'code NOT exist';

            $this->failResponse($comment, $code);
        } // END if


        $locales = $this->localesRepository->findByCode($code);

        $localesData = (object) array_map('strval', $locales->first()->toArray());

        foreach ($locales->first()->Contents as $content) {
            $localesData->contents[] = array_map('strval', $content->toArray());
        } // END foreach

        $resultData = ['session' => $sessionCode, 'locales' => $localesData];

        $this->successResponse('find success', $resultData);
    } // END function

    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function findByLang(Request $request, $lang)
    {
        $token = $request->header('token');
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if

        if (!in_array($lang, config('locales.CURRENT_LOCALES_LANGS'))) {
            $code = 404;
            $comment = 'lang error';

            $this->failResponse($comment, $code);
        } // END if


        $isLangExist = $this->localesContentRepository->isLangExist($lang);

        if (empty($isLangExist)) {
            $code = 404;
            $comment = 'lang NOT exist';

            $this->failResponse($comment, $code);
        } // END if


        $localesIds = [];

        $localesContents = $this->localesContentRepository->findByLang($lang);

        foreach ($localesContents->all() as $localesContent) {
            $locales = $localesContent->Locales;
            if ($locales->get()->isEmpty()) {continue;}

            array_push($localesIds, $locales->id);
        } // END foreach

        $finalData = [];

        foreach ($localesIds as $localesId) {

            $locales = $this->localesRepository->findById($localesId);

            $localesData = (object) array_map('strval', $locales->first()->toArray());

            foreach ($locales->first()->Contents as $content) {
                $localesData->contents[] = array_map('strval', $content->toArray());
            } // END foreach

            array_push($finalData, $localesData);
        } // END foreach

        $resultData = ['session' => $sessionCode, 'locales' => $finalData];

        $this->successResponse('find success', $resultData);
    } // END function


    /**
     * Find the blog By id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function makeFilesByLang(Request $request, $lang)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($lang)) {
            $code = 400;
            $comment = 'lang is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if

        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);
        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        if ($lang != 'all') {
            if (!in_array($lang, config('locales.CURRENT_LOCALES_LANGS'))) {
                $code = 404;
                $comment = 'lang error';

                $this->failResponse($comment, $code);
            } // END if


            $isLangExist = $this->localesContentRepository->isLangExist($lang);

            if (empty($isLangExist)) {
                $code = 404;
                $comment = 'lang NOT exist';

                $this->failResponse($comment, $code);
            } // END if
        } // END if

        $lang = $lang == 'all' ? NULL : $lang;
        $this->makeFiles($lang);

        $resultData = ['session' => $sessionCode];

        $this->successResponse('make success', $resultData);
    } // END function



    private function makeFiles($lang = NULL)
    {

        $langs = empty($lang) ? config('locales.CURRENT_LOCALES_LANGS') : [$lang];

        foreach ($langs as $lang) {

            $localesIds = [];

            $localesContents = $this->localesContentRepository->findByLang($lang);

            foreach ($localesContents->all() as $localesContent) {
                $locales = $localesContent->Locales;
                if ($locales->get()->isEmpty()) {continue;}

                array_push($localesIds, $locales->id);
            } // END foreach

            $finalData = [];

            foreach ($localesIds as $localesId) {

                $locales = $this->localesRepository->findById($localesId);

                $localesContent = $this->localesContentRepository->findByLocalesIdAndLang($localesId, $lang);

                $finalData[$locales->first()->code] = urlencode(strval($localesContent->first()->content));

            } // END foreach

            $localesFilename = 'locales_' . $lang . '.json';
            $localesContent = urldecode(json_encode($finalData));

            Storage::disk('locales')->put($localesFilename, $localesContent);

            Storage::disk('s3-locales')->put('json/' . $localesFilename, $localesContent, 'public');
        } // END foreach


    } // END function


} // END class
