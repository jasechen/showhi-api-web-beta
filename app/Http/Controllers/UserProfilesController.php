<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Traits\ImageToolTrait;
use App\Traits\JsonResponseTrait;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserProfileRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\BlogRepositoryEloquent;
use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\FileRepositoryEloquent;

class UserProfilesController extends Controller
{
    use JsonResponseTrait, ImageToolTrait;

    protected $userRepository, $userProfileRepository;
    protected $sessionRepository;
    protected $blogRepository;
    protected $directoryRepository, $fileRepository;


    public function __construct()
    {
        $this->userRepository = app(UserRepositoryEloquent::class);
        $this->userProfileRepository = app(UserProfileRepositoryEloquent::class);

        $this->sessionRepository = app(SessionRepositoryEloquent::class);

        $this->blogRepository  = app(BlogRepositoryEloquent::class);
        $this->directoryRepository = app(DirectoryRepositoryEloquent::class);
        $this->fileRepository = app(FileRepositoryEloquent::class);
    } // END function


    /**
     * Update user profile
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $id
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function update(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if


        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'enable') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if


        $userProfile = $this->userProfileRepository->findByUserId($id);

        if ($userProfile->isEmpty()) {
            $code = 404;
            $comment = 'user profile error';

            $this->failResponse($comment, $code);
        } // END if


        $firstName = $request->input('first_name');
        $lastName  = $request->input('last_name');
        $nickname  = $request->input('nickname');

        $gender = $request->input('gender');
        $birth  = $request->input('birth');

        $city    = $request->input('city');
        $intro   = $request->input('intro');
        $website = $request->input('website');

        $languages = $request->input('languages');
        $interests = $request->input('interests');
        $countries = $request->input('countries');
        $pets = $request->input('pets');


        $update_fields = [];

        if (!empty($firstName)) {
            $update_fields['first_name'] = $firstName;
        } // END if

        if (!empty($lastName)) {
            $update_fields['last_name'] = $lastName;
        } // END if

        if (!empty($nickname)) {

            $nicknameUserProfile = $this->userProfileRepository->findByNickname($nickname);

            if ($nicknameUserProfile->isNotEmpty() AND $nicknameUserProfile->first()->id != $userProfile->first()->id) {
                $code = 404;
                $comment = 'nickname ALREADY exist';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['nickname'] = $nickname;
        } // END if

        if (!empty($gender)) {
            $genderValidator = Validator::make(['gender' => $gender],
                ['gender' => ['in:'.implode(',', config('genders'))]]
            );

            if ($genderValidator->fails()) {
                $code = 403;
                $comment = 'gender error';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['gender'] = $gender;
        } // END if

        if (!empty($birth)) {
            $birthValidator = Validator::make(['birth' => $birth],
                ['birth' => ['before:10 years ago']]
            );

            if ($birthValidator->fails()) {
                $code = 403;
                $comment = 'birth error';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['birth'] = $birth;
        } // END if

        if (!empty($city)) {
            $update_fields['city'] = $city;
        } // END if

        if (!empty($intro)) {
            $update_fields['intro'] = $intro;
        } // END if

        if (!empty($website)) {

            if (!filter_var($website, FILTER_VALIDATE_URL)) {
                $code = 403;
                $comment = 'website error';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['website'] = $website;
        } // END if

        if (!empty($languages)) {
            $isValidLanguage = true;
            foreach ($languages as $language) {
                $languageValidator = Validator::make(['language' => $language],
                    ['language' => ['in:'.implode(',', config('languages'))]]
                );

                if ($languageValidator->fails()) {
                    $isValidLanguage = false;
                    break;
                } // END if
            } // END foreach

            if (empty($isValidLanguage)) {
                $code = 403;
                $comment = 'language error';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['languages'] = serialize($languages);
        } // END if

        if (!empty($interests)) {
            $isValidInterest = true;
            foreach ($interests as $interest) {
                $interestValidator = Validator::make(['interest' => $interest],
                    ['interest' => ['in:'.implode(',', config('interest'))]]
                );

                if ($interestValidator->fails()) {
                    $isValidInterest = false;
                    break;
                } // END if
            } // END foreach

            if (empty($isValidInterest)) {
                $code = 403;
                $comment = 'interest error';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['interest_items'] = serialize($interests);
        } // END if

        if (!empty($countries)) {
            $isValidCountry = true;
            foreach ($countries as $country) {
                $countryValidator = Validator::make(['country' => $country],
                    ['country' => ['in:'.implode(',', config('country'))]]
                );

                if ($countryValidator->fails()) {
                    $isValidCountry = false;
                    break;
                } // END if
            } // END foreach

            if (empty($isValidCountry)) {
                $code = 403;
                $comment = 'country error';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['countries_visited'] = serialize($countries);
        } // END if

        if (!empty($pets)) {
            $isValidPet = true;
            foreach ($pets as $pet) {
                $petValidator = Validator::make(['pet' => $pet],
                    ['pet' => ['in:'.implode(',', config('pets'))]]
                );

                if ($petValidator->fails()) {
                    $isValidPet = false;
                    break;
                } // END if
            } // END foreach

            if (empty($isValidPet)) {
                $code = 403;
                $comment = 'pet error';

                $this->failResponse($comment, $code);
            } // END if

            $update_fields['pets'] = serialize($pets);
        } // END if


        if (empty($update_fields)) {
            $code = 400;
            $comment = 'update fields empty';

            $this->failResponse($comment, $code);
        } // END if


        $result = $this->userProfileRepository->update($update_fields, $userProfile->first()->id);

        if (empty($result)) {
            $code = 422;
            $comment = 'update error';

            $this->failResponse($comment, $code);
        } // END if


        $resultData = ['session' => $sessionCode];

        $this->successResponse('update success', $resultData);
    } // END function


    /**
     * Upload avatar file of the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response\Json
     */
    public function uploadAvatar(Request $request, $id)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if

        $token  = $request->header('token');

        if (empty($token)) {
            $code = 400;
            $comment = 'token is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($id)) {
            $code = 400;
            $comment = 'id is empty';

            $this->failResponse($comment, $code);
        } // END if

        if (empty($request->hasFile('avatar'))) {
            $code = 400;
            $comment = 'avatar is empty';

            $this->failResponse($comment, $code);
        } // END if

        $isAlive = $this->sessionRepository->isAlive($sessionCode);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session expire time';

            $this->failResponse($comment, $code);
        } // END if


        $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

        if (empty($isValid)) {
            $code = 403;
            $comment = 'session token error';

            $this->failResponse($comment, $code);
        } // END if


        $user = $this->userRepository->findById($id);

        if ($user->isEmpty()) {
            $code = 404;
            $comment = 'user error';

            $this->failResponse($comment, $code);
        } // END if

        if ($user->first()->status != 'enable') {
            $code = 403;
            $comment = 'user status error';

            $this->failResponse($comment, $code);
        } // END if

        $avatarUploadFile = $request->file('avatar');

        if (empty($avatarUploadFile->isValid())) {
            $code = 403;
            $comment = 'avatar error';

            $this->failResponse($comment, $code);
        } // END if


        $blog = $this->blogRepository->findByOwnerId($id);

        $filename = $this->convertImage($avatarUploadFile, $blog->first()->id, 'blog', 'avatar');

        $fileExtension = $avatarUploadFile->extension();
        $fileMimeType  = $avatarUploadFile->getMimeType();
        $fileSize      = $avatarUploadFile->getSize();
        $fileOriginalName = $avatarUploadFile->getClientOriginalName();

        $avatarDir  = $this->directoryRepository->findByParentTypeAndParentIdAndTypeAndOwnerId('blog', $blog->first()->id, 'avatar', $user->first()->id);

        if ($avatarDir->isEmpty()) {
            $code = 422;
            $comment = 'create dir error';

            $this->failResponse($comment, $code);
        } // END if

        $avatarFile = $this->fileRepository->initImageFile($avatarDir->first()->id, 0, $filename, $fileExtension, $fileMimeType, $fileSize, true, $user->first()->id, $fileOriginalName);

        if ($avatarFile->isEmpty()) {
            $code = 422;
            $comment = 'create file error';

            $this->failResponse($comment, $code);
        } // END if


        $profile = $this->userProfileRepository->findByUserId($user->first()->id);
        $this->userProfileRepository->update(['avatar' => $filename], $profile->first()->id);


        $avatarPaths = $this->getImageLinks($filename);

        foreach ($avatarPaths as $avatarPath) {
            $sourceFilename = base_path('public'.$avatarPath);
            $targetFolder   = mb_substr(pathinfo($avatarPath, PATHINFO_DIRNAME), 9);
            $targetFilename = pathinfo($avatarPath, PATHINFO_BASENAME);

            $sourceFilename = new File($sourceFilename);
            Storage::disk('s3-gallery')->putFileAs($targetFolder, $sourceFilename, $targetFilename, 'public');
        } // END foreach


        $resultData = [
            'session' => $sessionCode,
            'avatar_links' => $avatarPaths
        ];

        $this->successResponse('upload avatar success', $resultData);
    } // END function


} // END class
