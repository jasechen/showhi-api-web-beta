<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Traits\JsonResponseTrait;
use App\Repositories\DeviceRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;

class SessionsController extends Controller
{
    use JsonResponseTrait;

    protected $deviceRepository;
    protected $sessionRepository;


    public function __construct()
    {
        $this->deviceRepository  = app(DeviceRepositoryEloquent::class);
        $this->sessionRepository = app(SessionRepositoryEloquent::class);
    } // END function


    /**
     * 初始化 session
     *
     * @param   string                  $device_code
     *          string                  $device_os
     *          string                  $device_type
     *          string                  $device_lang
     *          string                  $token
     *
     * @return  string                  $device_code
     *          string                  $session
     *
     */
    public function init(Request $request)
    {
        $deviceCode  = $request->input('device_code');
        $deviceOS    = $request->input('device_os', config('device.DEFAULT_DEVICE_OS'));
        $deviceType  = $request->input('device_type', config('device.DEFAULT_DEVICE_TYPE'));
        $deviceLang  = $request->input('device_lang', config('device.DEFAULT_DEVICE_LANG'));
        $deviceToken = $request->input('device_token');

        $remoteAddress = $request->ip();

        $deviceCode = empty($deviceCode) ? $this->deviceRepository->initCode() : $deviceCode;

        if (!in_array($deviceOS, config('device.DEVICE_OS'))) {
            $code = 404;
            $comment = 'device os error';

            $this->failResponse($comment, $code);
        } // END if

        if (!in_array($deviceType, config('device.DEVICE_TYPES'))) {
            $code = 404;
            $comment = 'device type error';

            $this->failResponse($comment, $code);
        } // END if

        if (!in_array($deviceLang, config('device.DEVICE_LANGS'))) {
            $code = 404;
            $comment = 'device lang error';

            $this->failResponse($comment, $code);
        } // END if


        $device = $this->deviceRepository->findByCode($deviceCode);

        if ($device->isEmpty()) {
            $device = $this->deviceRepository->init($deviceCode, $deviceOS, $deviceType, $deviceLang, $deviceToken);
        } // END if


        $session = $this->sessionRepository->findByDeviceId($device->first()->id);

        if ($session->isEmpty()) {
            $session = $this->sessionRepository->init($device->first()->id, $remoteAddress);
            $request->session()->setId($session->first()->code);
        } else {
            $sessionFields = ['remote_address' => $remoteAddress];

            $isAlive = $this->sessionRepository->isAlive($session->first()->code);

            if (empty($isAlive)) {
                $sessionFields['code'] = $this->sessionRepository->initCode();
                $request->session()->setId($sessionFields['code']);
            } // END if

            $this->sessionRepository->update($sessionFields, $session->first()->id);
            $session = $this->sessionRepository->findByDeviceId($device->first()->id);
        } // END if else


        $resultData = ['device_code' => $device->first()->code, 'session' => $session->first()->code];

        $this->successResponse('init success', $resultData);
    } // END function


    /**
     * 查詢 session
     *
     * @param   string                  $code
     *
     * @return  string                  $session
     *          string                  $status
     *
     */
    public function findByCode($code)
    {
        $isAlive = $this->sessionRepository->isAlive($code);

        if (empty($isAlive)) {
            $code = 403;
            $comment = 'session is NOT alive';

            $this->failResponse($comment, $code);
        } // END if


        $session = $this->sessionRepository->findByCode($code);


        $resultData = [
            'session' => $session->first()->code,
            'status'  => $session->first()->status
        ];

        $this->successResponse('fetch success', $resultData);
    } // END function
} // END class
