<?php namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Traits\JsonResponseTrait;
use App\Traits\ImageToolTrait;
use App\Repositories\NoticesRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\BaseFuncService;

class NoticesController extends Controller {
    use JsonResponseTrait,ImageToolTrait;
    public function __construct(){
        $this->baseFuncService = new BaseFuncService;
        $this->noticesRepository  = app(NoticesRepositoryEloquent::class);
        $this->sessionRepository  = app(SessionRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
        $this->tmpIdArr =['id','sender_id','receiver_id','parent_id'];

    }

    /** 建立並且發送 一個notices 訊息
     *  Desc:建立並且發送 一個notices訊息
     *  參考輸入範例
     * /Notices/create
        {
            "sender_id":"14380081212428288",
            "receiver_id":"17204493187944448",
            "content":"hello",
            "parentType":"group",
            "parentId":"15079001701879808"
        }
     */
    public function create(Request $request){
        //不能用 $request 應該是要用類似tools 才對
        $tmpArr =['senderId','receiverId','content','parentType','parentId'];
        $filedArr = $request->all();
        //迴圈空值判斷
        $this->baseFuncService->checkEmptyFiled($filedArr,$tmpArr);
        //end 迴圈空直判斷

        $senderId = $filedArr['senderId'];
        $receiverId =  $filedArr['receiverId'];
        $content = $filedArr['content'];
        $parentType = $filedArr['parentType'];
        $parentId = $filedArr['parentId'];

        //存進資料庫
            $Notices = $this->noticesRepository->init($senderId,$receiverId,
            $content,$parentType,$parentId);
            $NoticesArr[] = $Notices->toArray();
            $comment ='return create notices info';
            $NoticesArr = $this->baseFuncService->idToString($NoticesArr,$this->tmpIdArr);
            $this->successResponse($comment,$NoticesArr);

    }

    /** 列出訊息
     *  Desc:訊息
     *  參考輸入範例
     * /Notices/{parentType}/list/{status}
     * status=all,readed,noreaded
        {
            "parentType":"blog",
            "status":"all",
        }
     */
    public function list($parentType,$status,Request $request){
        $this->changeRead();
        $code ='400';
        //checkSession
        $userId = $this->getSessionUserid($request);
        //end checkSession
        if($status =='all'){
            $whereFiledArr['parent_type'] =$parentType;
            $whereFiledArr['receiver_id']= $userId;
            $orWhereFiledArr['status']= ['init','readed'];
        }else{
            $whereFiledArr['parent_type'] =$parentType;
            $whereFiledArr['receiver_id']= $userId;
            $orWhereFiledArr['status']= [$status];
        }

        //l5 原生 laravel 寫法
        $Notices = $this->noticesRepository->scopeQuery(function($query) use ($whereFiledArr,$orWhereFiledArr) {
            return $query->where($whereFiledArr)
            ->where(function($query) use ($orWhereFiledArr)
            {
                foreach($orWhereFiledArr as $key => $value){
                    foreach($value as $key1 => $value2){
                        $query->orWhere($key,$value2);
                    }

                }
            });
        })->all();

        $NoticesArr = $Notices->toArray();
        //id 轉文字
        //this->baseFunc->idToString($NoticesArr,$tmpArr)
        //IdtoString
        $NoticesArr =$this->baseFuncService->idToString($NoticesArr,$this->tmpIdArr);
        //end IdtoString
        //end
            $comment ='return notices info';
            $this->successResponse($comment,$NoticesArr);
    }

    /** 使用者刪除訊息
     *  Desc:使用者刪除訊息
     *  參考輸入範例
     * /Notices/delete/{id}
        {
        }
     */
    public function delete($id,Request $request){

        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->checkSession($request);
        //end checkSession

        $whereFiledArr =null;
        $whereFiledArr['id'] =$id;
        $whereFiledArr['receiver_id'] =$userId;
        $orWhereFiledArr['status'] =['init','readed'];

        $tmp = $this->noticesRepository->scopeQuery(function($query) use ($whereFiledArr,$orWhereFiledArr) {
            return $query->where($whereFiledArr)
            ->where(function($query) use ($orWhereFiledArr)
            {
                foreach($orWhereFiledArr as $key => $value){
                    foreach($value as $key1 => $value2){
                        $query->orWhere($key,$value2);
                    }

                }
            });
        })->all();

        $tmpArr =$tmp->toArray();

        if(count($tmpArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }
        if($id !=null){
            $filedArr['status']='delete';
            $groups = $this->noticesRepository->update(
                $filedArr,
                $id
            );
            $data='';
            $comment ='delete down';
            $this->successResponse($comment,$data);
        }
    }
    /** 使用者查詢訊息
     *  Desc:使用者查詢訊息
     *  參考輸入範例
     * /Notices/{id}
        {
        }
     */
    public function findIdInfo($id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->checkSession($request);
        //end checkSession

        $whereFiledArr =null;
        $whereFiledArr['id'] =$id;
        $whereFiledArr['receiver_id'] =$userId;
        $orWhereFiledArr['status'] =['init','readed'];

        $tmp = $this->noticesRepository->scopeQuery(function($query) use ($whereFiledArr,$orWhereFiledArr) {
            return $query->where($whereFiledArr)
            ->where(function($query) use ($orWhereFiledArr)
            {
                foreach($orWhereFiledArr as $key => $value){
                    foreach($value as $key1 => $value2){
                        $query->orWhere($key,$value2);
                    }

                }
            });
        })->all();

        $tmpArr =$tmp->toArray();

        if(count($tmpArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }else{
            $comment ='return notices info';
            $tmpArr = $this->baseFuncService->idToString($tmpArr,$this->tmpIdArr);
            $this->successResponse($comment,$tmpArr);
        }
    }

    /** 已讀訊息記錄
     *  Desc:已讀訊息記錄
     *  參考輸入範例
     * /groups/readed/{id}
        {
        }

     */
    public function readed($id,Request $request){

        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->checkSession($request);
        //end checkSession

        $tmp = $this->noticesRepository->findWhere([
            'id' =>$id,
            'receiver_id' =>$userId,
            'status' =>'init'
            ]);
        $tmpArr =$tmp->toArray();
        if(count($tmpArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }
        if($id !=null){
            $filedArr['status']='readed';
            $groups = $this->noticesRepository->update(
                $filedArr,
                $id
            );
            $data='';
            $comment ='update down';
            $this->successResponse($comment,$data);
        }

    }

    //轉換為讀取資料庫
    private function changeRead(){
        $this->noticesRepository->changeRead();
    }


    //確認session沒有過期 並且傳回 user_id
    private function checkSession($request){

        //get session 不能為空
        $code = 400;
        $sessionCode  = $request->header('session');
        if($sessionCode == null){
            $comment ='session is empty';
            $this->failResponse($comment,$code);
        }
            //確認session 沒有過期
        $session = $this->sessionRepository->findByCode($sessionCode);
                if ($session->isEmpty()) {
                    $code = 404;
                    $comment = 'session error';
                    $this->failResponse($comment, $code);
                } // END if

            //抓取create_id
        $seesionArr =$session->toArray();
        $owner_id = $seesionArr['0']['owner_id'];
        //上面常用判斷
        return $owner_id;
    }

    private function getSessionUserId($request){
                //get session 不能為空
                $code = 400;
                $sessionCode  = $request->header('session');
                if($sessionCode != null){
                    //確認session 沒有過期
                    $session = $this->sessionRepository->findByCode($sessionCode);
                    if ($session->isEmpty()) {
                        $code = 404;
                        $comment = 'session error';
                        $this->failResponse($comment, $code);
                    } // END if

                        //抓取create_id
                        $seesionArr =$session->toArray();
                        $owner_id = $seesionArr['0']['owner_id'];
                    //上面常用判斷
                    return $owner_id;
                }
                return '';

    }

}