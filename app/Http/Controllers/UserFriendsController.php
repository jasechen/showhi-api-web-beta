<?php namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Services\BaseFuncService;
use App\Traits\ImageToolTrait;
use App\Traits\JsonResponseTrait;

use App\Repositories\UserFriendsRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;

class UserFriendsController extends Controller {
    use JsonResponseTrait ,ImageToolTrait;
    protected $userRepository;
    protected $groupsRepository;
    protected $groupUsersRepository;

    public function __construct(){
        $this->userFriendsRepository  = app(UserFriendsRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
        $this->baseFuncService = new BaseFuncService;
        $this->tmpIdArr =['id','user_id','friend_id'];
    }

    /** 邀請好友
     *  Desc:邀請好友
     *  參考輸入範例
     * /userFriends/create
        {
            "friend_id":"19098383511851008"
        }
     */
    public function create(Request $request){
        $tmpArr =['friend_id'];
        $filedArr = $request->all();
        // 迴圈空值判斷
        $this->baseFuncService->checkEmptyFiled($filedArr,$tmpArr);
        // end 迴圈空直判斷
        // end input
        // own check
        $userArr = $this->baseFuncService->getSessionUserArr($request);

        //判斷user 是否存在
        $user = $this->userRepository->findWhere([
            'id' =>$userArr['0']['id']
            ]);
             if($user->isEmpty()){
                $code ='403';
                $comment ='no this user';
                $this->failResponse($comment,$code);
             }

        //判斷是否有加好友過
        //拉出兩筆後判斷是否有一筆有enable
            $nextWhereFiledArr['user_id']= $filedArr['friend_id'];
            $nextWhereFiledArr['friend_id']= $userArr['0']['id'];
            $whereFiledArr['user_id']= $userArr['0']['id'];
            $whereFiledArr['friend_id']= $filedArr['friend_id'];

            //判斷是否有加過好友
            $userfriends = $this->userFriendsRepository->scopeQuery(function($query) use ($whereFiledArr,$nextWhereFiledArr) {
                return
                $query
                ->where(function($query) use ($whereFiledArr)
                {
                    $query->where($whereFiledArr);
                })
                ->orWhere(function($query) use ($nextWhereFiledArr)
                {
                    $query->where($nextWhereFiledArr);
                });
            })->all();

             if(count($userfriends) > 2){
                $code ='401';
                $comment ='Rows Error';
                $this->failResponse($comment,$code);
             }
             //有加過不等於0
            if(count($userfriends) !=0){
                $userfriends = $this->userFriendsRepository->scopeQuery(function($query) use ($whereFiledArr,$nextWhereFiledArr) {
                    return
                    $query->where('status', '=', 'delete')
                    ->where(function($query) use ($whereFiledArr)
                    {
                        $query->where($whereFiledArr);
                    })
                    ->orWhere(function($query) use ($nextWhereFiledArr)
                    {
                        $query->where($nextWhereFiledArr);
                    });
                })->all();

                if(count($userfriends) > 2 || count($userfriends)  == 0){
                    $code ='401';
                    $comment ='Rows Error';
                    $this->failResponse($comment,$code);
                 }

                 if(!$userfriends->isEmpty()){
                    $userfriendsArr = $userfriends->toArray();
                    foreach($userfriendsArr as $key =>$value){
                        //取得要變動的好友
                        if($value['user_id'] == $whereFiledArr['user_id']){
                            $id = $value['id'];
                            //使用update
                            $dbHave = true;
                            break;
                        }
                    }
                 }

            }

        //end判斷是否有加好友
        //end check own
        unset($userArr['0']['password']);
        if(empty($dbHave))
        $dbHave =false;
        //存進資料庫
        //如果db沒有就是新增
        if(!$dbHave){
            $filedArr['user_id'] =$userArr['0']['id'];
            $filedArr['status']='init';
            $userFriends = $this->userFriendsRepository->create($filedArr);
        }else{
            $filedArr['status']='init';
            $userFriends = $this->userFriendsRepository->update($filedArr,$id);
        }
            $data = [
                'id' =>(string) $userFriends->id,
                'user_id' =>(string) $userFriends->user_id,
                'friend_id' =>(string) $userFriends->friend_id,
                'status' => $userFriends->status,
                'updated_at' => $userFriends->updated_at,
                'created_at' => $userFriends->created_at,
            ];
            //回傳資料
            $comment ='return create friend info';
            $this->successResponse($comment,$data);

    }

    /** 同意加為好友 agreeFriends
     *  Desc:同意加為好友 agreeFriends
     *  參考輸入範例
     * /userFriends/agreeFriends/{id}
        {
        }
     */
    public function agreeFriends($id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        $status = $request->input('status');
        $code ='400';
        if($status == null){
            $comment ='status is empty';
            $this->failResponse($comment,$code);
        }

        $tmpList ="enable,delete";
            if(!$this->baseFuncService->checkTypeFiled($status,$tmpList)){
                $code ='403';
                $comment ='status is error';
                $this->failResponse($comment,$code);
            }
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

            $WhereFiledArr['id']= $id;
            $WhereFiledArr['user_id']= $userId;
            $userfriends = $this->userFriendsRepository->scopeQuery(function($query) use ($WhereFiledArr) {
                return $query->where($WhereFiledArr);
            })->all();

            if($userfriends->isEmpty()){
                $code ='401';
                $comment ='no this row';
                $this->failResponse($comment,$code);
            }

            if($userfriends->first()->status !='init'){
                $code ='401';
                $comment ='Status Error';
                $this->failResponse($comment,$code);
            }
            $filedArr['status'] =$status;
        if($id !=null){
            $userFriends = $this->userFriendsRepository->update(
                $filedArr,
                $id
            );
            $data='';
            $comment ='update down';
            $this->successResponse($comment,$data);
        }

    }

    /** 列出自己所有好友
     *  Desc:列出自己所有好友
     *  參考輸入範例
     * /userFriends/list/{status}
        {
        }
     */
    public function list($status,Request $request){
        $this->changeRead();
        $code ='400';
        if($status == null){
            $comment ='status is empty';
            $this->failResponse($comment,$code);
        }

        $tmpList ="init,enable";
            if(!$this->baseFuncService->checkTypeFiled($status,$tmpList)){
                $code ='403';
                $comment ='status is error';
                $this->failResponse($comment,$code);
            }


        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        if($status =='init'){
        $userfriends = $this->userFriendsRepository->findWhere([
            'status' =>$status,
            'user_id' =>$userId
            ]);
        }else{
            //雙向好友
            $orWhereFiledArr['user_id']= [$userId];
            $orWhereFiledArr['friend_id']= [$userId];

            $userFriends = $this->userFriendsRepository->scopeQuery(function($query) use ($orWhereFiledArr,$status) {
                return
                $query->where(['status' =>$status])
                ->where(function($query) use ($orWhereFiledArr)
                {
                    foreach($orWhereFiledArr as $key => $value){
                        foreach($value as $key1 => $value2){
                            $query->orWhere($key,$value2);
                        }
                    }
                });
            })->all();
        }
        //get avatar
        $userFriends->each(function ($item, $key) use ($userId) {
            if($userId ==$item->user_id){
                $item->avatar = $item->friendIdProfile->first()->avatar;
                $item->last_name = $item->friendIdProfile->first()->last_name;
                $item->first_name = $item->friendIdProfile->first()->first_name;
                $item->nick_name = $item->friendIdProfile->first()->nick_name;
            }
            else{
                $item->avatar = $item->userIdProfile->first()->avatar;
                $item->last_name = $item->userIdProfile->first()->last_name;
                $item->first_name = $item->userIdProfile->first()->first_name;
                $item->nick_name = $item->userIdProfile->first()->nick_name;
            }
        });

        $userFriendsArr = $userFriends->toArray();
        foreach($userFriendsArr as $key =>$value){
            if($userId ==$value['user_id'])
                unset($userFriendsArr[$key]['friend_id_profile']);
            else
                unset($userFriendsArr[$key]['user_id_profile']);

            $userFriendsArr[$key]['avatar_links'] = empty($userFriendsArr[$key]['avatar']) ? [] : $this->getImageLinks($userFriendsArr[$key]['avatar']);

        }
        //end get avatar

        $comment ='return friends list';

        $userFriendsArr = $this->baseFuncService->idToString($userFriendsArr,$this->tmpIdArr);
        $this->successResponse($comment,$userFriendsArr);

    }

    /** 刪除好友
     *  Desc:刪除好友
     *  參考輸入範例
     * /userFriends/delete/{id}
        {
        }
     */
    public function delete($id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        if($id !=null){
            $filedArr['status'] ='delete';
            $userfriends = $this->userFriendsRepository->update(
                $filedArr,
                $id
            );
            $data='';
            $comment ='delete down';
            $this->successResponse($comment,$data);
        }
    }

    /** 查詢 一個好友資訊
     *  Desc:查詢 一個好友資訊
     *  參考輸入範例
     * /userFriends/{id}
        {
        }
     */
    public function findIdInfo($id,Request $request){
        $this->changeRead();
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //先抓出要回傳的資料
        $userFriends = $this->userFriendsRepository->findWhere([
            'id' =>$id
            ]);
        $userFriendsArr = $userFriends->toArray();
        //群組已經刪除
        if($userFriendsArr['0']['status'] =='delete'){
            $code = 401;
            $comment = 'userFriends not fund or delete';
            $this->failResponse($comment, $code);
        }
        if($userFriendsArr['0']['status'] =='disable'){
            $code = 401;
            $comment = 'userFriends is disable';
            $this->failResponse($comment, $code);
        }

        //get avatar
        $userFriends->each(function ($item, $key) use ($userId) {
            if($userId ==$item->user_id)
                $item->avatar = $item->friendIdProfile->first()->avatar;
            else
                $item->avatar = $item->userIdProfile->first()->avatar;
        });

        $userFriendsArr = $userFriends->toArray();
        foreach($userFriendsArr as $key =>$value){
            if($userId ==$value['user_id'])
                unset($userFriendsArr[$key]['friend_id_profile']);
            else
                unset($userFriendsArr[$key]['user_id_profile']);

            $userFriendsArr[$key]['avatar_links'] = empty($userFriendsArr[$key]['avatar']) ? [] : $this->getImageLinks($userFriendsArr[$key]['avatar']);

        }
        //end get avatar

        //回傳資料
        $comment ='return userFriends info';
        $userFriendsArr = $this->baseFuncService->idToString($userFriendsArr,$this->tmpIdArr);
        $this->successResponse($comment,$userFriendsArr);

    }

    //轉換為讀取資料庫
    private function changeRead(){
        $this->userFriendsRepository->changeRead();
    }

}