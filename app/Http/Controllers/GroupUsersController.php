<?php namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Traits\JsonResponseTrait;
use App\Traits\ImageToolTrait;

use App\Repositories\GroupsRepositoryEloquent;
use App\Repositories\GroupUsersRepositoryEloquent;
use App\Repositories\NoticesRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\PermissionRepositoryEloquent;

use App\Repositories\RoleRepositoryEloquent;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\BaseFuncService;
use App\Services\GroupService;

class GroupUsersController extends Controller {
    use JsonResponseTrait,ImageToolTrait;
    public function __construct(){
        $this->groupsRepository  = app(GroupsRepositoryEloquent::class);
        $this->groupUsersRepository  = app(GroupUsersRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
        $this->noticesRepository  = app(NoticesRepositoryEloquent::class);
        $this->roleRepository = app(RoleRepositoryEloquent::class);
        $this->permissionRepository = app(PermissionRepositoryEloquent::class);

        $this->baseFuncService = new BaseFuncService;
        $this->groupService = new GroupService;
        $this->tmpIdArr =['id','group_id','user_id','recruiter_id','allower_id','role_id'];
    }


    /** 新增group user
     *  Desc:新增group user 要有管理權才行
     *  參考輸入範例
     * /groupUsers/create
        {
            "group_id": "15079001701879808",
            "account": "mzjian@showhi.co"
        }
     */
    public function create(Request $request){
        $groupId = $request->input('groupId');
        $account = $request->input('account');
        $code ='400';
        $data ='';
        if($groupId == null){
            $comment ='group id is empty';
            $this->failResponse($comment,$code,$data);
        }
        if($account == null){
            $comment ='account id is empty';
            $this->failResponse($comment,$code,$data);
        }
        //own check
        $SessionUserArr = $this->baseFuncService->getSessionUserArr($request);
        //end check own

        //get user
        $userArr = $this->baseFuncService->getUserByAcoount($account);
        unset($userArr['password']);
        //get user

        $groupUserArr = $this->groupService->getGroupUser($groupId,$userArr[0]['id']);
        if(count($groupUserArr) >1){
            $comment ='DB Error';
            $code =401;
            $this->failResponse($comment,$code);
        }


        $tmp = $this->groupsRepository->findWhere([
            'id' =>$groupId
            ]);
        $groupArr = $tmp->toArray();

        if(count($groupArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        //end input

        //抓取role_id
        $tmpArr = $this->roleRepository->findByTypeAndName('group','member');
        $role_id = $tmpArr->first()->id;
        $tmpArr =null;

        //有筆數改用update
        if(count($groupUserArr) ==1){
            if($groupUserArr[0]['status'] == 'enable' || $groupUserArr[0]['status'] == 'init'){
                $comment ='group have this User';
                $code =401;
                $this->failResponse($comment,$code);
            }else{
                //新增user for update 重置之前刪除的那筆user
                $filedArr['status'] ='init';
                $filedArr['recruiter_id'] =$userArr[0]['id'];
                $filedArr['allower_id'] =0;
                $filedArr['role_id'] =$role_id;
                $tmp = $this->groupUsersRepository->update(
                    $filedArr,
                    $groupUserArr['0']['id']
                );
                $tmpArr[] =$tmp->toArray();
                $comment ='return groupUser Info';
                //notice
                $parentType ='group';
                $parentId =$groupId;
                $content =$groupArr['0']['title'].' group '.config('notices.NOTICES_INVITES').' your';
                $Notices = $this->noticesRepository->init($SessionUserArr['0']['id'],$userArr[0]['id'],$content,$parentType,$parentId);
                //end notice

                //傳回groupUser info
                $tmpArr = $this->baseFuncService->idToString($tmpArr,$this->tmpIdArr);
                $this->successResponse($comment,$tmpArr);
            }
        }



        //存進資料庫
            $groupUsersFields = [
                                'group_id' => $groupId,
                                'user_id' => $userArr[0]['id'],
                                'role_id' => $role_id,
                                'status' => 'init',
                                'meeting_enable' => false,
                                'live_enable' => false,
                                'recruiter_id' => $userArr[0]['id'],
                                'allower_id' => 0,
                            ];
            $groupUsers = $this->groupUsersRepository->create($groupUsersFields);
            $groupUsersArr[] =$groupUsers->toArray();

                //notice
                $parentType ='group';
                $parentId =$groupId;
                $content =$groupArr['0']['title'].' group '.config('notices.NOTICES_INVITES').' your';
                $Notices = $this->noticesRepository->init($SessionUserArr['0']['id'],$userArr[0]['id'],$content,$parentType,$parentId);
                //end notice

            $groupUsersArr = $this->baseFuncService->idToString($groupUsersArr,$this->tmpIdArr);
            $comment ='return create groupUser info';
            $this->successResponse($comment,$groupUsersArr);
    }

    /** 加入group
     *  Desc:加入group
     *  參考輸入範例
     * /groupUsers/join
        {
            "group_id": "15079001701879808"
        }
     */
    public function join(Request $request){

        $groupId = $request->input('groupId');
        $code ='400';
        $data ='';
        //member_create
        if($groupId == null){
            $comment ='group id is empty';
            $this->failResponse($comment,$code,$data);
        }
        //own check
        $SessionUserId = $this->baseFuncService->getSessionUserid($request);
        $SessionUserArr = $this->userRepository->findWhere([
            'id' =>$SessionUserId
            ]);
        //end check own

        //get group
        $tmp = $this->groupsRepository->findWhere([
            'id' =>$groupId
            ]);
        $groupArr = $tmp->toArray();

        if(count($groupArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }
        //end get group

        $groupUserArr = $this->groupService->getGroupUser($groupId,$SessionUserArr['0']['id']);
        //get user
        $userArr = $SessionUserArr;
        unset($userArr['password']);
        //get user
        //end input

        //抓取role_id
        $tmpArr = $this->roleRepository->findByTypeAndName('group','member');
        $role_id = $tmpArr->first()->id;
        $tmpArr =null;

        //抓取roleArr 發送通知
        $permission = $this->permissionRepository->findByTypeAndName('group','member_create');
        $roleArr = $permission->first()->Roles->toArray();
        foreach($roleArr as $key => $value){
            $roleIdArr[] = $value['id'];
        }


        //有筆數改用update
        if(count($groupUserArr) ==1){
            if($groupUserArr[0]['status'] == 'enable' || $groupUserArr[0]['status'] == 'init'){
                // echo $groupUserArr[0]['status'];
                // exit;
                $comment ='group have this User';
                $code =401;
                $this->failResponse($comment,$code);
            }else{
                //新增user for update 重置之前刪除的那筆user
                $filedArr['status'] ='init';
                $filedArr['recruiter_id'] =0;
                $filedArr['allower_id'] =0;
                $filedArr['role_id'] =$role_id;
                $tmp = $this->groupUsersRepository->update(
                    $filedArr,
                    $groupUserArr['0']['id']
                );
                $tmpArr[] = $tmp->toArray();

                //notice
                    //取得該發通知的管理層
                    $whereFiledArr['group_id'] =$groupId;
                    $whereFiledArr['status']= 'enable';
                    $orWhereFiledArr['role_id']=$roleIdArr;
                    //l5 原生 laravel 寫法
                    $groupAllUsers = $this->groupUsersRepository->scopeQuery(function($query) use ($whereFiledArr,$orWhereFiledArr) {
                        return $query->where($whereFiledArr)
                        ->where(function($query) use ($orWhereFiledArr)
                        {
                            foreach($orWhereFiledArr as $key => $value){
                                foreach($value as $key1 => $value2){
                                    $query->orWhere($key,$value2);
                                }

                            }
                        });
                    })->all();
                    $groupAllUsersArr = $groupAllUsers->toArray();
                    //end 取得該發通知的管理層
                    $content =$groupArr['0']['title'].config('notices.NOTICES_JOIN').'your group';
                    //迴圈發送
                    $parentType ='group';
                    $parentId =$groupId;
                    foreach($groupAllUsersArr as $key =>$value){
                        $Notices = $this->noticesRepository->init(
                            $userArr[0]['id']
                            ,$value['user_id'],$content,$parentType,$parentId);
                    }
                    //end 迴圈發送
                //end notice

                $comment ='return groupUser Info';
                //傳回groupUser info
                $tmpArr = $this->baseFuncService->idToString($tmpArr,$this->tmpIdArr);
                $this->successResponse($comment,$tmpArr);
            }
        }

        //存進資料庫
            $groupUsersFields = [
                                'group_id' => $groupId,
                                'user_id' => $userArr[0]['id'],
                                'role_id' => $role_id,
                                'status' => 'init',
                                'meeting_enable' => 'disable',
                                'live_enable' => 'disable',
                                'recruiter_id' => 0,
                                'allower_id' => 0,
                            ];
            $groupUsers = $this->groupUsersRepository->create($groupUsersFields);

                //notice
                    //取得該發通知的管理層
                    $whereFiledArr['group_id'] =$groupId;
                    $whereFiledArr['status']= 'enable';
                    $orWhereFiledArr['role_id']= $roleIdArr;
                    //todo 發送 以role id 為主
                    //l5 原生 laravel 寫法
                    $groupAllUsers = $this->groupUsersRepository->scopeQuery(function($query) use ($whereFiledArr,$orWhereFiledArr) {
                        return $query->where($whereFiledArr)
                        ->where(function($query) use ($orWhereFiledArr)
                        {
                            foreach($orWhereFiledArr as $key => $value){
                                foreach($value as $key1 => $value2){
                                    $query->orWhere($key,$value2);
                                }

                            }
                        });
                    })->all();
                    $groupAllUsersArr = $groupAllUsers->toArray();
                    //end 取得該發通知的管理層
                    $content =$groupArr['0']['title'].config('notices.NOTICES_JOIN').'your group';
                    //迴圈發送
                    foreach($groupAllUsersArr as $key =>$value){
                        $Notices = $this->noticesRepository->init(
                            $userArr[0]['id']
                            ,$value['user_id'],$content,$parentType,$parentId);
                    }
                    //end 迴圈發送
                //end notice

            $groupUsersArr[] = $groupUsers->toArray();
            $comment ='return create groupUser info';
            $groupUsersArr = $this->baseFuncService->idToString($groupUsersArr,$this->tmpIdArr);
            $this->successResponse($comment,$groupUsersArr);
    }

    /** 列出這個group all users
     *  Desc:列出這個group all users
     *  參考輸入範例
     * /groupUsers/{groupId}/{id}
        {
            "groupId":"15079001701879808",
            "id":"15079001735434240",
        }
     */
    public function list($id,Request $request){
        //role 不能為空
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        $this->changeRead();
        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //先抓出要回傳的資料
        $groupUsers = $this->groupUsersRepository->findWhere([
            'status' =>'enable',
            'group_id' =>$id
            ]);
        //get role
        $groupUsers->each(function ($item, $key) {
            $item->avatar = $item->Profile->first()->avatar;
            $item->last_name = $item->Profile->first()->last_name;
            $item->first_name = $item->Profile->first()->first_name;
            $item->nick_name = $item->Profile->first()->nick_name;
            $item->role = $item->Roles->first()->name;
        });

        $groupUsersArr = $groupUsers->toArray();
        foreach($groupUsersArr as $key =>$value){
            unset($groupUsersArr[$key]['profile']);
            unset($groupUsersArr[$key]['roles']);
            $groupUsersArr[$key]['avatar_links'] = empty($groupUsersArr[$key]['avatar']) ? [] : $this->getImageLinks($groupUsersArr[$key]['avatar']);

        }
        //end get role

        $groupUsersArr = $this->baseFuncService->idToString($groupUsersArr,$this->tmpIdArr);

        //群組已經刪除
        //群組關閉只有成員看得到
        //end
            $comment ='return groupUsers list';
            $this->successResponse($comment,$groupUsersArr);
    }

    /** 列出這個group all users
     *  Desc:列出這個group all users
     *  參考輸入範例
     * /groupUsers/{groupId}/{id}
        {
            "role":"admin"
        }
     */

    public function role($groupId,$id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        if($groupId == null){
            $comment ='groupId is empty';
            $this->failResponse($comment,$code);
        }

        //get update filed
        $filedArr = $request->all();
        if(count($filedArr) == 0){
            $comment ='no update filed data';
            $this->failResponse($comment,$code);
        }
        $tmpList ="admin,recruiter,editor,member";
        if(array_key_exists('role',$filedArr)){
            if(!$this->baseFuncService->checkTypeFiled($filedArr['role'],$tmpList)){
                $code ='403';
                $comment ='role is error';
                $this->failResponse($comment,$code);
            }
        }else{
            $code ='403';
            $comment ='role is error';
            $this->failResponse($comment,$code);
        }


        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        //checkGroupUser
        $SessionUserArr = $this->groupService->getGroupUser($groupId,$userId);
        //end checkGroupUser

        $tmp = $this->groupUsersRepository->findWhere([
            'id' =>$id
            ]);
        $roleName = $tmp->first()->Roles->first()->name;
        $userArr = $tmp->toArray();

        //todo 改偵測 role_id
        if($roleName =='owner'){
            $code = 401;
            $comment = "Owner can't change";
            $this->failResponse($comment, $code);
        }

        if(count($userArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }
        //end check
        //抓取role_id
        $tmpArr = $this->roleRepository->findByTypeAndName('group',$filedArr['role']);
        $role_id = $tmpArr->first()->id;
        $filedArr['role_id'] = $role_id;
        $tmpArr =null;
        if($id !=null){
            $groups = $this->groupUsersRepository->update(
                $filedArr,
                $id
            );

            //抓取group
            $tmp = $this->groupsRepository->findWhere([
                'id' =>$groupId
                ]);
            $groupArr = $tmp->toArray();

            if(count($groupArr) !=1){
                $comment ='no this row';
                $this->failResponse($comment,$code);
            }

            //notice
            $parentType ='group';
            $parentId =$groupId;
            $content =$groupArr['0']['title'].' group change '.' your role ';
            $Notices = $this->noticesRepository->init(
                $SessionUserArr['0']['id'],
                $userArr[0]['user_id'],
                $content,
                $parentType,
                $parentId
            );
            //end notice

            $data='';
            $comment ='update down';
            $this->successResponse($comment,$data);
        }

    }
    /** 預約帳號許可
     *  Desc:預約帳號許可
     *  參考輸入範例
     * /groupUsers/{groupId}/meetingEnable/{id}
        {
            "meeting_enable":true
        }
     */
    public function meetingEnable($groupId,$id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        if($groupId == null){
            $comment ='groupId is empty';
            $this->failResponse($comment,$code);
        }

        //get update filed
        $filedArr = $request->all();
        if(count($filedArr) == 0){
            $comment ='no update filed data';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        //checkGroupUser
        $SessionUserArr = $this->groupService->getGroupUser($groupId,$userId);
        //end checkGroupUser

        $tmp = $this->groupsRepository->findWhere([
            'id' =>$groupId
            ]);
        $groupArr = $tmp->toArray();

        if(count($groupArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        $tmp = $this->groupUsersRepository->findWhere([
            'id' =>$id
            ]);
        $userArr = $tmp->toArray();
        if(count($userArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        if($id !=null){
            $groups = $this->groupUsersRepository->update(
                $filedArr,
                $id
            );

            //notice
            $parentType ='group';
            $parentId =$groupId;
            $content =$groupArr['0']['title'].' group change '.' your meeting Enable ';
            $Notices = $this->noticesRepository->init(
                $SessionUserArr['0']['id'],
                $userArr[0]['user_id'],
                $content,
                $parentType,
                $parentId
            );
            //end notice

            $data='';
            $comment ='update down';
            $this->successResponse($comment,$data);
        }

    }
    /** 直播帳號的許可
     *  Desc:直播帳號的許可
     *  參考輸入範例
     * /groupUsers/{groupId}/live_enable/{id}
        {
            "live_enable":true
        }
     */
    public function liveEnable($groupId,$id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        if($groupId == null){
            $comment ='groupId is empty';
            $this->failResponse($comment,$code);
        }

        //get update filed
        $filedArr = $request->all();
        if(count($filedArr) == 0){
            $comment ='no update filed data';
            $this->failResponse($comment,$code);
        }

        

        $tmp = $this->groupsRepository->findWhere([
            'id' =>$groupId
            ]);
        $groupArr = $tmp->toArray();

        if(count($groupArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //checkGroupUser
        $SessionUserArr = $this->groupService->getGroupUser($groupId,$userId);
        //end checkGroupUser


        $tmp = $this->groupUsersRepository->findWhere([
            'id' =>$id
            ]);
        $userArr = $tmp->toArray();
        if(count($userArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        if($id !=null){
            $groups = $this->groupUsersRepository->update(
                $filedArr,
                $id
            );

            //notice
            $parentType ='group';
            $parentId =$groupId;
            $content =$groupArr['0']['title'].' group change '.' your live Enable ';
            $Notices = $this->noticesRepository->init(
                $SessionUserArr['0']['id'],
                $userArr[0]['user_id'],
                $content,
                $parentType,
                $parentId
            );
            //end notice

            $data='';
            $comment ='update down';
            $this->successResponse($comment,$data);
        }
    }

    /** 允許一個人加入群組
     *  Desc:允許一個人加入群組
     *  參考輸入範例
     * /groupUsers/{groupId}/allow/{id}
        {
            "groupId":"15079001701879808",
            "id":"15079001735434240",
            "status":"enable"
        }
     */
    public function allow($groupId,$id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        if($groupId == null){
            $comment ='groupId is empty';
            $this->failResponse($comment,$code);
        }

        //get update filed
        $filedArr = $request->all();
        if(count($filedArr) == 0){
            $comment ='no update filed data';
            $this->failResponse($comment,$code);
        }

        $tmpList ="enable,disable";
        if(array_key_exists('status',$filedArr)){
            if(!$this->baseFuncService->checkTypeFiled($filedArr['status'],$tmpList)){
                $code ='403';
                $comment ='status is error';
                $this->failResponse($comment,$code);
            }
        }else{
            $code ='403';
            $comment ='status is error';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        //checkGroupUser
        $SessionUserArr = $this->groupService->getGroupUser($groupId,$userId);
        //end checkGroupUser

        $tmp = $this->groupUsersRepository->findWhere([
            'id' =>$id
            ]);
        $userArr = $tmp->toArray();
        if(count($userArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }
        if($userArr['0']['status'] !='init'){
            $comment ='this id Status not init';
            $this->failResponse($comment,$code);
        }

        if($id !=null){
            $filedArr['allow_id'] =$userId;
            $groups = $this->groupUsersRepository->update(
                $filedArr,
                $id
            );

            $tmp = $this->groupsRepository->findWhere([
                'id' =>$groupId
                ]);
            $groupArr = $tmp->toArray();

            if(count($groupArr) !=1){
                $comment ='no this row';
                $this->failResponse($comment,$code);
            }

            //notice
            $parentType ='group';
            $parentId =$groupId;
            $content =$groupArr['0']['title'].' group allow '.' your.';
            $Notices = $this->noticesRepository->init(
                $SessionUserArr['0']['id'],
                $userArr[0]['user_id'],
                $content,
                $parentType,
                $parentId
            );
            //end notice

            $data='';
            $comment ='Allow down';
            $this->successResponse($comment,$data);
        }

    }

    /** 把一個人設定為owner，原owner 變admin
     *  Desc:把一個人設定為owner，原owner 變admin
     *  參考輸入範例
     * /groupUsers/{groupId}/changeOwner/{id}
        {
            "groupId":"15079001701879808",
            "id":"15079001735434240",
            "status":"enable"
        }
     */
    public function changeOwner($groupId,$id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        if($groupId == null){
            $comment ='groupId is empty';
            $this->failResponse($comment,$code);
        }

        //get update filed
        $filedArr = $request->all();
        if(count($filedArr) == 0){
            $comment ='no update filed data';
            $this->failResponse($comment,$code);
        }

        //todo check user is school or
        $tmpList ="owner";
        if(array_key_exists('role',$filedArr)){
            if(!$this->baseFuncService->checkTypeFiled($filedArr['role'],$tmpList)){
                $code ='403';
                $comment ='role is error';
                $this->failResponse($comment,$code);
            }
        }else{
                $code ='403';
                $comment ='role is error';
                $this->failResponse($comment,$code);
        }


        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //checkGroupUser
        $SessionUserArr = $this->groupService->getGroupUser($groupId,$userId);
        //end checkGroupUser

        $tmp = $this->groupUsersRepository->findWhere([
            'id' =>$id,
            'group_id' =>$groupId
            ]);
        $userArr = $tmp->toArray();
        if(count($userArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        $tmpArr = $this->roleRepository->findByTypeAndName('group','owner');
        $role_owner_id = $tmpArr->first()->id;
        $tmpArr = $this->roleRepository->findByTypeAndName('group','admin');
        $role_admin_id = $tmpArr->first()->id;
        $tmpArr =null;
        $filedOwnerArr['role_id']=$role_owner_id;
        $filedAdminArr['role_id']=$role_admin_id;

        if($id !=null){
            //todo change group ownid
            $groups = $this->groupUsersRepository->update(
                $filedOwnerArr,
                $id
            );
            $groups = $this->groupUsersRepository->update(
                $filedAdminArr,
                $SessionUserArr['0']['id']
            );

            $groups = $this->groupsRepository->update(
                ['owner_id'=>$userArr['0']['user_id']],
                 $groupId
            );

            $tmp = $this->groupsRepository->findWhere([
                'id' =>$groupId
                ]);
            $groupArr = $tmp->toArray();

            //notice
            $parentType ='group';
            $parentId =$groupId;
            $content =$groupArr['0']['title'].' group give your owner.';
            $Notices = $this->noticesRepository->init(
                $SessionUserArr['0']['id'],
                $userArr[0]['user_id'],
                $content,
                $parentType,
                $parentId
            );
            //end notice

            $data='';
            $comment ='Change down';
            $this->successResponse($comment,$data);
        }

    }

    /** 刪除 Group Users
     *  Desc:刪除 Group Users
     *  參考輸入範例
     * /groupUsers/{groupId}/delete/{id}
        {
            "groupId":"15079001701879808"
            "id":"15079001735434240"
        }
     */
    public function delete($groupId,$id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        if($groupId == null){
            $comment ='groupId is empty';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        //checkGroupUser
        $SessionUserArr = $this->groupService->getGroupUser($groupId,$userId);
        //end checkGroupUser

        $tmp = $this->groupsRepository->findWhere([
            'id' =>$groupId
            ]);
        $groupArr = $tmp->toArray();

        if(count($groupArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        $tmp = $this->groupUsersRepository->findWhere([
            'id' =>$id
            ]);
        $roleName = $tmp->first()->Roles->first()->name;
        $userArr = $tmp->toArray();
        if(count($userArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        if($roleName =='owner'){
            $code = 401;
            $comment = "Owner can't delete";
            $this->failResponse($comment, $code);
        }

        if($id !=null){
            $filedArr['status'] ='disable';
            $tmp = $this->groupUsersRepository->update(
                $filedArr,
                $id
            );

            //notice
            $parentType ='group';
            $parentId =$groupId;
            $content =$groupArr['0']['title'].' group remove your.';
            $Notices = $this->noticesRepository->init(
                $SessionUserArr['0']['id'],
                $userArr[0]['user_id'],
                $content,
                $parentType,
                $parentId
            );
            //end notice
            $data='';
            $comment ='delete down';
            $this->successResponse($comment,$data);
        }
    }

    /** 退出Group
     *  Desc:退出Group
     *  參考輸入範例
     * /groupUsers/{groupId}/delete/{id}
        {
            "groupId":"15079001701879808"
            "id":"15079001735434240"
        }
     */
    public function selfDelete($groupId,$id,Request $request){
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }
        if($groupId == null){
            $comment ='groupId is empty';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession
        //checkGroupUser
        $userArr = $this->groupService->checkGroupUser($groupId,$userId);
        //end checkGroupUser

        //get group
        $tmp = $this->groupsRepository->findWhere([
            'id' =>$groupId
            ]);
        $groupArr = $tmp->toArray();

        if(count($groupArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }
        //end get group

        if($userArr['0']['user_id'] !=$userId){
            $comment ='user Id different';
            $this->failResponse($comment,$code,$data);
        }

        $tmp = $this->groupUsersRepository->findWhere([
            'id' =>$id
            ]);
        $roleName = $tmp->first()->Roles->first()->name;

        //抓取roleArr 發送通知
        $permission = $this->permissionRepository->findByTypeAndName('group','member_create');
        $roleArr = $permission->first()->Roles->toArray();
        foreach($roleArr as $key => $value){
            $roleIdArr[] = $value['id'];
        }

        $tmpArr = $tmp->toArray();
        if(count($tmpArr) !=1){
            $comment ='no this row';
            $this->failResponse($comment,$code);
        }

        if($roleName =='owner'){
            $code = 401;
            $comment = "Owner can't delete";
            $this->failResponse($comment, $code);
        }

        if($id !=null){
            $filedArr['status'] ='disable';
            $tmp = $this->groupUsersRepository->update(
                $filedArr,
                $id
            );

                //notice
                    //取得該發通知的管理層
                    $whereFiledArr['group_id'] =$groupId;
                    $whereFiledArr['status']= 'enable';
                    $orWhereFiledArr['role_id']= $roleIdArr;
                    //l5 原生 laravel 寫法
                    $groupAllUsers = $this->groupUsersRepository->scopeQuery(function($query) use ($whereFiledArr,$orWhereFiledArr) {
                        return $query->where($whereFiledArr)
                        ->where(function($query) use ($orWhereFiledArr)
                        {
                            foreach($orWhereFiledArr as $key => $value){
                                foreach($value as $key1 => $value2){
                                    $query->orWhere($key,$value2);
                                }

                            }
                        });
                    })->all();
                    $groupAllUsersArr = $groupAllUsers->toArray();
                    //end 取得該發通知的管理層
                    $content =$groupArr['0']['title'].config('notices.NOTICES_REMOVE').'your group';
                    //迴圈發送
                    $parentType ='group';
                    $parentId =$groupId;
                    foreach($groupAllUsersArr as $key =>$value){
                        $Notices = $this->noticesRepository->init(
                            $userArr[0]['id']
                            ,$value['user_id'],$content,$parentType,$parentId);
                    }
                    //end 迴圈發送
                //end notice

            $data='';
            $comment ='delete down';
            $this->successResponse($comment,$data);
        }
    }

    /** 查詢某一個group 裡的 groupUsers
     *  Desc:查詢某一個group 裡的 groupUsers
     *  參考輸入範例
     * /groupUsers/{groupId}/delete/{id}
        {
            "groupId":"15079001701879808",
            "id":"15079001735434240",
        }
     */
    public function findIdInfo($id,Request $request){

        $this->changeRead();
        $code ='400';
        if($id == null){
            $comment ='id is empty';
            $this->failResponse($comment,$code);
        }

        //checkSession
        $userId = $this->baseFuncService->getSessionUserid($request);
        //end checkSession

        //先抓出要回傳的資料

        $groupUsers = $this->groupUsersRepository->findWhere(
            ['id' => $id]
        );

        //get role
        $groupUsersArr = $groupUsers->toArray();
        $groupUsers->each(function ($item, $key) {
             $item->role = $item->Roles->first()->name;
        });

        $groupUsersArr = $groupUsers->toArray();
        foreach($groupUsersArr as $key =>$value){
            unset($groupUsersArr[$key]['roles']);
        }
        //end get role
        if(count($groupUsersArr) ==0){
            $code = 401;
            $comment = 'group User not fund or delete';
            $this->failResponse($comment, $code);
        }
        //群組已經刪除
        if($groupUsersArr['0']['status'] !='enable'){
            $code = 401;
            $comment = 'group User not fund or delete';
            $this->failResponse($comment, $code);
        }

        //群組關閉只有成員看得到
        //end
        //回傳資料
        $comment ='return group user info';
        $groupUsersArr = $this->baseFuncService->idToString($groupUsersArr,$this->tmpIdArr);
        $this->successResponse($comment,$groupUsersArr);

    }

    //轉換為讀取資料庫
    private function changeRead(){
        $this->groupsRepository->changeRead();
        $this->groupUsersRepository->changeRead();
    }

}