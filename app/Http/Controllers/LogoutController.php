<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Traits\JsonResponseTrait;
use App\Repositories\SessionRepositoryEloquent;

class LogoutController extends Controller
{
    use JsonResponseTrait;

    protected $sessionRepository;


    public function __construct()
    {
        $this->sessionRepository  = app(SessionRepositoryEloquent::class);
    } // END function


    /**
     * 使用者登出
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     */
    public function index(Request $request)
    {
        $sessionCode  = $request->header('session');

        if (empty($sessionCode)) {
            $code = 400;
            $comment = 'session code is empty';

            $this->failResponse($comment, $code);
        } // END if


        $token  = $request->header('token');

        if (!empty($token)) {
            $isValid = $this->sessionRepository->isValidCodeAndToken($sessionCode, $token);

            if (empty($isValid)) {
                $code = 403;
                $comment = 'session token error';

                $this->failResponse($comment, $code);
            } // END if
        } // END if


        $session = $this->sessionRepository->findByCode($sessionCode);

        if ($session->isNotEmpty()) {
            $sessionFields = ['status' => 'logout'];

            $this->sessionRepository->update($sessionFields, $session->first()->id);
        } // END if


        $request->session()->flush();
        $request->session()->getHandler()->destroy($sessionCode);


        $this->successResponse('logout success');
    } // END function
} // END class
