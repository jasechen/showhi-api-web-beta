<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Board extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "boards";

    protected $fillable = [
        'parent_type', 'parent_id', 'privacy_status', 'status', 'text_id', 'directory_id', 'has_file', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  One2many
     */
    public function Text()
    {
        return $this->hasOne('App\Entities\Text', 'board_id', 'id');
    } // END function
}
