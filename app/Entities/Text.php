<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Text extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "texts";

    protected $fillable = [
        'board_id', 'content', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function Board()
    {
        return $this->belongsTo('App\Entities\Board', 'board_id', 'id');
    } // END function
}
