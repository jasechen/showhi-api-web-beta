<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class UserEducationMajor extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_education_majors";

    protected $fillable = [
        'user_id', 'user_education_id',
        'content'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function Education()
    {
        return $this->belongsTo('App\Entities\UserEducation', 'user_education_id', 'id');
    } // END function
}
