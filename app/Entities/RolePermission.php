<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class RolePermission extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "role_permissions";

    protected $fillable = [
        'role_id', 'permission_id', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships

}
