<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class Qrcodes extends Model {

    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "qrcodes";

    protected $fillable = ['type', 'url', 'filename','parent_type','parent_id', 'expire_time', 'num_scan',
    'creater_id','created_at','updated_at','note','status'];

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];

    public function Profile()
    {
        return $this->hasOne('App\Entities\UserProfile', 'user_id', 'user_id');
    } // END function

    public function UserCheck()
    {
        return $this->hasOne('App\Entities\UserCheck', 'user_id', 'user_id');
    } // END function

    public function RegisterArr()
    {
        return $this->hasMany('App\Entities\UserRecruiter', 'qrcode_id', 'id');
    } // END function


}
