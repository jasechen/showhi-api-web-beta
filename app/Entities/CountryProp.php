<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class CountryProp extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "country_props";

    protected $fillable = [
        'code', 'calling_code', 'tld', 'lat', 'lng'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships

}
