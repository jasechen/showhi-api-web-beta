<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class UserRole extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_roles";

    protected $fillable = [
        'user_id', 'role_id', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships

}
