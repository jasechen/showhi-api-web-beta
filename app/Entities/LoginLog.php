<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class LoginLog extends Model
{
    use HasSnowflakePrimary;

    protected $connection = 'log';

    protected $table = "login_logs";

    protected $fillable = [
        'session_id', 'device_id', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
}
