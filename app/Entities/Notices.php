<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class Notices extends Model {

    use HasSnowflakePrimary;
    protected $connection = "mysql";

    protected $table = "notices";
    protected $fillable = ['sender_id', 'receiver_id', 'status', 'content','parent_type', 'parent_id',
    'created_at','updated_at'];

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];

}
