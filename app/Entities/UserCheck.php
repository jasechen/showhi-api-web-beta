<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class UserCheck extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_checks";

    protected $fillable = [
        'user_id', 'option', 'code',
        'status'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function User()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    } // END function
}
