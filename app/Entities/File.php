<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class File extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "files";

    protected $fillable = [
        'directory_id', 'board_id',
        'filename', 'extension', 'mime_type', 'size',
        'title', 'description', 'type',
        'privacy_status', 'status',
        'is_cover',
        'owner_id', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function Directory()
    {
        return $this->belongsTo('App\Entities\Directory', 'directory_id', 'id');
    } // END function
}
