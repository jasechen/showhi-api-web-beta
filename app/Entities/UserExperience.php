<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class UserExperience extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_experiences";

    protected $fillable = [
        'user_id', 'company', 'title', 'city', 'decription',
        'start_time', 'end_time', 'is_working',
        'privacy_status'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function User()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    } // END function
}
