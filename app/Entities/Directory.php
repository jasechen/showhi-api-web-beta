<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Directory extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "directories";

    protected $fillable = [
        'parent_type','parent_id',
        'board_id', 'type',
        'title', 'description', 'cover',
        'privacy_status', 'status',
        'is_album', 'is_default',
        'owner_id', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  One2many
     */
    public function Files()
    {
        return $this->hasMany('App\Entities\File', 'directory_id', 'id');
    } // END function
}
