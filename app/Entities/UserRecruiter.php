<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class UserRecruiter extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_recruiters";

    protected $fillable = [
        'user_id', 'recruiter_id', 'qrcode_id',
        'note'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function User()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    } // END function

    public function Recruiter()
    {
        return $this->belongsTo('App\Entities\User', 'recruiter_id', 'id');
    } // END function

    public function QRCode()
    {
        return $this->belongsTo('App\Entities\qrcode', 'qrcode_id', 'id');
    } // END function
}
