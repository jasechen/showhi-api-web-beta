<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Role extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "roles";

    protected $fillable = [
        'type', 'name', 'description', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  Many2many
     */
    public function Permissions()
    {
        return $this->belongsToMany('App\Entities\Permission', 'role_permissions', 'role_id', 'permission_id');
    } // END function

    /*
     *  many2Many
     */
    public function Users()
    {
        return $this->belongsToMany('App\Entities\User', 'user_roles', 'role_id', 'user_id');
    } // END function

    public function GroupUsers()
    {
        return $this->hasMany('App\Entities\GroupUsers', 'role_id', 'id');
    } // END function

}
