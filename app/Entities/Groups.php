<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class Groups extends Model {

    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "groups";

    protected $fillable = ['title','avatar','keywords','cover','type', 'description', 'owner_id',
    'creater_id','created_at','updated_at',
    'status','privacy_status','type',
];

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];

}
