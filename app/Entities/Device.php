<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Device extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "devices";

    protected $fillable = [
        'code', 'os', 'type', 'lang', 'token'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  One2one
     */
    public function Session()
    {
        return $this->hasOne('App\Entities\Session', 'device_id', 'id');
    } // END function
}
