<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Session extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "sessions";

    protected $fillable = [
        'code', 'device_id', 'api_token', 'status',
        'expire_time', 'remote_address', 'owner_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2One
     */
    public function Device()
    {
        return $this->belongsTo('App\Entities\Device');
    } // END function

    /*
     *  one2Many
     */
    public function Owner()
    {
        return $this->belongsTo('App\Entities\User', 'owner_id', 'id');
    } // END function
}
