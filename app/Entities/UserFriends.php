<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class UserFriends extends Model {

    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_friends";

    protected $fillable = ['id','avatar','user_id','friend_id', 'status',
    'updated_at','created_at',
];

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];

    public function userIdProfile()
    {
        return $this->hasOne('App\Entities\UserProfile', 'user_id', 'user_id');
    } // END function

    public function friendIdProfile()
    {
        return $this->hasOne('App\Entities\UserProfile', 'user_id', 'friend_id');
    } // END function

}
