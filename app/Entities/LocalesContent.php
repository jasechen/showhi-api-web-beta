<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class LocalesContent extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "locales_contents";

    protected $fillable = [
        'locales_id', 'lang', 'content', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function Locales()
    {
        return $this->belongsTo('App\Entities\Locales', 'locales_id', 'id');
    } // END function
}
