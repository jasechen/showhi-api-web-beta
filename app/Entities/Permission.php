<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Permission extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "permissions";

    protected $fillable = [
        'type', 'name', 'description', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  many2Many
     */
    public function Roles()
    {
        return $this->belongsToMany('App\Entities\Role', 'role_permissions', 'permission_id', 'role_id');
    } // END function
}
