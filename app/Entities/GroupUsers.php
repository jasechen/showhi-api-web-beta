<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Kra8\Snowflake\HasSnowflakePrimary;

class GroupUsers extends Model {

    use HasSnowflakePrimary;
    protected $connection = "mysql";

    protected $table = "group_users";
    protected $fillable = ['group_id', 'user_id','role_id', 'status','meeting_enable', 'live_enable',
    'recruiter_id','allower_id','created_at','updated_at'];

    protected $guarded = [];

    protected $dates = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    public function Roles()
    {
        return $this->hasMany('App\Entities\Role', 'id', 'role_id');
    } // END function

    public function Profile()
    {
        return $this->hasOne('App\Entities\UserProfile', 'user_id', 'user_id');
    } // END function

}
