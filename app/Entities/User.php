<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class User extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "users";

    protected $fillable = [
        'account', 'password', 'status', 'token', 'type', 'generate_qrcode', 'introducer_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  One2one
     */
    public function Profile()
    {
        return $this->hasOne('App\Entities\UserProfile', 'user_id', 'id');
    } // END function

    /*
     *  One2many
     */
    public function Sessions()
    {
        return $this->hasMany('App\Entities\Session', 'creater_id', 'id');
    } // END function

    public function Recruiters()
    {
        return $this->hasMany('App\Entities\UserRecruiter', 'user_id', 'id');
    } // END function

    public function Checks()
    {
        return $this->hasMany('App\Entities\UserCheck', 'user_id', 'id');
    } // END function

    public function Educations()
    {
        return $this->hasMany('App\Entities\UserEducation', 'user_id', 'id');
    } // END function

    public function Experiences()
    {
        return $this->hasMany('App\Entities\UserExperience', 'user_id', 'id');
    } // END function

    /*
     *  Many2many
     */
    public function Roles()
    {
        return $this->belongsToMany('App\Entities\Role', 'user_roles', 'user_id', 'role_id');
    } // END function

}
