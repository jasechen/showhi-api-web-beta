<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class UserProfile extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_profiles";

    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'nickname', 'avatar', 'gender', 'birth',
        'email', 'mobile_country_code', 'mobile_phone', 'city', 'intro', 'website',
        'languages', 'interest_items', 'countries_visited', 'pets'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2One
     */
    public function User()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    } // END function
}
