<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Blog extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "blogs";

    protected $fillable = [
        'intro', 'cover', 'status', 'owner_id', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
}
