<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class Locales extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "locales";

    protected $fillable = [
        'code', 'note', 'creater_id'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  One2many
     */
    public function Contents()
    {
        return $this->hasMany('App\Entities\LocalesContent', 'locales_id', 'id');
    } // END function

}
