<?php namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

use Kra8\Snowflake\HasSnowflakePrimary;

class UserEducation extends Model
{
    use HasSnowflakePrimary;

    protected $connection = "mysql";

    protected $table = "user_educations";

    protected $fillable = [
        'user_id', 'school', 'decription', 'type',
        'start_time', 'end_time', 'have_graduated',
        'privacy_status'
    ];

    protected $guarded = [];

    protected $dates = [];

    protected $hidden = [];

    public $incrementing = false;

    public static $rules = [
        // Validation rules
    ];


    // Relationships
    /*
     *  one2Many
     */
    public function User()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    } // END function

    /*
     *  One2many
     */
    public function Majors()
    {
        return $this->hasMany('App\Entities\UserEducationMajor', 'user_education_id', 'id');
    } // END function
}
