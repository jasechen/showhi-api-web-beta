<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $emailFields;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $emailFields)
    {
        //
        $this->emailFields = $emailFields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
                    ->subject('ShowHi 帳號啟動通知信!!')
                    ->view('emails.verify_email')
                    ->with([
                        'account' => $this->emailFields['account'],
                        'code' => $this->emailFields['code'],
                        'link' => $this->emailFields['link'],
                    ]);
    }
}
