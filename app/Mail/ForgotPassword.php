<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $emailFields;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $emailFields)
    {
        //
        $this->emailFields = $emailFields;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
                    ->subject('ShowHi 忘記密碼通知信!!')
                    ->view('emails.forgot_password')
                    ->with([
                        'account' => $this->emailFields['account'],
                        'code' => $this->emailFields['code'],
                        'link' => $this->emailFields['link'],
                    ]);
    }
}
