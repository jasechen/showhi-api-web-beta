<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $repositories = config('repository');

        foreach ($repositories as $repository) {
            $repositoryPath = "App\Repositories\\";
            $interfaceName  = ucfirst($repository).'Repository';
            $repositoryName = ucfirst($repository).'RepositoryEloquent';

            $this->app->bind($repositoryPath . $interfaceName, $repositoryPath . $repositoryName);
        }
    }
}
