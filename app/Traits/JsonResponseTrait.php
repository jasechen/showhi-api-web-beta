<?php

namespace App\Traits;

trait JsonResponseTrait
{
    public static function failResponse($comment, $code = 400, $data = [])
    {
        static::_render('fail', $code, strval($comment), $data);
    } // END function


    public static function successResponse($comment, $data = [])
    {
        static::_render('success', 200, strval($comment), $data);
    } // END function


    private static function _render($status, $code, $comment, $data)
    {
        $headers = [];
        $headers['Cache-Control'] = "no-transform, public, max-age=300, s-maxage=900";
        $headers['Content-Type']  = "application/json; charset=utf-8";

        $options = phpversion() >= 5.4 ? JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES : 0;

        $output = ['status' => $status, 'code' => $code, 'comment' => $comment];

        if (!empty($data)) {
            $output['data'] = $data;
        } // END if

        response()->json($output, $output['code'], $headers, $options)->send();
        exit();
    } // END function
}
