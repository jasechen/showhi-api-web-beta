<?php

namespace App\Traits;

use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Support\Facades\File;

use App\Repositories\DirectoryRepositoryEloquent;
use App\Repositories\FileRepositoryEloquent;

trait ImageToolTrait
{



    /**
     * Covert the image.
     *
     * @param  \Illuminate\Filesystem\Filesystem  $file
     * @param  integer  $parentId
     * @param  string   $parentType (blog/group)
     * @param  string   $type (cover/avatar)
     * @throws
     * @return string   $filename
     */
    public function convertImage($file, $parentId, $parentType = 'blog', $type = 'cover')
    {

        // $savePath  = storage_path('resource/' . $parentType . '_' . $parentId . '/' . $type);
        $savePath  = base_path('public/gallery/' . $parentType . '_' . $parentId . '/' . $type);

        $filename = Carbon::now()->timestamp . mt_rand(100000, 999999);
        $fileExtension = $file->extension();

        $img = Image::make($file);

        $oFilePath = $savePath . '/' . $filename . '_o.' . $fileExtension;
        $img->save($oFilePath);

        $imgWidth = $img->width();

        $sizes = config('image.sizes');

        foreach ($sizes as $id => $size) {
            if ($imgWidth < $size['width']) {
                continue;
            }

            if ($type == 'cover'  and $size['width'] < 800) {
                continue;
            }
            if ($type == 'avatar' and $size['width'] > 150) {
                continue;
            }

            if ($size['width'] == $size['height']) {
                $img->fit($size['width']);
            } else {
                $img->resize($size['width'], $size['height'], function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $filePath = $savePath . '/' . $filename . '_' . $id . '.' . $fileExtension;
            $img->save($filePath);
        } // END foreach

        return $filename;
    } // END function


    public function getImageLinks($filename)
    {
        $directoryRepository = app(DirectoryRepositoryEloquent::class);
        $fileRepository = app(FileRepositoryEloquent::class);

        $imageFile = $fileRepository->findByFilename($filename);

        if ($imageFile->isEmpty()) {
            return [];
        }

        $imageDir = $directoryRepository->findByField(['id' => $imageFile->first()->directory_id]);

        if ($imageDir->isEmpty()) {
            return [];
        }


        $imageLinks = [];

        $imagePath = $imageDir->first()->parent_type . '_' . $imageDir->first()->parent_id . '/' . $imageDir->first()->type;
        // $storageImagePath = storage_path('resource/' . $imagePath);
        $storageImagePath = base_path('public/gallery/' . $imagePath);

        $sizes = config('image.sizes');
        foreach ($sizes as $id => $size) {
            $imageFilename = $imageFile->first()->filename . '_' . $id . '.' . $imageFile->first()->extension;
            if (empty(is_file($storageImagePath . '/' . $imageFilename))) {
                continue;
            }

            array_push($imageLinks, '/gallery/' . $imagePath . '/' . $imageFilename);
        } // END foreach

        $imageFilename = $imageFile->first()->filename . '_o.' . $imageFile->first()->extension;
        array_push($imageLinks, '/gallery/' . $imagePath . '/' . $imageFilename);


        return $imageLinks;
    } // END function
} // END trait
