<?php

namespace App\Doc;


/**
     * 查詢語言名稱
     *
     * @api {GET} /language/{lang} 01. 查詢語言名稱
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢語言名稱
     * @apiName GetLanguageFindByLang
     * @apiGroup Language
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    lang    語系
     *
     * @apiParamExample {json} Request
{
    "lang"     : "tw",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "languages": [
            {
                "code": "ar-eg",
                "language_name": "阿拉伯語（埃及）"
            },
            {
                "code": "ar-il",
                "language_name": "阿拉伯語（以色列）"
            },
            {
                "code": "bg-bg",
                "language_name": "保加利亞語"
            },
            {
                "code": "ca-es",
                "language_name": "加泰羅尼亞語"
            },
            {
                "code": "zh-cn",
                "language_name": "中文（簡體）"
            },
            {
                "code": "zh-tw",
                "language_name": "中文（繁體）"
            },
            ...
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "lang is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "lang error"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "language empty"
}
     *
     */



