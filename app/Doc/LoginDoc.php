<?php

namespace App\Doc;

/**
     * 使用者登入
     *
     * @api {POST} /login 02. 登入
     * @apiVersion 0.1.0
     * @apiDescription ・ 使用者登入
     * @apiName PostUserLogin
     * @apiGroup Others
     *
     * @apiHeader {string}    session                     Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    [account]                      使用者帳號
     * @apiParam {string}    [mobile_country_code]        手機號碼國碼
     * @apiParam {string}    [mobile_phone]               手機號碼
     * @apiParam {string}    password                     密碼
     *
     * @apiParamExample {json} Request
{
    "account"     : "service@showhi.co",
    "mobile_country_code"     : "886",
    "mobile_phone"     : "912345678",
    "password"    : "12345678"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     * @apiSuccess (Success) {string}   data.token          使用者 token
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "login success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "token": "33a909a7-22d2-5c0e-90c6-833a5e5b30b0",
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "account / mobile_country_code, mobile_phone is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "password is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "password' length < 8"
}
     *

     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "mobile_phone format error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "mobile country code / phone error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "account error"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "password error"
}
     *
     * @apiErrorExample {json}  Response: 403.05
{
    "status": "fail",
    "code": 403,
    "comment": "user status error"
}
     *
     */
