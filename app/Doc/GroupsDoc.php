<?php

//Groups Create
    /**
     * Groups Create
     *
     * @api {POST} /groups 01. 建立Groups
     * @apiVersion 0.1.2
     * @apiDescription ・ 用來建立建立Groups
     *
     * @apiName PostGroupsCreate
     * @apiGroup Groups
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string=school,dept,community,business}          type         group類別
     * @apiParam {string}                      title        group title
     * @apiParam {string}                      keywords        group keywords
     * @apiParam {string}                      [description]  group description
     *
     * @apiParamExample {json} Request
        {
            "type":"school",
            "title":"john school",
            "keywords":"john,大學",
            "description":"約翰大學"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該筆Group資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return create group info",
            "data": {
                "id": 15425163365584896,
                "type": "school",
                "keywords": "john,大學",
                "title": "john school",
                "description": "約翰大學"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "type is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "title is empty"
        }
     *
     * 
     * @apiErrorExample {json}  Response: 404.04
        {
            "status": "fail",
            "code": 404,
            "comment": "user error"
        }
     *
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     */

//Groups list
    /**
     * Groups list
     *
     * @api {GET} /groups 02. Groupslist
     * @apiVersion 0.1.2
     * @apiDescription ・ 列出所有開放的Groups list
     *
     * @apiName GetGroupsList
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User QRcode List資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return grouplist info",
            "data": [
                {
                    "id": "19098383511851008",
                    "title": "john school",
                    "description": "約翰大學",
                    "keywords": "john,大學",
                    "avatar": "1512546318248624",
                    "cover": "1512546301851475",
                    "status": "enable",
                    "privacy_status": "public",
                    "type": "school",
                    "owner_id": "17927074073415680",
                    "creater_id": "17927074073415680",
                    "updated_at": "2017-12-06 15:45:18",
                    "created_at": "2017-12-04 16:50:09",
                    "cover_links": [
                        "/gallery/group_19098383511851008/cover/1512546301851475_o.jpeg"
                    ],
                    "avatar_links": [
                        "/gallery/group_19098383511851008/avatar/1512546318248624_q.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512546318248624_sq.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512546318248624_o.jpeg"
                    ]
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {

        }
     *
     */

//Groups MyOwnList
    /**
     * Groups MyOwnList
     *
     * @api {GET} /groups/MyOwnList 03. MyOwnList
     * @apiVersion 0.1.2
     * @apiDescription ・ 列出我擁有的Groups list
     *
     * @apiName GetGroupsMyOwnList
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User QRcode List資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return grouplist info",
            "data": [
                {
                    "id": "19063950817759232",
                    "title": "john school",
                    "keywords": "john,大學",
                    "description": "約翰大學",
                    "avatar": "",
                    "cover": "",
                    "status": "enable",
                    "privacy_status": "private",
                    "type": "school",
                    "owner_id": "17927074073415680",
                    "creater_id": "17927074073415680",
                    "updated_at": "2017-12-04 14:33:20",
                    "created_at": "2017-12-04 14:33:20",
                    "cover_links": [],
                    "avatar_links": []
                },
                {
                    "id": "19098383511851008",
                    "title": "john school",
                    "description": "約翰大學",
                    "avatar": "1512543229970511",
                    "cover": "1512542681558364",
                    "status": "enable",
                    "privacy_status": "public",
                    "type": "school",
                    "owner_id": "17927074073415680",
                    "creater_id": "17927074073415680",
                    "updated_at": "2017-12-06 15:36:27",
                    "created_at": "2017-12-04 16:50:09",
                    "cover_links": [
                        "/gallery/group_19098383511851008/avatar/1512542681558364_q.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512542681558364_sq.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512542681558364_o.jpeg"
                    ],
                    "avatar_links": [
                        "/gallery/group_19098383511851008/avatar/1512543229970511_q.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512543229970511_sq.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512543229970511_o.jpeg"
                    ]
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {

        }
     *
     */

//Groups MyAddList
    /**
     * Groups MyAddList
     *
     * @api {GET} /groups/MyAddList 04. MyAddList
     * @apiVersion 0.1.2
     * @apiDescription ・ 列出我加入的Groups list
     *
     * @apiName GetGroupsMyAddList
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User QRcode List資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return grouplist info",
            "data": [
                {
                    "id": "19098383511851008",
                    "title": "john school",
                    "keywords": "john,大學",
                    "description": "約翰大學",
                    "avatar": "1512543229970511",
                    "cover": "1512542681558364",
                    "status": "enable",
                    "privacy_status": "private",
                    "type": "school",
                    "owner_id": "17927074073415680",
                    "creater_id": "17927074073415680",
                    "updated_at": "2017-12-06 14:53:49",
                    "created_at": "2017-12-04 16:50:09",
                    "cover_links": [
                        "/gallery/group_19098383511851008/avatar/1512542681558364_q.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512542681558364_sq.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512542681558364_o.jpeg"
                    ],
                    "avatar_links": [
                        "/gallery/group_19098383511851008/avatar/1512543229970511_q.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512543229970511_sq.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512543229970511_o.jpeg"
                    ]
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {

        }
     *
     */

//Groups update
    /**
     * Groups update
     *
     * @api {PUT} /groups/{id} 05. Update Group
     * @apiVersion 0.1.2
     * @apiDescription ・ Update Group
     *
     * @apiName PutUpdate
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string}                      [title]                標題
     * @apiParam {string}                      [description]          簡介
     * @apiParam {string=enable,disable,delete}             [status]               群組狀態
     * @apiParam {string=public,private}                    [privacy_status]       群組是否公開
     * @apiParam {string=school,dept,community,business}    [type]                 群組類別
     * @apiParam {int}                                      [owner_id]             擁有人ID
     *
     * @apiParamExample {json} Request
        {
            "title":"john test1",
            "description":"test",
            "status":"enable",
            "privacy_status":"public",
            "type":"school",
            "owner_id":"14380081212428288"
        }

     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "update note down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "no update filed data"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "privacy status is error"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "status is error"
        }
     *
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "type is error"
        }
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     *
     *
     */
//Groups delete
    /**
     * Groups delete
     *
     * @api {DELETE} /groups/{id} 06. 刪除Group
     * @apiVersion 0.1.2
     * @apiDescription ・ 刪除Group
     *
     * @apiName DeleteGroup
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id        group id
     *
     * @apiParamExample {json} Request
        {
            "id":"14653195607478272"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "delete down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     */

//Groups id
    /**
     * Groups id
     *
     * @api {GET} /groups/{id} 07. 查詢group
     * @apiVersion 0.1.7
     * @apiDescription ・ 查詢group,返回這個group相關資訊
     *
     * @apiName  FindGroup
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id        group id
     *
     * @apiParamExample {json} Request
        {
            "id":"14653195607478272"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return group info",
            "data": [
                {
                    "id": 19098383511851008,
                    "keywords": "john,大學",
                    "title": "john school",
                    "description": "約翰大學",
                    "avatar": "1512546318248624",
                    "cover": "1512546301851475",
                    "status": "enable",
                    "privacy_status": "public",
                    "type": "school",
                    "owner_id": 17927074073415680,
                    "creater_id": 17927074073415680,
                    "updated_at": "2017-12-06 15:45:18",
                    "created_at": "2017-12-04 16:50:09",
                    "cover_links": [
                        "/gallery/group_19098383511851008/cover/1512546301851475_o.jpeg"
                    ],
                    "avatar_links": [
                        "/gallery/group_19098383511851008/avatar/1512546318248624_q.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512546318248624_sq.jpeg",
                        "/gallery/group_19098383511851008/avatar/1512546318248624_o.jpeg"
                    ]
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.02
        {
            "status": "fail",
            "code": 401,
            "comment": "group not fund or delete"
        }
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "group is disable"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     *
     */

//Groups cover
    /**
     * Groups id
     *
     * @api {POST} /groups/{id}/cover 08. 上傳群組背景圖
     * @apiVersion 0.1.7
     * @apiDescription ・ 08. 上傳群組背景圖
     *
     * @apiName  UploadCoverGroup
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id        group id
     * @apiParam {file}                   cover      上傳檔案欄位
     * @apiParam {string}                 _method     值 =PUT
     *
     * @apiParamExample {json} Request
        {
            "id":"14653195607478272",
            "_method":"PUT"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "upload cover success",
            "data": {
                "session": "696c042e351dde990af8306a70f9a8f5",
                "cover_links": [
                    "/gallery/group_19098383511851008/cover/1512546301851475_o.jpeg"
                ]
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "avatar is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "group not fund or delete"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "Role cant do this"
        }
     *
     * @apiErrorExample {json}  Response: 403.05
        {
            "status": "fail",
            "code": 403,
            "comment": "group status error"
        }
     *
     * @apiErrorExample {json}  Response: 403.06
        {
            "status": "fail",
            "code": 403,
            "comment": "avatar error"
        }
     *
     *
     * @apiErrorExample {json}  Response: 403.07
        {
            "status": "fail",
            "code": 403,
            "comment": "group error"
        }
     *
     *
     */



//Groups avatar
    /**
     * Groups avatar
     *
     * @api {POST} /groups/{id}/avatar 09. 上傳群組頭像
     * @apiVersion 0.1.7
     * @apiDescription ・ 09. 上傳群組頭像
     *
     * @apiName  UploadAvatarGroup
     * @apiGroup Groups
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id        group id
     * @apiParam {file}                   avatar      上傳檔案欄位
     * @apiParam {string}                 _method     值 =PUT
     *
     * @apiParamExample {json} Request
        {
            "id":"14653195607478272",
            "_method":"PUT"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "upload avatar success",
            "data": {
                "session": "696c042e351dde990af8306a70f9a8f5",
                "avatar_links": [
                    "/gallery/group_19098383511851008/avatar/1512546318248624_q.jpeg",
                    "/gallery/group_19098383511851008/avatar/1512546318248624_sq.jpeg",
                    "/gallery/group_19098383511851008/avatar/1512546318248624_o.jpeg"
                ]
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "avatar is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "group not fund or delete"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "Role cant do this"
        }
     *
     * @apiErrorExample {json}  Response: 403.05
        {
            "status": "fail",
            "code": 403,
            "comment": "group status error"
        }
     *
     * @apiErrorExample {json}  Response: 403.06
        {
            "status": "fail",
            "code": 403,
            "comment": "avatar error"
        }
     *
     *
     * @apiErrorExample {json}  Response: 403.07
        {
            "status": "fail",
            "code": 403,
            "comment": "group error"
        }
     *
     */



