<?php

namespace App\Doc;

/**
     * 初始化 session
     *
     * @api {POST} /session/init 01. 初始化
     * @apiVersion 0.1.0
     * @apiDescription ・ 用 device_code, device_os, device_type, device_lang 初始化 session<br /> ・ 若不傳 device_code 或為空值，則自行產生
     * @apiName PostSessionInit
     * @apiGroup Session
     *
     * @apiParam {string}                                           [device_code]             裝置代碼
     * @apiParam {string=ios, andriod, macos, win, others}          [device_os="others"]      裝置作業系統
     * @apiParam {string=phone, pad, desktop, notebook, others}     [device_type="others"]    裝置型態
     * @apiParam {string=zh-tw, zh-cn, jp, en}                      [device_lang="en"]        裝置語言
     * @apiParam {string}                                           [device_token]            裝置 token
     *
     * @apiParamExample {json} Request
{
    "device_code" : "b4233639-0a61-4b0f-9ab3-682066ded25a",
    "device_os"   : "ios",
    "device_type" : "phone" ,
    "device_lang" : "en",
    "device_lang" : "b4233639-0a61-4b0f-9ab3-682066ded25a"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.device_code    裝置代碼
     * @apiSuccess (Success) {string}   data.session        Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "init success",
    "data": {
        "device_code": "aeb0c92d-c9fa-47ea-814e-8e009c1d3056",
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "device os error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "device type error"
}
     *
     * @apiErrorExample {json}  Response: 404.03
{
    "status": "fail",
    "code": 404,
    "comment": "device lang error"
}
     *
     */


// ===
    /**
     * 查詢 session
     *
     * @api {GET} /session/{code} 02. 查詢
     * @apiVersion 0.1.0
     * @apiDescription ・ 用 code 查詢 session。
     * @apiName GetSessionFindByCode
     * @apiGroup Session
     *
     * @apiParam {string}    code       Session 代碼
     *
     * @apiParamExample {json} Request
{
    "code" : "8f3e973061800dc6ebcb367079b305d8",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     * @apiSuccess (Success) {string}   data.status    Session 狀態
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "fetch success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "status": "login"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 403
{
    "status": "fail",
    "code": 403,
    "comment": "session is NOT alive"
}
     *
     */
