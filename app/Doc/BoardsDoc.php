<?php

namespace App\Doc;


// ===
    /**
     * 新增文章
     *
     * @api {POST} /board/text 01. 新增文章
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增文章 <br/>・ Permission : general / text_create
     * @apiName PutBoardCreateText
     * @apiGroup Board
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string=blog,group}               [parent_type="blog"]         所屬項目
     * @apiParam {integer}                         parent_id                    所屬項目 ID
     * @apiParam {string=public,private}           [privacy_status="public"]    隱私狀態
     * @apiParam {string=enable,disable,delete}    [status="enable"]            狀態
     * @apiParam {string}                          content                      內容
     * @apiParam {integer}                         [board_id]                   留言板 ID
     *
     * @apiParamExample {json} Request
{
    "parent_type" : "blog",
    "parent_id"  : "13984102596349952" ,
    "privacy_status" : "public",
    "status" : "enable",
    "content" : "999",
    "board_id" : "13984102596349954",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "change intro success",
    "data": {
        "session": "78b262c0d1ceb830cbc99e923cd61629",
        "board_id": "23017577773142016",
        "text_id": "23017577781530624"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "parent_id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "content is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "parent_type error"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "privacy_status error"
}
     *
     * @apiErrorExample {json}  Response: 403.05
{
    "status": "fail",
    "code": 403,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "parent error"
}
     *
     * @apiErrorExample {json}  Response: 403.06
{
    "status": "fail",
    "code": 403,
    "comment": "creater != owner"
}
     *
     * @apiErrorExample {json}  Response: 403.07
{
    "status": "fail",
    "code": 403,
    "comment": "board error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "create board error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "create text error"
}
     *
     */


// ===
    /**
     * 上傳圖檔
     *
     * @api {POST} /board/image 02. 上傳圖檔
     * @apiVersion 0.1.0
     * @apiDescription ・ 上傳圖檔 <br/>・ Permission : general / file_create
     * @apiName PostBoardUploadImage
     * @apiGroup Board
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string=blog,group}               [parent_type="blog"]         所屬項目
     * @apiParam {integer}                         parent_id                    所屬項目 ID
     * @apiParam {string=public,private}           [privacy_status="public"]    隱私狀態
     * @apiParam {string=enable,disable,delete}    [status="enable"]            狀態
     * @apiParam {file}                            image                        圖檔
     * @apiParam {integer}                         [board_id]                   留言板 ID
     *
     * @apiParamExample {json} Request
{
    "parent_type" : "blog",
    "parent_id"  : "13984102596349952" ,
    "privacy_status" : "public",
    "status" : "enable",
    "image" : "",
    "board_id" : "13984102596349954",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session        Session 代碼
     * @apiSuccess (Success) {string}     data.board_id       留言板 ID
     * @apiSuccess (Success) {string}     data.file_id        圖檔 ID
     * @apiSuccess (Success) {string[]}   data.image_links    各尺寸圖檔連結
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "upload image success",
    "data": {
        "session": "78b262c0d1ceb830cbc99e923cd61629",
        "board_id": "23017577773142016",
        "file_id": "23019925052461056",
        "image_links": [
            "/gallery/blog_22472343041478656/no_dir/1513312377156093_c.jpeg",
            "/gallery/blog_22472343041478656/no_dir/1513312377156093_z.jpeg",
            "/gallery/blog_22472343041478656/no_dir/1513312377156093_n.jpeg",
            "/gallery/blog_22472343041478656/no_dir/1513312377156093_q.jpeg",
            "/gallery/blog_22472343041478656/no_dir/1513312377156093_sq.jpeg",
            "/gallery/blog_22472343041478656/no_dir/1513312377156093_o.jpeg"
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "parent_id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "image is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "parent_type error"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "privacy_status error"
}
     *
     * @apiErrorExample {json}  Response: 403.05
{
    "status": "fail",
    "code": 403,
    "comment": "status error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "parent error"
}
     *
     * @apiErrorExample {json}  Response: 403.06
{
    "status": "fail",
    "code": 403,
    "comment": "creater != owner"
}
     *
     * @apiErrorExample {json}  Response: 403.07
{
    "status": "fail",
    "code": 403,
    "comment": "board error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "create board error"
}
     *
     * @apiErrorExample {json}  Response: 403.08
{
    "status": "fail",
    "code": 403,
    "comment": "image error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "create dir error"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "create file error"
}
     *
     */
