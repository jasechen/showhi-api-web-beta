<?php

//Notices Create
    /**
     * Notices Create
     *
     * #api {POST} /Notices 01. 建立Notices
     * @apiVersion 0.1.6
     * @apiDescription ・ 用來建立建立Notices
     *
     * @apiName PostNoticesCreate
     * @apiGroup Notices
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string}                      senderId         發送者id
     * @apiParam {string}                      receiverId       接收者
     * @apiParam {string}                      content          內文
     * @apiParam {string=group,blog}                      parentType       屬於
     * @apiParam {string}                      parentId         屬於id
     *
     * @apiParamExample {json} Request
        {
            "senderId":"14380081212428288",
            "receiverId":"17204493187944448",
            "content":"hello",
            "parentType":"group",
            "parentId":"15079001701879808"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該筆Group資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return create notices info",
            "data": [
                {
                    "sender_id": "14380081212428288",
                    "receiver_id": "17204493187944448",
                    "content": "hello",
                    "parent_type": "group",
                    "parent_id": "15079001701879808",
                    "id": "19052162051084288",
                    "updated_at": "2017-12-04 13:46:29",
                    "created_at": "2017-12-04 13:46:29"
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "sender_id is empty"
        }
     *
     *
     */

//Notices list
    /**
     * Notices list
     *
     * @api {GET} /Notices/{parentType}/list/{status} 02. Noticeslist
     * @apiVersion 0.1.6
     * @apiDescription ・ 列出所有的指定 Notices list
     *
     * @apiName GetNoticesList
     * @apiGroup Notices
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string=all,init,readed}                      status     訊息狀態
     * @apiParam {string=group,blog}                      parentType       屬於
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User QRcode List資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return notices info",
            "data": [
                {
                    "id": "17999095151071232",
                    "sender_id": "14380081212428288",
                    "receiver_id": "17927074073415680",
                    "status": "init",
                    "content": "hello",
                    "parent_type": "group",
                    "parent_id": "15079001701879808",
                    "updated_at": "2017-12-01 17:18:37",
                    "created_at": "2017-12-01 16:01:58"
                },
                {
                    "id": "17999098619760640",
                    "sender_id": "14380081212428288",
                    "receiver_id": "17927074073415680",
                    "status": "init",
                    "content": "hello",
                    "parent_type": "group",
                    "parent_id": "15079001701879808",
                    "updated_at": "2017-12-01 17:18:37",
                    "created_at": "2017-12-01 16:01:59"
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {

        }
     *
     */
//Notices readed
    /**
     * Notices readed
     *
     * @api {PUT} /Notices/{id}/readed 05. Readed Notices
     * @apiVersion 0.1.6
     * @apiDescription ・ readed Notices
     *
     * @apiName PutNoticesReaded
     * @apiGroup Notices
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     *
     * @apiParamExample {json} Request
        {
        }

     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "Readed down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     * @apiErrorExample {json}  Response: 401.02
        {
            "status": "fail",
            "code": 401,
            "comment": "status is error"
        }
     *
     * @apiErrorExample {json}  Response: 400.03
        {
            "status": "fail",
            "code": 400,
            "comment": "no this row"
        }
     *
     *
     */
//Notices delete
    /**
     * Notices delete
     *
     * @api {DELETE} /Notices/{id} 06. 刪除Notices
     * @apiVersion 0.1.6
     * @apiDescription ・ 刪除Notices
     *
     * @apiName DeleteNotices
     * @apiGroup Notices
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "delete down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "no this row"
        }
     *
     * 
     */

//Notices id
    /**
     * Notices id
     *
     * @api {GET} /Notices/{id} 07. 查詢Notices
     * @apiVersion 0.1.6
     * @apiDescription ・ 查詢Notices,返回這個Notices相關資訊
     *
     * @apiName  FindNotices
     * @apiGroup Notices
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return notices info",
            "data": [
                {
                    "id": "17999095151071232",
                    "sender_id": "14380081212428288",
                    "receiver_id": "17927074073415680",
                    "status": "init",
                    "content": "hello",
                    "parent_type": "group",
                    "parent_id": "15079001701879808",
                    "updated_at": "2017-12-01 17:18:37",
                    "created_at": "2017-12-01 16:01:58"
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "no this row"
        }
     *
     * 
     */

