<?php

namespace App\Doc;


/**
     * 查詢國家名稱
     *
     * @api {GET} /country/{lang} 01. 查詢國家名稱
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢國家名稱
     * @apiName GetCountryFindByLang
     * @apiGroup Country
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    lang    語系
     *
     * @apiParamExample {json} Request
{
    "lang"     : "tw",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "countries" : [
            {
                "code": "AF",
                "country_name": "阿富汗"
            },
            {
                "code": "AX",
                "country_name": "奧蘭群島"
            },
            {
                "code": "AL",
                "country_name": "阿爾巴尼亞"
            },
            {
                "code": "DZ",
                "country_name": "阿爾及利亞"
            },
            {
                "code": "AS",
                "country_name": "美屬薩摩亞"
            },
            {
                "code": "AD",
                "country_name": "安道爾"
            },
            {
                "code": "AO",
                "country_name": "安哥拉"
            },
            {
                "code": "AI",
                "country_name": "安圭拉"
            },
            {
                "code": "AG",
                "country_name": "安地卡及巴布達"
            },
            ...
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "lang is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "lang error"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "country name empty"
}
     *
     */


/**
     * 查詢國家 calling codes
     *
     * @api {GET} /country/callings/{lang} 02. 查詢國家 calling codes
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢國家 calling codes
     * @apiName GetCountryFindCallingCodesByLang
     * @apiGroup Country
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    lang    語系
     *
     * @apiParamExample {json} Request
{
    "lang"     : "tw",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "callings" : [
            {
                "code": "AF",
                "country_name": "阿富汗",
                "calling_code": "93"
            },
            {
                "code": "AX",
                "country_name": "奧蘭群島",
                "calling_code": "358"
            },
            {
                "code": "AL",
                "country_name": "阿爾巴尼亞",
                "calling_code": "355"
            },
            {
                "code": "DZ",
                "country_name": "阿爾及利亞",
                "calling_code": "213"
            },
            {
                "code": "AS",
                "country_name": "美屬薩摩亞",
                "calling_code": "1684"
            },
            {
                "code": "AD",
                "country_name": "安道爾",
                "calling_code": "376"
            },
            {
                "code": "AO",
                "country_name": "安哥拉",
                "calling_code": "244"
            },
            {
                "code": "AI",
                "country_name": "安圭拉",
                "calling_code": "1264"
            },
            {
                "code": "AG",
                "country_name": "安地卡及巴布達",
                "calling_code": "1268"
            },
            ...
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "lang is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "lang error"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "calling code error"
}
     *
     */


