<?php

//UserFriends Create
    /**
     * UserFriends Create
     *
     * @api {POST} /userFriends/create 01. 邀請好友
     * @apiVersion 0.1.8
     * @apiDescription ・ 邀請好友
     *
     * @apiName PostUserFriendsCreate
     * @apiGroup userFriends
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string}                      friend_id        加好友的user_id
     *
     * @apiParamExample {json} Request
        {
            "friend_id":"19066613668122624"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該筆UserFriends資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return create friend info",
            "data": {
                "id": "20183693616353280",
                "user_id": "17927074073415680",
                "friend_id": "19066613668122624",
                "status": "init",
                "updated_at": {
                    "date": "2017-12-11 11:16:08.000000",
                    "timezone_type": 3,
                    "timezone": "Asia/Taipei"
                },
                "created_at": {
                    "date": "2017-12-07 16:42:47.000000",
                    "timezone_type": 3,
                    "timezone": "Asia/Taipei"
                }
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 403.01
        {
            "status": "fail",
            "code": 403,
            "comment": "no this user"
        }
     *
     * @apiErrorExample {json}  Response: 401.02
        {
            "status": "fail",
            "code": 401,
            "comment": "Rows Error"
        }
     *
     * 
     */
//UserFriends agreeFriends
    /**
     * userFriends agreeFriends
     *
     * @api {PUT} /userFriends/agreeFriends/{id} 02. 同意加為好友
     * @apiVersion 0.1.8
     * @apiDescription ・ 同意加為好友 agreeFriends
     *
     * @apiName UserFriendsagreeFriends
     * @apiGroup userFriends
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string=enable,delete}                      status        同意好友
     *
     * @apiParamExample {json} Request
        {
            "status":"enable"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳UserFriends List
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "update down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "status is empty"
        }
     *
     * @apiErrorExample {json}  Response: 403.03
        {
            "status": "fail",
            "code": 403,
            "comment": "status is error"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "no this row"
        }
     *
     */

//UserFriends list
    /**
     * UserFriends list
     *
     * @api {GET} /userFriends/list/{status} 03. 列出自己所有好友
     * @apiVersion 0.1.8
     * @apiDescription  列出自己所有好友
     *
     * @apiName GetUserFriendsList
     * @apiGroup userFriends
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string=init,enable}                      status        要列出得好友狀態 init 等待開放,enable 已經是好友
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User 好友列表
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return friends list",
            "data": [
                {
                    "id": 20183693616353280,
                    "user_id": 17927074073415680,
                    "friend_id": 19066613668122624,
                    "status": "init",
                    "updated_at": "2017-12-11 12:11:03",
                    "created_at": "2017-12-07 16:42:47"
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "status is empty"
        }
     *
     * @apiErrorExample {json}  Response: 403.02
        {
            "status": "fail",
            "code": 403,
            "comment": "status is empty"
        }
     *
     */




//UserFriends delete
    /**
     * Groups delete
     *
     * @api {DELETE} /userFriends/delete/{id} 04. 刪除 刪除好友
     * @apiVersion 0.1.8
     * @apiDescription ・ 刪除 刪除好友
     *
     * @apiName DeleteUserFriends
     * @apiGroup userFriends
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id              UserFriends id
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "delete down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     *
     */


//UserFriends id
    /**
     * UserFriends id
     *
     * @api {GET} /userFriends/{id} 05. 查詢 一個好友資訊
     * @apiVersion 0.1.8
     * @apiDescription ・ 查詢 一個好友資訊
     *
     * @apiName  FindUserFriends
     * @apiGroup userFriends
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id             UserFriends id
     *
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return userFriends info",
            "data": [
                {
                    "id": "20183693616353280",
                    "user_id": "17927074073415680",
                    "friend_id": "19066613668122624",
                    "status": "init",
                    "updated_at": "2017-12-11 12:11:03",
                    "created_at": "2017-12-07 16:42:47"
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "group User not fund or delete"
        }
     *
     *
     */

