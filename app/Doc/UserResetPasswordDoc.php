<?php

namespace App\Doc;

/**
     * 重設使用者忘記密碼
     *
     * @api {PUT} /user/{id}/password/reset 06. 重設「忘記密碼」
     * @apiVersion 0.1.0
     * @apiDescription ・ 重設使用者忘記密碼
     * @apiName PutUserResetPassword
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    id                          使用者 ID
     * @apiParam {string}    account                           使用者帳號
     * @apiParam {string}    password                          密碼
     * @apiParam {string}    password_repeat                   確認密碼
     * @apiParam {string}    check_code                   驗證碼
     *
     * @apiParamExample {json} Request
{
    "id"  : "13984102596349952" ,
    "account"   : "service@showhi.co",
    "password"  : "12345678" ,
    "password_repeat" : "12345678",
    "check_code"  : "123456" ,
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "verify mobile success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "account is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "account error (email format error)"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "password' length < 8"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "password / password_repeat is NOT equal"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "code is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.05
{
    "status": "fail",
    "code": 403,
    "comment": "user status error"
}
     *
     * @apiErrorExample {json}  Response: 403.06
{
    "status": "fail",
    "code": 403,
    "comment": "user account error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "user check error"
}
     *
     * @apiErrorExample {json}  Response: 403.07
{
    "status": "fail",
    "code": 403,
    "comment": "user check status error"
}
     *
     * @apiErrorExample {json}  Response: 403.08
{
    "status": "fail",
    "code": 403,
    "comment": "user check code error"
}
     *
     */


// ===
    /**
     *  寄送忘記密碼通知信
     *
     * @api {POST} /user/password/notify 05. 寄送「忘記密碼」通知信
     * @apiVersion 0.1.0
     * @apiDescription ・ 寄送忘記密碼通知信
     * @apiName PostUserSendForgotPasswordNotify
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    account                           使用者帳號
     *
     * @apiParamExample {json} Request
{
    "account"   : "service@showhi.co",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "send verify mobile success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "account is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "account error (email format error)"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "user status error"
}
     *
     */
