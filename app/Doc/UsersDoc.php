<?php

namespace App\Doc;

/**
     * 查詢使用者
     *
     * @api {GET} /user/{id} 01. 查詢使用者
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢使用者
     * @apiName GetUserFind
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id    使用者 ID
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0",
        "user" : {
            "id": "",
            "account": "",
            "status": "enable",
            "type": "",
            "generate_code": "",
            "introducer_id": "",
            "email_status": "checked",
            "mobile_status": "checked",
            "profile": {
                "first_name": "",
                "last_name": "",
                "avatar": "1511803961697971",
                "birth": "",
                "email": "",
                "mobile_country_code": "",
                "mobile_phone": "",
                "city": "",
                "intro": "",
                "website": "",
                "languages": "",
                "interest_items": "",
                "countries_visited": "",
                "pets": ""
            },
            "avatar_links": [
                "/gallery/blog_17208095155556352/avatar/1511803961697971_q.png",
                "/gallery/blog_17208095155556352/avatar/1511803961697971_sq.png"
            ],
            "blog_id": "17208095155556352",
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     */


// ===
    /**
     * 改變使用者密碼
     *
     * @api {PUT} /user/{id}/password 03. 修改使用者密碼
     * @apiVersion 0.1.0
     * @apiDescription ・ 改變使用者密碼 <br/>・ Permission : general / member_update
     * @apiName PutUserChangePassword
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id    使用者 ID
     * @apiParam {string}    password                          密碼
     * @apiParam {string}    password_repeat                   確認密碼
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
    "password"  : "12345678" ,
    "password_repeat" : "12345678",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "change password success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "password' length < 8"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "password / password_repeat is NOT equal"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     */


// ===
    /**
     * 上傳使用者頭像
     *
     * @api {POST} /user/{id}/avatar 04. 上傳使用者頭像
     * @apiVersion 0.1.0
     * @apiDescription ・ 上傳使用者頭像 <br/>・ Permission : general / member_update
     * @apiName PostUserUploadAvatar
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id                          使用者 ID
     * @apiParam {file}      avatar                          頭像
     * @apiParam {string}    _method                   傳送方式 (PUT)
     *
     * @apiParamExample {json} Request
{
    "id"  : "13984102596349952" ,
    "avatar" : "",
    "_method" : "PUT",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session   Session 代碼
     * @apiSuccess (Success) {string[]}   data.avatar_links   各尺寸頭像連結
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "upload avatar success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "avatar_links": [
            "/gallery/blog_13984102596349952/avatar/1511947819834209_q.png",
            "/gallery/blog_13984102596349952/avatar/1511947819834209_sq.png",
        ],
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "avatar is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "user status error"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "avatar error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "create dir error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "create file error"
}
     *
     */


    /**
     * 查詢使用者 by token
     *
     * @api {GET} /user/token/{token} 02. 查詢使用者 by token
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢使用者 by token
     * @apiName GetUserFindByToken
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiParamExample {json} Request
{
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0",
        "user" : {
            "id": "",
            "account": "",
            "status": "enable",
            "type": "",
            "generate_code": "",
            "introducer_id": "",
            "email_status": "checked",
            "mobile_status": "checked",
            "profile": {
                "first_name": "",
                "last_name": "",
                "avatar": "1511803961697971",
                "birth": "",
                "email": "",
                "mobile_country_code": "",
                "mobile_phone": "",
                "city": "",
                "intro": "",
                "website": "",
                "languages": "",
                "interest_items": "",
                "countries_visited": "",
                "pets": ""
            },
            "avatar_links": [
                "/gallery/blog_17208095155556352/avatar/1511803961697971_q.png",
                "/gallery/blog_17208095155556352/avatar/1511803961697971_sq.png"
            ],
            "blog_id": "17208095155556352",
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     */


/**
     * 查詢使用者角色權限
     *
     * @api {GET} /user/{id}/role 13. 查詢使用者角色權限
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢使用者角色權限
     * @apiName GetUserFindRole
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id    使用者 ID
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0",
        "roles": [
            {
                "id": "20537486744883200",
                "type": "general",
                "name": "admin",
                "description": "網站管理員",
                "creater_id": "0",
                "updated_at": "2017-12-08 16:08:38",
                "created_at": "2017-12-08 16:08:38",
                "permissions": [
                    {
                        "id": "20537486807797760",
                        "type": "admin",
                        "name": "role_create",
                        "description": "新增角色",
                        "creater_id": "0",
                        "updated_at": "2017-12-08 16:08:38",
                        "created_at": "2017-12-08 16:08:38"
                    },
                    {
                        "id": "20537486832963584",
                        "type": "admin",
                        "name": "role_update",
                        "description": "更新角色",
                        "creater_id": "0",
                        "updated_at": "2017-12-08 16:08:38",
                        "created_at": "2017-12-08 16:08:38"
                    },
                    {
                        "id": "20537486874906624",
                        "type": "admin",
                        "name": "role_delete",
                        "description": "刪除角色",
                        "creater_id": "0",
                        "updated_at": "2017-12-08 16:08:38",
                        "created_at": "2017-12-08 16:08:38"
                    },
                    ...
                ]
            },
            {
                "id": "20537486988152832",
                "type": "general",
                "name": "member",
                "description": "網站會員",
                "creater_id": "0",
                "updated_at": "2017-12-08 16:08:38",
                "created_at": "2017-12-08 16:08:38",
                "permissions": [
                    {
                        "id": "20537486996541440",
                        "type": "general",
                        "name": "member_update",
                        "description": "更新自己資料",
                        "creater_id": "0",
                        "updated_at": "2017-12-09 00:33:48",
                        "created_at": "2017-12-08 16:08:38"
                    },
                    {
                        "id": "20537487013318656",
                        "type": "general",
                        "name": "blog_update",
                        "description": "更新部落格資料",
                        "creater_id": "0",
                        "updated_at": "2017-12-08 16:08:38",
                        "created_at": "2017-12-08 16:08:38"
                    },
                    {
                        "id": "20537487025901568",
                        "type": "general",
                        "name": "text_create",
                        "description": "新增文章",
                        "creater_id": "0",
                        "updated_at": "2017-12-08 16:08:38",
                        "created_at": "2017-12-08 16:08:38"
                    },
                    ...
                ]
            }
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     */


// ===
    /**
     * 改變使用者角色
     *
     * @api {PUT} /user/{id}/role 14. 改變使用者角色
     * @apiVersion 0.1.0
     * @apiDescription ・ 改變使用者角色 <br/>・ Permission : admin / user_update
     * @apiName PutUserUpdateRole
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id    使用者 ID
     * @apiParam {string}    role_id                          角色 ID
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
    "role_id"  : "12345678"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "update role success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "role_id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
     *
     * @apiErrorExample {json}  Response: 404.03
{
    "status": "fail",
    "code": 404,
    "comment": "user role error"
}
     *
     */


