<?php

namespace App\Doc;

/**
     * 使用者註冊
     *
     * @api {POST} /register 01. 註冊
     * @apiVersion 0.1.0
     * @apiDescription ・ 使用者註冊
     * @apiName PostUserRegister
     * @apiGroup Others
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    account                           使用者帳號
     * @apiParam {string}    password                          密碼
     * @apiParam {string}    password_repeat                   確認密碼
     * @apiParam {string}    first_name                        名
     * @apiParam {string}    last_name                         姓
     * @apiParam {string}    mobile_country_code               手機號碼國碼
     * @apiParam {string}    mobile_phone                      手機號碼
     * @apiParam {string}    qrcode_id                         QRCode ID
     * @apiParam {string}    [introducer_id="recruiter_id"]    介紹人 ID
     *
     * @apiParamExample {json} Request
{
    "account"   : "service@showhi.co",
    "password"  : "12345678" ,
    "password_repeat" : "12345678",
    "first_name" : "John",
    "last_name"   : "Wu",
    "mobile_country_code" : "886",
    "mobile_phone" : "912345678",
    "qrcode_id"  : "16924987168526336",
    "introducer_id" : "15136530842652672",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "register success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "account is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "account error (email format error)"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "password / password_repeat is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "password' length < 8"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "password / password_repeat is NOT equal"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "first_name is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "last_name is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.06
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_country_code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.07
{
    "status": "fail",
    "code": 400,
    "comment": "mobile_phone is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.08
{
    "status": "fail",
    "code": 400,
    "comment": "qrcode_id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.05
{
    "status": "fail",
    "code": 403,
    "comment": "mobile_phone format error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "qrcode error"
}
     *
     * @apiErrorExample {json}  Response: 403.06
{
    "status": "fail",
    "code": 403,
    "comment": "qrcode expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.07
{
    "status": "fail",
    "code": 403,
    "comment": "someone already use this account"
}
     *
     * @apiErrorExample {json}  Response: 403.08
{
    "status": "fail",
    "code": 403,
    "comment": "mobile phone already used"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "create user fail"
}
     *
     */
