<?php

namespace App\Doc;

/**
     * 查詢語言列表
     *
     * @api {GET} /locales/langs 01. 查詢語言列表
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢可使用的語言列表
     * @apiName GetLocalesFindLangs
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "langs" : [
            "tw",
            "en",
            "cn"
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     */


// ===
/**
     * 查詢語系內容 by lang
     *
     * @api {GET} /locales/lang/{lang} 02. 查詢語系 by lang
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢語系內容 by lang
     * @apiName GetLocalesFindByLang
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    lang    語系
     *
     * @apiParamExample {json} Request
{
    "lang"     : "tw"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "locales" : [
            {
                "id": 1,
                "code": "b10_subject_menu",
                "note": "b10_subject_menu's comment",
                "creater_id": 0,
                "updated_at": "2015-07-15 04:43:29",
                "created_at": "2015-07-15 04:43:29",
                "contents": [
                    [
                        {
                            "id": 1,
                            "locales_id": 1,
                            "lang": "tw",
                            "content": "選單",
                            "creater_id": 0,
                            "updated_at": "2015-07-15 04:43:29",
                            "created_at": "2015-07-15 04:43:29"
                        }
                    ]
                ]
            },
            {
                "id": 2,
                "code": "b10_title_scan",
                "note": "b10_title_scan's comment",
                "creater_id": 0,
                "updated_at": "2015-07-15 04:43:29",
                "created_at": "2015-07-15 04:43:29",
                "contents": [
                    [
                        {
                            "id": 119,
                            "locales_id": 2,
                            "lang": "tw",
                            "content": "掃描",
                            "creater_id": 0,
                            "updated_at": "2015-08-10 09:05:18",
                            "created_at": "2015-07-15 04:43:29"
                        }
                    ]
                ]
            },...
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "lang is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "lang error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "lang NOT exist"
}
     *
     */


// ===
/**
     * 查詢語系內容 by code
     *
     * @api {GET} /locales/code/{code} 03. 查詢語系 by code
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢語系內容 by code
     * @apiName GetLocalesFindByCode
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    code    語系代號
     *
     * @apiParamExample {json} Request
{
    "code"     : "b10_subject_menu"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "locales" : {
            "id": 1,
            "code": "b10_subject_menu",
            "note": "b10_subject_menu's comment",
            "creater_id": 0,
            "updated_at": "2015-07-15 04:43:29",
            "created_at": "2015-07-15 04:43:29",
            "contents": [
                {
                    "id": 3,
                    "locales_id": 1,
                    "lang": "cn",
                    "content": "选单",
                    "creater_id": 0,
                    "updated_at": "2015-07-15 04:43:29",
                    "created_at": "2015-07-15 04:43:29"
                },
                {
                    "id": 2,
                    "locales_id": 1,
                    "lang": "en",
                    "content": "Menu",
                    "creater_id": 0,
                    "updated_at": "2015-07-15 04:43:29",
                    "created_at": "2015-07-15 04:43:29"
                },
                {
                    "id": 1,
                    "locales_id": 1,
                    "lang": "tw",
                    "content": "選單",
                    "creater_id": 0,
                    "updated_at": "2015-07-15 04:43:29",
                    "created_at": "2015-07-15 04:43:29"
                }
           ]
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "code is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "code NOT exist"
}
     *
     */


// ===
/**
     * 查詢語系內容 by lang, code
     *
     * @api {GET} /locales/{lang}/{code} 04. 查詢語系 by lang, code
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢語系內容 by lang, code
     * @apiName GetLocalesFindByLangAndCode
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    code    語系代號
     *
     * @apiParamExample {json} Request
{
    "lang"     : "tw"
    "code"     : "b10_subject_menu"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "locales" : {
            "id": 1,
            "code": "b10_subject_menu",
            "note": "b10_subject_menu's comment",
            "creater_id": 0,
            "updated_at": "2015-07-15 04:43:29",
            "created_at": "2015-07-15 04:43:29",
            "contents": [
                {
                    "id": 1,
                    "locales_id": 1,
                    "lang": "tw",
                    "content": "選單",
                    "creater_id": 0,
                    "updated_at": "2015-07-15 04:43:29",
                    "created_at": "2015-07-15 04:43:29"
                }
            ]
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "lang is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "code is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "lang error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "code and lang NOT exist"
}
     *
     */

// ===
    /**
     * 新增語系
     *
     * @api {POST} /locales 05. 新增語系
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增語系。新增後會自行產生語系檔 (.json) <br/>・ Permission : admin / locales_create
     * @apiName PostLocalesCreate
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    code                      語系代號
     * @apiParam {string}    note                          備註
     * @apiParam {string}    lang_tw                   繁體語系語言
     * @apiParam {string}    content_tw                   繁體語系內容
     * @apiParam {string}    lang_en                   英語系語言
     * @apiParam {string}    content_en                   英語系內容
     * @apiParam {string}    lang_cn                   簡體語系語言
     * @apiParam {string}    content_cn                   簡體語系內容
     *
     * @apiParamExample {json} Request
{
    "code":"button_111",
    "note":"",
    "lang_tw":"tw",
    "content_tw":"按鈕 111",
    "lang_en":"en",
    "content_en":"button 111",
    "lang_cn":"cn",
    "content_cn":"button 111"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "locales_id": ""
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "code is empty"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "code ALREADY exist"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "contents empty"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "create locales fail"
}
     *
     */


// ===
    /**
     * 修改語系
     *
     * @api {PUT} /locales/{id} 06. 修改語系
     * @apiVersion 0.1.0
     * @apiDescription ・ 修改語系。修改後會自行產生語系檔 (.json) <br/>・ Permission : admin / locales_update
     * @apiName PostLocalesUpdate
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id                      語系 ID
     * @apiParam {string}    code                      語系代號
     * @apiParam {string}    note                          備註
     * @apiParam {string}    lang_tw                   繁體語系語言
     * @apiParam {string}    content_tw                   繁體語系內容
     * @apiParam {string}    lang_en                   英語系語言
     * @apiParam {string}    content_en                   英語系內容
     * @apiParam {string}    lang_cn                   簡體語系語言
     * @apiParam {string}    content_cn                   簡體語系內容
     *
     * @apiParamExample {json} Request
{
    "id":"",
    "code":"button_111",
    "note":"",
    "lang_tw":"tw",
    "content_tw":"按鈕 111",
    "lang_en":"en",
    "content_en":"button 111",
    "lang_cn":"cn",
    "content_cn":"button 111"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "locales_id": ""
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "code is empty"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "code ALREADY exist"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "contents empty"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "update locales fail"
}
     *
     */


// ===
    /**
     * 刪除語系
     *
     * @api {DELETE} /locales/{id} 07. 刪除語系
     * @apiVersion 0.1.0
     * @apiDescription ・ 刪除語系 <br/>・ Permission : admin / locales_delete
     * @apiName PostLocalesDelete
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id                      語系 ID
     *
     * @apiParamExample {json} Request
{
    "id":""
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "locales_id": ""
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "delete fail"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "delete content fail"
}
     *
     */


// ===
/**
     * 產生語系檔案 by lang
     *
     * @api {GET} /locales/lang/{lang}/makefile 08. 產生語系檔案 by lang
     * @apiVersion 0.1.0
     * @apiDescription ・ 產生語系檔案 by lang (Ex: http://{HOST}/asset/json/locales_{lang}.json) <br/>・ Permission : admin / locales_update
     * @apiName GetLocalesMakeFileByLang
     * @apiGroup Locales
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string=tw,en,cn,all}    lang    語系
     *
     * @apiParamExample {json} Request
{
    "lang"     : "tw"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "make success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "lang is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "lang error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "lang NOT exist"
}
     *
     */


