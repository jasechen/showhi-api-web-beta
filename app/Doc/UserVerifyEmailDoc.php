<?php

namespace App\Doc;

/**
     * 啟動使用者帳號
     *
     * @api {PUT} /user/{id}/email/verify 08. 驗證使用者 Email
     * @apiVersion 0.1.0
     * @apiDescription ・ 啟動使用者帳號
     * @apiName PutUserVerifyEmail
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                  id                      使用者 ID
     * @apiParam {string}                                  check_code                   驗證碼
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
    "check_code"  : "123456" ,
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "verify email success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "code is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "user status error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "user check error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "user check status error"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "user check code error"
}
     *
     */


// ===
    /**
     *  寄送啟動使用者帳號通知信
     *
     * @api {GET} /user/{id}/email/notify 07. 寄送驗證使用者 Email 通知信
     * @apiVersion 0.1.0
     * @apiDescription ・ 寄送啟動使用者帳號通知信
     * @apiName GetUserSendVerifyEmailNotify
     * @apiGroup User
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}                                  id                      使用者 ID
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "send verify email success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "user status error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "user check error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "user check status error"
}
     *
     */
