<?php
//Qrcodes Create
    /**
     * Qrcodes Create
     *
     * @api {POST} /qrcodes 01. 建立Qrcodes
     * @apiVersion 0.1.1
     * @apiDescription ・ 用來建立建立Qrcodes
     *
     * @apiName PostQrcodesCreate
     * @apiGroup Qrcodes
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string=agent, user}                      type        qrcode類別
     * @apiParam {string=blog, group}                      [parentType]  屬於,當 parentType = group 時 parentId 為必填
     * @apiParam {string=agent, user}                      [parentId]    屬於Id(group_id or blog_id)
     *
     * @apiParamExample {json} Request
        {
            "type" : "agent",
            "parentType" : "group",
            "parentId" : "14674086743642112"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該筆QRcode資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return create qrcode info",
            "data": {
                "id": 14674086743642112,
                "file_name": "1511322574.png",
                "created_at": {
                    "date": "2017-11-22 11:49:34.910557",
                    "timezone_type": 3,
                    "timezone": "Asia/Taipei"
                },
                "expire_time": {
                    "date": "2017-11-23 11:49:34.910557",
                    "timezone_type": 3,
                    "timezone": "Asia/Taipei"
                },
                "url": "https://showhi.co/qrcodes/mzjian@showhi.co/1511322574"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "type is empty"
        }
     *
     * @apiErrorExample {json}  Response: 404.04
        {
            "status": "fail",
            "code": 404,
            "comment": "user error"
        }
     *
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     */
//Qrcodes Check
    /**
     * Qrcodes Check
     *
     * @api {get} /qrcodes/{id}/check 02. 確認連結有效性
     * @apiVersion 0.1.1
     * @apiDescription ・ 確認連結有效性
     *
     * @apiName GetQrcodesCheck
     * @apiGroup Qrcodes
     *
     * @apiParam {string}                       id        qrocde id
     *
     * @apiParamExample {json} Request
        {
            "id" : "16933130309079040"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳是否通過
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "Link ok",
            "data": true
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *

     * @apiErrorExample {json}  Response: 401.02
        {
            "status": "fail",
            "code": 401,
            "comment": "this page cant login or session"
        }
     *
     */
//Qrcodes list
    /**
     * Qrcodes list
     *
     * @api {GET} /qrcodes 03. Qrcodeslist
     * @apiVersion 0.1.1
     * @apiDescription ・ 列出屬於我的Qrcodes list
     *
     * @apiName GetQrcodesList
     * @apiGroup Qrcodes
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User QRcode List資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return qrcodelist info",
            "data": [
                {
                    "id": 16933130309079040,
                    "type": "agent",
                    "url": "https://showhi.co/register/16933130309079040",
                    "filename": "1511861172.png",
                    "expire_time": 86400,
                    "num_scan": 1,
                    "creater_id": 14380081212428288,
                    "updated_at": "2017-11-28 17:39:18",
                    "created_at": "2017-11-28 17:26:12",
                    "note": ""
                    "status": "enable",
                    "parent_type": "blog",
                    "parent_id": "17927074127941632",
                    "path": [
                        "/qrcode/blog_17927074127941632/_n.pmg",
                        "/qrcode/blog_17927074127941632/_sq.pmg"
                    ]
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "note is empty"
        }
     *
     * @apiErrorExample {json}  Response: 404.03
        {
            "status": "fail",
            "code": 404,
            "comment": "user error"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     */



//Qrcodes updateNote
    /**
     * Qrcodes updateNote
     *
     * @api {PUT} /qrcodes/{id}/updatenote 04. 寫入note
     * @apiVersion 0.1.1
     * @apiDescription ・ 針對這筆Qrcode 寫入note
     *
     * @apiName PutUpdateNote
     * @apiGroup Qrcodes
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id           qrcode id
     * @apiParam {string}                      note         描述
     *
     * @apiParamExample {json} Request
        {
            "id":"14653195607478272",
            "note":"text test"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "update note down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "note is empty"
        }
     *
     * @apiErrorExample {json}  Response: 404.03
        {
            "status": "fail",
            "code": 404,
            "comment": "user error"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     */
//Qrcodes delete
    /**
     * Qrcodes delete
     *
     * @api {DELETE} /qrcodes/{id} 05. 刪除Qrcdoe
     * @apiVersion 0.1.1
     * @apiDescription ・ 刪除Qrcode
     *
     * @apiName DeleteQrcode
     * @apiGroup Qrcodes
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string=agent, user}                      type        qrcode類別
     *
     * @apiParamExample {json} Request
        {
            "id":"14653195607478272"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "delete down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     */

//qrcodes id
    /**
     * Qrcodes id
     *
     * @api {get} /qrcodes/{id} 06. 回傳qrcode 資訊
     * @apiVersion 0.1.4
     * @apiDescription ・ 回傳qrcode 資訊
     *
     * @apiName GetQrcodesFindInfo
     * @apiGroup Qrcodes
     *
     * @apiParam {int}                       id        Qrcode Id
     *
     * @apiParamExample {json} Request
        {
            "id" : "14663411203969024"
        }
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiSuccess (Success) {string}   data    回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return qrcodes info",
            "data": [
                {
                    "id": 16933130309079040,
                    "type": "agent",
                    "url": "https://showhi.co/register/16933130309079040",
                    "filename": "1511861172.png",
                    "expire_time": 86400,
                    "num_scan": 1,
                    "creater_id": 14380081212428288,
                    "updated_at": "2017-11-28 17:39:18",
                    "created_at": "2017-11-28 17:26:12",
                    "note": ""
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     */

//Qrcodes groupList
    /**
     * Qrcodes groupList
     *
     * @api {GET} /qrcodes/{groupId}/groupList 07. QrcodeGrouplist
     * @apiVersion 0.1.1
     * @apiDescription ・ 列出屬於我的Qrcode Group list
     *
     * @apiName GetQrcodesGroupList
     * @apiGroup Qrcodes
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam int                      groupId        groupId
     *
     * @apiParamExample {json} Request
        {
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User QRcode List資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return qrcodelist info",
            "data": [
                {
                    "id": "24094581469614080",
                    "type": "agent",
                    "url": "",
                    "filename": "",
                    "expire_time": 86400,
                    "num_scan": 0,
                    "creater_id": "17927074073415680",
                    "updated_at": "2017-12-18 11:43:15",
                    "created_at": "2017-12-18 11:43:15",
                    "note": "",
                    "status": "enable",
                    "parent_type": "group",
                    "parent_id": "22312657180823552",
                    "path": [
                        "/qrcode/group_22312657180823552/_n.pmg",
                        "/qrcode/group_22312657180823552/_sq.pmg"
                    ]
                }
            ]
        }

     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "note is empty"
        }
     *
     * @apiErrorExample {json}  Response: 404.03
        {
            "status": "fail",
            "code": 404,
            "comment": "user error"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "type no permissions"
        }
     *
     */