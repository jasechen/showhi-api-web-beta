<?php

namespace App\Doc;

/**
     * 查詢部落格
     *
     * @api {GET} /blog/{id} 01. 查詢部落格
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢部落格
     * @apiName GetBlogFind
     * @apiGroup Blog
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id    部落格 ID
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "blog":{
            "id":"17208095155556352",
            "intro":"",
            "cover":"1511803961697971",
            "status":"enable",
            "onwer_id":"17208095038115840",
            "creater_id":"15136530842652672",
            "cover_links":[
                "/gallery/blog_17208095155556352/cover/1511947819834209_k.png",
                "/gallery/blog_17208095155556352/cover/1511947819834209_h.png",
                "/gallery/blog_17208095155556352/cover/1511947819834209_l.png",
                "/gallery/blog_17208095155556352/cover/1511947819834209_c.png",
            ],
            "boards":[
                {
                    "id":"",
                    "parent_type":"blog",
                    "parent_id":"",
                    "privacy_status":"public",
                    "status":"enable",
                    "has_text":"true",
                    "creater_id":"17208095038115840",
                    "text": {
                        "id":"",
                        "board_id":"",
                        "content":"Welcome!! This is your FIRST text.",
                        "creater_id":"17208095038115840",
                    }
                },
                ....
            ]
        }
    }

}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "blog error"
}
     *
     */


// ===
    /**
     * 變更部落格簡介
     *
     * @api {PUT} /blog/{id}/intro 02. 變更部落格簡介
     * @apiVersion 0.1.0
     * @apiDescription ・ 變更部落格簡介 <br/>・ Permission : general / blog_update
     * @apiName PutBlogChangeIntro
     * @apiGroup Blog
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id                      部落格 ID
     * @apiParam {string}    intro                   簡介
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
    "intro"  : "HiHi.. Welcome!!" ,
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "change intro success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "intro is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "blog error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "blog status error"
}
     *
     */


// ===
    /**
     * 上傳部落格封面
     *
     * @api {POST} /blog/{id}/cover 03. 上傳部落格封面
     * @apiVersion 0.1.0
     * @apiDescription ・ 上傳部落格封面 <br/>・ Permission : general / blog_update
     * @apiName PostBlogUploadCover
     * @apiGroup Blog
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    id                          部落格 ID
     * @apiParam {file}      cover                          封面
     * @apiParam {string}    _method                   傳送方式 (PUT)
     *
     * @apiParamExample {json} Request
{
    "id"  : "13984102596349952" ,
    "cover" : "",
    "_method" : "PUT",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}     data.session   Session 代碼
     * @apiSuccess (Success) {string[]}   data.cover_links   各尺寸頭像連結
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "upload cover success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "cover_links": [
            "/gallery/blog_13984102596349952/cover/1511947819834209_k.png",
            "/gallery/blog_13984102596349952/cover/1511947819834209}_h.png",
            "/gallery/blog_13984102596349952/cover/1511947819834209_l.png",
            "/gallery/blog_13984102596349952/cover/1511947819834209_c.png",
        ],
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "cover is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "blog error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "blog status error"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "cover error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "create dir error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "create file error"
}
     *
     */
