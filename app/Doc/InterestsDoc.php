<?php

namespace App\Doc;

/**
     * 查詢興趣列表
     *
     * @api {GET} /interest/{lang} 01. 查詢興趣列表
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢興趣列表
     * @apiName GetInterestFindByLang
     * @apiGroup Interest
     *
     * @apiHeader {string}   session                             Session 代碼
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8"
}
     *
     * @apiParam {string}    lang    語系
     *
     * @apiParamExample {json} Request
{
    "lang"     : "tw",
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "interests": [
            {
                "code": "anime",
                "interest_name": "動漫"
            },
            {
                "code": "art",
                "interest_name": "藝術人文"
            },
            {
                "code": "book",
                "interest_name": "書籍"
            },
            {
                "code": "business",
                "interest_name": "商業"
            },
            {
                "code": "car",
                "interest_name": "汽車機車"
            },
            {
                "code": "design",
                "interest_name": "設計"
            },
            {
                "code": "education",
                "interest_name": "教育"
            },
            {
                "code": "entertainment",
                "interest_name": "名人娛樂"
            },
            {
                "code": "esports",
                "interest_name": "電競遊戲"
            },
            {
                "code": "estate",
                "interest_name": "房地產"
            },
            {
                "code": "family",
                "interest_name": "家庭親子"
            },
            {
                "code": "fashion",
                "interest_name": "時尚美妝"
            },
            {
                "code": "finance",
                "interest_name": "理財"
            },
            {
                "code": "food",
                "interest_name": "美食"
            },
            {
                "code": "gardening",
                "interest_name": "家居園藝"
            },
            {
                "code": "health",
                "interest_name": "健康瘦身"
            },
            {
                "code": "history",
                "interest_name": "歷史文化"
            },
            {
                "code": "international",
                "interest_name": "國際政治"
            },
            {
                "code": "language",
                "interest_name": "語言"
            },
            {
                "code": "movie",
                "interest_name": "電影戲劇"
            },
            {
                "code": "music",
                "interest_name": "音樂"
            },
            {
                "code": "news",
                "interest_name": "時事"
            },
            {
                "code": "psychic",
                "interest_name": "兩性心理"
            },
            {
                "code": "religion",
                "interest_name": "宗教命理"
            },
            {
                "code": "science",
                "interest_name": "科學"
            },
            {
                "code": "shopping",
                "interest_name": "購物"
            },
            {
                "code": "software",
                "interest_name": "軟體"
            },
            {
                "code": "sports",
                "interest_name": "運動"
            },
            {
                "code": "technology",
                "interest_name": "科技"
            },
            {
                "code": "travel",
                "interest_name": "旅遊"
            },
            {
                "code": "others",
                "interest_name": "其他"
            }
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "lang is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "lang error"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "interest empty"
}
     *
     */


