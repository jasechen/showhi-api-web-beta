<?php

namespace App\Doc;

/**
     * 更新使用者個資
     *
     *
     * @api {PUT} /user/{id}/profile 12. 更新使用者個資
     * @apiVersion 0.1.0
     * @apiDescription ・ 更新使用者個資 <br/>・ Permission : general / member_update
     * @apiName PutUserProfile
     * @apiGroup User
     *
     * @apiHeader {string}   session                           Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}                                  id                     使用者 ID
     * @apiParam {string}                                  [first_name]           名
     * @apiParam {string}                                  [last_name]            姓
     * @apiParam {string}                                  [nickname]             暱稱
     * @apiParam {string}                                  [gender]               性別
     * @apiParam {string}                                  [birth]                生日
     * @apiParam {string}                                  [city]                 居住城市
     * @apiParam {string}                                  [intro]                自我介紹
     * @apiParam {string}                                  [website]              網站
     * @apiParam {string[]="bg_BG","zh_TW","zh_CN","..."} [languages]            語言
     * @apiParam {string[]="anime","art","book","..."}    [interests]            興趣
     * @apiParam {string[]="TW","CN","US","..."}           [countries]            去過的國家
     * @apiParam {string[]="dog","cat","others"}           [pets]                 寵物
     *
     * @apiParamExample {json} Request
{
    "id"     : "13984102596349952",
    "first_name":"Jase",
    "last_name":"Chen",
    "nickname" : "JaseChen",
    "gender":"male",
    "birth":"1976-11-24",
    "city":"tpe",
    "intro":"lalala...",
    "website":"http://hitonews.com",
    "languages":[
        "zh_tw",
        "en"
    ],
    "interests":[
        "anime",
        "book"
    ],
    "countries":[
        "TW",
        "JP"
    ],
    "pets":[
        "cat",
        "dog"
    ]
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "verify qrcode success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8"
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "user error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "user status error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "user profile error"
}
     *
     * @apiErrorExample {json}  Response: 404.03
{
    "status": "fail",
    "code": 404,
    "comment": "nickname ALREADY exist"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "gender error"
}
     *
     * @apiErrorExample {json}  Response: 403.05
{
    "status": "fail",
    "code": 403,
    "comment": "birth error"
}
     *
     * @apiErrorExample {json}  Response: 403.06
{
    "status": "fail",
    "code": 403,
    "comment": "website error"
}
     *
     * @apiErrorExample {json}  Response: 403.07
{
    "status": "fail",
    "code": 403,
    "comment": "language error"
}
     *
     * @apiErrorExample {json}  Response: 403.08
{
    "status": "fail",
    "code": 403,
    "comment": "interest error"
}
     *
     * @apiErrorExample {json}  Response: 403.09
{
    "status": "fail",
    "code": 403,
    "comment": "country error"
}
     *
     * @apiErrorExample {json}  Response: 403.10
{
    "status": "fail",
    "code": 403,
    "comment": "pet error"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "update fields empty"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "update error"
}
     *
     */


