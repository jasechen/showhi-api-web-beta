<?php

//GroupUsers Create
    /**
     * GroupUsers Create
     *
     * @api {POST} /groupUsers 01. 邀請別人加入Group
     * @apiVersion 0.1.5
     * @apiDescription ・ 邀請別人加入Group
     *
     * @apiName PostGroupUsersCreate
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string}                      title        group title
     * @apiParam {string}                      [description]  group description
     *
     * @apiParamExample {json} Request
        {
            "group_id":"15079001701879808",
            "account":"mzjian@showhi.co"
        } 

     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該筆GroupUsers資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return create groupUser info",
            "data": {
                "group_id": "15079001701879808",
                "user_id": 17204493187944448,
                "role": "member",
                "status": "init",
                "meeting_enable": "disable",
                "live_enable": "disable",
                "recruiter_id": 17204493187944448,
                "allower_id": 0,
                "id": 17210592423186432,
                "updated_at": "2017-11-29 11:48:44",
                "created_at": "2017-11-29 11:48:44"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "account is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     * @apiErrorExample {json}  Response: 404.03
        {
            "status": "fail",
            "code": 404,
            "comment": "user error"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "group have this User"
        }
     * 
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "Role Error"
        }
     * 
     * 
     * 
     */
//GroupUsers Join
 /**
     * GroupUsers Join
     *
     * @api {POST} /groupUsers/join 02. 自己加入group
     * @apiVersion 0.1.5
     * @apiDescription ・ 自己加入group
     *
     * @apiName PostGroupUsersjoin
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {string}                      title        group title
     * @apiParam {string}                      [description]  group description
     *
     * @apiParamExample {json} Request
        {
            "group_id":"15079001701879808"
        } 

     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該筆GroupUsers資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return create groupUser info",
            "data": {
                "group_id": "15079001701879808",
                "user_id": 17204493187944448,
                "role": "member",
                "status": "init",
                "meeting_enable": "disable",
                "live_enable": "disable",
                "recruiter_id": 0,
                "allower_id": 0,
                "id": 17237331635474432,
                "updated_at": "2017-11-29 13:35:00",
                "created_at": "2017-11-29 13:35:00"
            }
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "account is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     * @apiErrorExample {json}  Response: 404.03
        {
            "status": "fail",
            "code": 404,
            "comment": "user error"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "group have this User"
        }
     * 
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "Role Error"
        }
     * 
     * 
     * 
     */

//GroupUsers list
    /**
     * GroupUsers list
     *
     * @api {GET} /groupUsers/{groupId}/list 02. Group Users list
     * @apiVersion 0.1.5
     * @apiDescription ・ 列出這個Group Users list
     *
     * @apiName GetGroupsList
     * @apiGroup GroupUsers
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     *
     * @apiParamExample {json} Request
        {
            "groupId":  "15079001701879808"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳該User QRcode List資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return groupUsers list",
            "data": [
                {
                    "id": 15079001735434240,
                    "group_id": 15079001701879808,
                    "user_id": 14380081212428288,
                    "role": "admin",
                    "status": "enable",
                    "meeting_enable": "enable",
                    "live_enable": "enable",
                    "recruiter_id": 15079001701879808,
                    "allower_id": 15079001701879808,
                    "updated_at": "2017-11-28 12:11:26",
                    "created_at": "2017-11-23 14:38:34"
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     */




//GroupUsers delete
    /**
     * Groups delete
     *
     * @api {DELETE} /groupUsers/{groupId}/delete/{id} 03. 刪除 Group Users
     * @apiVersion 0.1.5
     * @apiDescription ・ 刪除 Group Users
     *
     * @apiName DeleteGroupUsers
     * @apiGroup GroupUsers
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     * @apiParam {int}                      id              groupUsers id
     *
     * @apiParamExample {json} Request
        {
            "groupId":"15079001701879808"
            "id":"15079001735434240"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "delete down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "group id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "Owner can't delete"
        }
     *
     *
     */

//GroupUsers selfDelete
    /**
     * GroupUsers selfDelete
     *
     * @api {DELETE} /groupUsers/{groupId}/selfDelete/{id} 03. 退出Group
     * @apiVersion 0.1.5
     * @apiDescription ・ 退出Group
     *
     * @apiName selfDeleteGroupUsers
     * @apiGroup GroupUsers
     *
     * 
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     * @apiParam {int}                      id             groupUsers id
     *
     * @apiParamExample {json} Request
        {
            "groupId":"15079001701879808"
            "id":"15079001735434240"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "delete down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "group id is empty"
        }
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "Owner can't delete"
        }
     *
     *
     */

//GroupUsers id
    /**
     * GroupUsers id
     *
     * @api {GET} /groupUsers/{id} 04. 查詢某一個group 裡的 一個 groupUsers
     * @apiVersion 0.1.5
     * @apiDescription ・ 查詢某一個group 裡的 一個 groupUsers
     *
     * @apiName  FindGroupUsers
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      id             groupUsers id
     *
     * @apiParamExample {json} Request
        {
            "id":"15079001735434240"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "return group user info",
            "data": [
                {
                    "id": 15079001735434240,
                    "group_id": 15079001701879808,
                    "user_id": 14380081212428288,
                    "role": "admin",
                    "status": "enable",
                    "meeting_enable": "enable",
                    "live_enable": "enable",
                    "recruiter_id": 15079001701879808,
                    "allower_id": 15079001701879808,
                    "updated_at": "2017-11-27 18:27:22",
                    "created_at": "2017-11-23 14:38:34"
                }
            ]
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "group User not fund or delete"
        }
     *
     *
     */

//GroupUsers role
    /**
     * GroupUsers Role
     *
     * @api {PUT} /groupUsers/{groupId}/role/{id} 05. 修改一個group users 的權限
     * @apiVersion 0.1.5
     * @apiDescription ・ 05. 修改一個group users 的權限
     *
     * @apiName  GroupUsersRole
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     * @apiParam {int}                      id             groupUsers id
     * @apiParam {string=admin,recruiter,editor,member}                   role           權限
     *
     * @apiParamExample {json} Request
        {
            "groupId":"15079001701879808",
            "id":"15079001735434240",
            "role":"admin"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "update down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "role is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 400.03
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "Owner can't change"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "group User not fund or delete"
        }
     *
     *
     */

//GroupUsers allow
    /**
     * GroupUsers allow
     *
     * @api {PUT} /groupUsers/{groupId}/allow/{id} 06. 允許一個人加入群組
     * @apiVersion 0.1.5
     * @apiDescription ・ 06. 允許一個人加入群組
     *
     * @apiName  GroupUsersAllow
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     * @apiParam {int}                      id             groupUsers id
     * @apiParam {string=enable,disable}    status         狀態
     *
     * @apiParamExample {json} Request
        {
            "groupId":"15079001701879808",
            "id":"15079001735434240",
            "status":"enable"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "Allow down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "this id Status not init"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "group User not fund or delete"
        }
     *
     *
     */

//GroupUsers meetingEnable
    /**
     * GroupUsers meetingEnable
     *
     * @api {PUT} /groupUsers/{groupId}/meetingEnable/{id} 07. 預約帳號許可
     * @apiVersion 0.1.5
     * @apiDescription ・ 07. 預約帳號許可
     *
     * @apiName  GroupUsersmeetingEnable
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     * @apiParam {int}                      id             groupUsers id
     * @apiParam {string=true,false}                   meeting_enable 是否可以預約,true以及false 不要有“
     *
     * @apiParamExample {json} Request
        {
            "meeting_enable":true
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "Update down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *

     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "no this row"
        }
     *
     * 
     * @apiErrorExample {json}  Response: 403.04
        {
            "status": "fail",
            "code": 403,
            "comment": "meeting enable is error'"
        }
     *
     *
     */

//GroupUsers liveEnable
    /**
     * GroupUsers liveEnable
     *
     * @api {PUT} /groupUsers/{groupId}/liveEnable/{id} 08. 直播帳號的許可
     * @apiVersion 0.1.5
     * @apiDescription ・ 08. 直播帳號的許可
     *
     * @apiName  GroupUserliveEnable
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     * @apiParam {int}                      id             groupUsers id
     * @apiParam {string=true,false}        live_enable 是否可以預約,true以及false 不要有“
     *
     * @apiParamExample {json} Request
        {
            "live_enable":ture
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "Update down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *

     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.03
        {
            "status": "fail",
            "code": 401,
            "comment": "no this row"
        }
     *
     *
     * @apiErrorExample {json}  Response: 403.04
        {
            "status": "fail",
            "code": 403,
            "comment": "live enable is error'"
        }
     *
     *
     */

//GroupUsers ChangeOwner
    /**
     * GroupUsers ChangeOwner
     *
     * @api {PUT} /groupUsers/{groupId}/ChangeOwner/{id} 06. 把一個人設定為owner，原owner 變admin
     * @apiVersion 0.1.5
     * @apiDescription ・ 11. 把一個人設定為owner，原owner 變admin
     *
     * @apiName  GroupUsersChangeOwner
     * @apiGroup GroupUsers
     *
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
        {
            "session": "8f3e973061800dc6ebcb367079b305d8",
            "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
        }
     *
     * @apiParam {int}                      groupId        group id
     * @apiParam {int}                      id             groupUsers id
     * @apiParam {string=owner}             role           權限
     *
     * @apiParamExample {json} Request
        {
            "groupId":"15079001701879808",
            "id":"15079001735434240",
            "role":"owner"
        }
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {string}   data                回傳資訊
     *
     * @apiSuccessExample {json}    Response
        {
            "status": "success",
            "code": 200,
            "comment": "Change down"
        }
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     * @apiError (Error) {string}   data        回傳資訊
     *
     *
     * @apiErrorExample {json}  Response: 400.01
        {
            "status": "fail",
            "code": 400,
            "comment": "id is empty"
        }
     *
     *
     * @apiErrorExample {json}  Response: 400.02
        {
            "status": "fail",
            "code": 400,
            "comment": "groupId is empty"
        }
     *
     * @apiErrorExample {json}  Response: 401.04
        {
            "status": "fail",
            "code": 401,
            "comment": "this id Status not init"
        }
     *
     *
     * @apiErrorExample {json}  Response: 401.05
        {
            "status": "fail",
            "code": 401,
            "comment": "group User not fund or delete"
        }
     *
     *
     */













