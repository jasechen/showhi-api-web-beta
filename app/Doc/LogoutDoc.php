<?php

namespace App\Doc;

/**
     * 使用者登出
     *
     * @api {POST} /logout 03. 登出
     * @apiVersion 0.1.0
     * @apiDescription ・ 使用者登出
     * @apiName PostUserLogout
     * @apiGroup Others
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "logout success",
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     */
