<?php

namespace App\Doc;

/**
     * 查詢角色
     *
     * @api {GET} /role/{id} 01. 查詢角色
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢角色
     * @apiName GetRoleFind
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {int}    id    角色 ID
     *
     * @apiParamExample {json} Request
{
    "id"     : 20162829764136960
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "role": {
            "id": "20162829764136960",
            "type": "general",
            "name": "member",
            "description": "",
            "creater_id": "17208095038115840",
            "updated_at": "2017-12-07 15:19:53",
            "created_at": "2017-12-07 15:19:53",
            "permissions": [
                {
                    "id": "20162829797691392",
                    "type": "admin",
                    "name": "role_create",
                    "description": "",
                    "creater_id": "17208095038115840",
                    "updated_at": "2017-12-07 15:19:53",
                    "created_at": "2017-12-07 15:19:53"
                },
                {
                    "id": "20162829822857216",
                    "type": "admin",
                    "name": "role_update",
                    "description": "",
                    "creater_id": "17208095038115840",
                    "updated_at": "2017-12-07 15:19:53",
                    "created_at": "2017-12-07 15:19:53"
                },
                {
                    "id": "20162829835440128",
                    "type": "admin",
                    "name": "role_delete",
                    "description": "",
                    "creater_id": "17208095038115840",
                    "updated_at": "2017-12-07 15:19:53",
                    "created_at": "2017-12-07 15:19:53"
                }
            ]
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "id error"
}
     *
     */


// ===
/**
     * 查詢角色 by type
     *
     * @api {GET} /role/type/{type} 02. 查詢角色 by type
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢角色 by type
     * @apiName GetRoleFindByType
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string=general,group}    type    種類
     *
     * @apiParamExample {json} Request
{
    "type"     : "general"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "roles": [
            {
                "id": "20162829764136960",
                "type": "general",
                "name": "member",
                "description": "",
                "creater_id": "17208095038115840",
                "updated_at": "2017-12-07 15:19:53",
                "created_at": "2017-12-07 15:19:53",
                "permissions": [
                    {
                        "id": "20162829797691392",
                        "type": "admin",
                        "name": "role_create",
                        "description": "",
                        "creater_id": "17208095038115840",
                        "updated_at": "2017-12-07 15:19:53",
                        "created_at": "2017-12-07 15:19:53"
                    },
                    {
                        "id": "20162829822857216",
                        "type": "admin",
                        "name": "role_update",
                        "description": "",
                        "creater_id": "17208095038115840",
                        "updated_at": "2017-12-07 15:19:53",
                        "created_at": "2017-12-07 15:19:53"
                    },
                    {
                        "id": "20162829835440128",
                        "type": "admin",
                        "name": "role_delete",
                        "description": "",
                        "creater_id": "17208095038115840",
                        "updated_at": "2017-12-07 15:19:53",
                        "created_at": "2017-12-07 15:19:53"
                    }
                ]
            }
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "type is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
     *
     */


// ===
/**
     * 查詢角色 by name
     *
     * @api {GET} /role/name/{name} 03. 查詢角色 by name
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢角色 by name
     * @apiName GetRoleFindByName
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string}    name    角色名稱
     *
     * @apiParamExample {json} Request
{
    "name"     : "member"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "roles": [
            {
                "id": "20162829764136960",
                "type": "general",
                "name": "member",
                "description": "",
                "creater_id": "17208095038115840",
                "updated_at": "2017-12-07 15:19:53",
                "created_at": "2017-12-07 15:19:53",
                "permissions": [
                    {
                        "id": "20162829797691392",
                        "type": "admin",
                        "name": "role_create",
                        "description": "",
                        "creater_id": "17208095038115840",
                        "updated_at": "2017-12-07 15:19:53",
                        "created_at": "2017-12-07 15:19:53"
                    },
                    {
                        "id": "20162829822857216",
                        "type": "admin",
                        "name": "role_update",
                        "description": "",
                        "creater_id": "17208095038115840",
                        "updated_at": "2017-12-07 15:19:53",
                        "created_at": "2017-12-07 15:19:53"
                    },
                    {
                        "id": "20162829835440128",
                        "type": "admin",
                        "name": "role_delete",
                        "description": "",
                        "creater_id": "17208095038115840",
                        "updated_at": "2017-12-07 15:19:53",
                        "created_at": "2017-12-07 15:19:53"
                    }
                ]
            }
        ]
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "name is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
     *
     */


// ===
/**
     * 查詢角色 by type, name
     *
     * @api {GET} /role/{type}/{name} 04. 查詢角色 by type, name
     * @apiVersion 0.1.0
     * @apiDescription ・ 查詢角色 by type, name
     * @apiName GetRoleFindByTypeAndName
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   [token]                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string=general,group}    type    種類
     * @apiParam {string}    name    角色名稱
     *
     * @apiParamExample {json} Request
{
    "type"     : "general",
    "name"     : "member"
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "find success",
    "data":{
        "session" : "8f3e973061800dc6ebcb367079b305d8",
        "role": {
            "id": "20162829764136960",
            "type": "general",
            "name": "member",
            "description": "",
            "creater_id": "17208095038115840",
            "updated_at": "2017-12-07 15:19:53",
            "created_at": "2017-12-07 15:19:53",
            "permissions": [
                {
                    "id": "20162829797691392",
                    "type": "admin",
                    "name": "role_create",
                    "description": "",
                    "creater_id": "17208095038115840",
                    "updated_at": "2017-12-07 15:19:53",
                    "created_at": "2017-12-07 15:19:53"
                },
                {
                    "id": "20162829822857216",
                    "type": "admin",
                    "name": "role_update",
                    "description": "",
                    "creater_id": "17208095038115840",
                    "updated_at": "2017-12-07 15:19:53",
                    "created_at": "2017-12-07 15:19:53"
                },
                {
                    "id": "20162829835440128",
                    "type": "admin",
                    "name": "role_delete",
                    "description": "",
                    "creater_id": "17208095038115840",
                    "updated_at": "2017-12-07 15:19:53",
                    "created_at": "2017-12-07 15:19:53"
                }
            ]
        }
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "type is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "name is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "role type error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
     *
     */

// ===
    /**
     * 新增角色
     *
     * @api {POST} /role 05. 新增角色
     * @apiVersion 0.1.0
     * @apiDescription ・ 新增角色 <br/>・ Permission : admin / role_create
     * @apiName PostRoleCreate
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {string=general,group}    role_type="general"                          角色種類
     * @apiParam {string}    role_name                          角色名稱
     * @apiParam {string}    [role_description]                   角色描述
     * @apiParam {string[]}  permissions                        權限s
     * @apiParam {string=general,group,admin}    permissions.permission_type="general"        權限種類
     * @apiParam {string}    permissions.permission_name        權限名稱
     * @apiParam {string}    [permissions.permission_description] 權限描述
     *
     * @apiParamExample {json} Request
{
    "role_type":"general",
    "role_name":"member",
    "role_description":"",
    "permissions":[
        {
            "permission_type":"admin",
            "permission_name":"role_create",
            "permission_description":""
        },
        {
            "permission_type":"admin",
            "permission_name":"role_update",
            "permission_description":""
        },
        {
            "permission_type":"admin",
            "permission_name":"role_delete",
            "permission_description":""
        }
    ]
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "create success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "role_id": ""
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "role type is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "role name is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.05
{
    "status": "fail",
    "code": 400,
    "comment": "permission type #1, 2 is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.06
{
    "status": "fail",
    "code": 400,
    "comment": "permission name #1, 2 is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "role type error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "create role error"
}
     *
     * @apiErrorExample {json}  Response: 400.07
{
    "status": "fail",
    "code": 400,
    "comment": "permission type empty (#general)"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "create permission error (#general-member)"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "create role permission error (#20162829764136960-20162829797691392)"
}
     *
     */


// ===
    /**
     * 修改角色
     *
     * @api {PUT} /role/{id} 06. 修改角色
     * @apiVersion 0.1.0
     * @apiDescription ・ 修改角色 <br/>・ Permission : admin / role_update
     * @apiName PostRoleUpdate
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {int}    id                      角色 ID
     * @apiParam {string=general,group}    role_type="general"                          角色種類
     * @apiParam {string}    role_name                          角色名稱
     * @apiParam {string}    [role_description]                   角色描述
     * @apiParam {string[]}  permissions                        權限s
     * @apiParam {string=general,group,admin}    permissions.permission_type="general"        權限種類
     * @apiParam {string}    permissions.permission_name        權限名稱
     * @apiParam {string}    [permissions.permission_description] 權限描述
     *
     * @apiParamExample {json} Request
{
    "id" : "",
    "role_type":"general",
    "role_name":"member",
    "role_description":"",
    "permissions":[
        {
            "permission_type":"admin",
            "permission_name":"role_create",
            "permission_description":""
        },
        {
            "permission_type":"admin",
            "permission_name":"role_update",
            "permission_description":""
        },
        {
            "permission_type":"admin",
            "permission_name":"role_delete",
            "permission_description":""
        }
    ]
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "update success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "role_id": ""
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.01
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
     *
     * @apiErrorExample {json}  Response: 403.03
{
    "status": "fail",
    "code": 403,
    "comment": "role type name error"
}
     *
     * @apiErrorExample {json}  Response: 403.04
{
    "status": "fail",
    "code": 403,
    "comment": "role type  error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "update role error"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "delete role permissions error"
}
     *
     * @apiErrorExample {json}  Response: 400.04
{
    "status": "fail",
    "code": 400,
    "comment": "permission type empty (#general)"
}
     *
     * @apiErrorExample {json}  Response: 422.03
{
    "status": "fail",
    "code": 422,
    "comment": "create permission error (#general-member)"
}
     *
     * @apiErrorExample {json}  Response: 422.04
{
    "status": "fail",
    "code": 422,
    "comment": "create role permission error (#20162829764136960-20162829797691392)"
}
     *
     */


// ===
    /**
     * 刪除角色
     *
     * @api {DELETE} /role/{id} 07. 刪除角色
     * @apiVersion 0.1.0
     * @apiDescription ・ 刪除角色 <br/>・ Permission : admin / role_delete
     * @apiName PostRoleDelete
     * @apiGroup Role
     *
     * @apiHeader {string}   session                             Session 代碼
     * @apiHeader {string}   token                             使用者 token
     *
     * @apiHeaderExample {json} Header
{
    "session": "8f3e973061800dc6ebcb367079b305d8",
    "token" : "33a909a7-22d2-5c0e-90c6-833a5e5b30b0"
}
     *
     * @apiParam {int}    id                      角色 ID
     *
     * @apiParamExample {json} Request
{
    "id":""
}
     *
     * @apiSuccess (Success) {string}   status              回傳狀態
     * @apiSuccess (Success) {int}      code                回傳代碼
     * @apiSuccess (Success) {string}   comment             回傳訊息
     * @apiSuccess (Success) {object}   data                回傳資訊
     * @apiSuccess (Success) {string}   data.session   Session 代碼
     *
     * @apiSuccessExample {json}    Response
{
    "status": "success",
    "code": 200,
    "comment": "delete success",
    "data": {
        "session": "8f3e973061800dc6ebcb367079b305d8",
        "role_id": ""
    }
}
     *
     * @apiError (Error) {string}   status      回傳狀態
     * @apiError (Error) {int}      code        回傳代碼
     * @apiError (Error) {string}   comment     回傳訊息
     *
     *
     * @apiErrorExample {json}  Response: 400.01
{
    "status": "fail",
    "code": 400,
    "comment": "session code is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.02
{
    "status": "fail",
    "code": 400,
    "comment": "token is empty"
}
     *
     * @apiErrorExample {json}  Response: 400.03
{
    "status": "fail",
    "code": 400,
    "comment": "id is empty"
}
     *
     * @apiErrorExample {json}  Response: 403.01
{
    "status": "fail",
    "code": 403,
    "comment": "session expire time"
}
     *
     * @apiErrorExample {json}  Response: 403.02
{
    "status": "fail",
    "code": 403,
    "comment": "session token error"
}
     *
     * @apiErrorExample {json}  Response: 404.02
{
    "status": "fail",
    "code": 404,
    "comment": "role error"
}
     *
     * @apiErrorExample {json}  Response: 422.01
{
    "status": "fail",
    "code": 422,
    "comment": "delete role fail"
}
     *
     * @apiErrorExample {json}  Response: 422.02
{
    "status": "fail",
    "code": 422,
    "comment": "delete permission fail"
}
     *
     */
