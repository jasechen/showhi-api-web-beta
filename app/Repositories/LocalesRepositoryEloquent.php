<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Locales;
use App\Validators\LocalesValidator;
use App\Repositories\LocalesRepository;
use App\Repositories\LocalesContentRepositoryEloquent;

/**
 * Class LocalesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LocalesRepositoryEloquent extends BaseRepository implements LocalesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Locales::class;
    }

    /**
     *
     */
    public function isCodeLangExist($code, $lang)
    {
        $this->model->setConnection('read-main');

        $locales = $this->findByCode($code);

        if ($locales->isEmpty()) {
            return false;
        }

        $localesId = $locales->first()->id;

        $localesContentRepository = app(LocalesContentRepositoryEloquent::class);

        $localesContent = $localesContentRepository->findByLocalesIdAndLang($localesId, $lang);

        return $localesContent->isEmpty() ? false : true;
    } // END function

    /**
     *
     */
    public function isCodeExist($code)
    {
        $this->model->setConnection('read-main');

        $locales = $this->findByCode($code);

        return $locales->isEmpty() ? false : true;
    } // END function

    /**
     *
     */
    public function findByCodeAndLang($code, $lang)
    {
        $this->model->setConnection('read-main');

        $locales = $this->findByCode($code);

        if ($locales->isEmpty()) {
            return false;
        }

        $localesId = $locales->first()->id;

        $localesContentRepository = app(LocalesContentRepositoryEloquent::class);

        return $localesContentRepository->findByLocalesIdAndLang($localesId, $lang);
    } // END function

    /**
     *
     */
    public function findByCode($code)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('code', $code);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
