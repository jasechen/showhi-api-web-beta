<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserFriendsRepository
 * @package namespace App\Repositories;
 */
interface UserFriendsRepository extends RepositoryInterface
{
    //
}
