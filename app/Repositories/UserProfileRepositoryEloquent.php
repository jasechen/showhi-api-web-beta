<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserProfileRepository;
use App\Entities\UserProfile;
use App\Validators\UserProfileValidator;

/**
 * Class UserProfileRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserProfileRepositoryEloquent extends BaseRepository implements UserProfileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserProfile::class;
    }

    /**
     *
     */
    public function init($userId, $email, $firstName, $lastName, $mobileCountryCode, $mobilePhone)
    {
        $fields = [
            'user_id' => $userId,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'nickname' => '',
            'avatar' => '',
            'birth' => '',
            'email' => $email,
            'mobile_country_code' => $mobileCountryCode,
            'mobile_phone' => $mobilePhone,
            'city' => '',
            'intro' => '',
            'website' => '',
            'languages' => '',
            'interest_items' => '',
            'countries_visited' => '',
            'pets' => ''
        ];

        $this->create($fields);

        return $this->findByUserId($userId);
    } // END function

    /**
     *
     */
    public function findByMobileCountryCodeAndPhone($mobileCountryCode, $mobilePhone)
    {
        $this->model->setConnection('read-main');

        return $this->findWhere(['mobile_country_code' => $mobileCountryCode, 'mobile_phone' => $mobilePhone]);
    } // END function

    /**
     *
     */
    public function findByNickname($nickname)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('nickname', $nickname);
    } // END function

    /**
     *
     */
    public function findByMobilePhone($mobilePhone)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('mobile_phone', $mobilePhone);
    } // END function

    /**
     *
     */
    public function findByUserId($userId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('user_id', $userId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
