<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Directory;
use App\Validators\DirectoryValidator;
use App\Repositories\DirectoryRepository;

/**
 * Class DirectoryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class DirectoryRepositoryEloquent extends BaseRepository implements DirectoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Directory::class;
    }

    /**
     *
     * @param string    $parentType (blog / group)
     *        integer   $parentId
     *        integer   $boardId
     *        string    $title
     *        string    $description
     *        integer   $ownerId
     *        integer   $createrId
     */
    public function initDir($parentType, $parentId, $boardId, $title, $description, $ownerId, $createrId)
    {
        $fields = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'board_id'  => $boardId,
            'type'  => 'others',
            'title'   => $title,
            'description' => $description,
            'cover'        => '',
            'is_album'     => false,
            'is_default' => false,
            'owner_id'  => $ownerId,
            'creater_id'  => $createrId,
        ];

        $this->create($fields);

        $this->orderBy('created_at', 'DESC');

        $where = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'type'  => 'others',
            'is_album'     => false,
            'is_default' => false,
            'owner_id'  => $ownerId,
        ];

        return $this->findWhere($where);
    } // END function

    /**
     *
     * @param string    $parentType (blog / group)
     *        integer   $parentId
     *        integer   $boardId
     *        string    $title
     *        string    $description
     *        integer   $ownerId
     *        integer   $createrId
     */
    public function initAlbum($parentType, $parentId, $boardId, $title, $description, $ownerId, $createrId)
    {
        $fields = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'board_id'  => $boardId,
            'type'  => 'others',
            'title'   => $title,
            'description' => $description,
            'cover'        => '',
            'is_album'     => true,
            'is_default' => false,
            'owner_id'  => $ownerId,
            'creater_id'  => $createrId,
        ];

        $this->create($fields);

        $this->orderBy('created_at', 'DESC');

        $where = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'type'  => 'others',
            'is_album'     => true,
            'is_default' => false,
            'owner_id'  => $ownerId,
        ];

        return $this->findWhere($where);
    } // END function

    /**
     *
     * @param string    $parentType (blog / group)
     *        integer   $parentId
     *        integer   $ownerId
     */
    public function initDefaultDir($parentType, $parentId, $ownerId)
    {
        $fields = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'board_id'  => 0,
            'type'  => 'no_dir',
            'title'   => 'Default Dir',
            'description' => '',
            'cover'        => '',
            'is_album'     => false,
            'is_default' => true,
            'owner_id'  => $ownerId,
            'creater_id'  => 0,
        ];

        $this->create($fields);

        $this->orderBy('created_at', 'DESC');

        $where = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'type'  => 'no_dir',
            'is_album'     => false,
            'is_default' => true,
            'owner_id'  => $ownerId,
        ];

        return $this->findWhere($where);
    } // END function

    /**
     *
     * @param string    $parentType (blog / group)
     *        integer   $parentId
     *        string    $type  (avatar / cover / others)
     *        integer   $ownerId
     *        integer   $createrId
     */
    public function initDefaultAlbum($parentType, $parentId, $type, $ownerId)
    {
        $fields = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'board_id'  => 0,
            'type'  => $type,
            'title'   => $parentType . ' ' . $type,
            'description' => '',
            'cover'        => '',
            'is_album'     => true,
            'is_default' => true,
            'owner_id'  => $ownerId,
            'creater_id'  => 0,
        ];

        $this->create($fields);

        $this->orderBy('created_at', 'DESC');

        $where = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'type'  => $type,
            'is_album'     => true,
            'is_default' => true,
            'owner_id'  => $ownerId,
        ];

        return $this->findWhere($where);
    } // END function

    /**
     *
     */
    public function findByParentTypeAndParentIdAndTypeAndOwnerId($parentType, $parentId, $type, $ownerId)
    {
        $this->model->setConnection('read-main');

        $where = [
            'parent_type' => $parentType,
            'parent_id' => $parentId,
            'type' => $type,
            'owner_id' => $ownerId
        ];

        return $this->findWhere($where);
    } // END function

    /**
     *
     */
    public function findByOwnerId($ownerId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('owner_id', $ownerId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
