<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\UserRole;
use App\Validators\UserRoleValidator;
use App\Repositories\UserRoleRepository;

/**
 * Class UserRoleRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRoleRepositoryEloquent extends BaseRepository implements UserRoleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserRole::class;
    }

    /**
     *
     */
    public function findByUserIdAndRoleId($userId, $roleId)
    {
        $this->model->setConnection('read-main');

        return $this->findWhere(['user_id' => $userId, 'role_id' => $roleId]);
    } // END function

    /**
     *
     */
    public function findByRoleId($roleId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('role_id', $roleId);
    } // END function

    /**
     *
     */
    public function findByUserId($userId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('user_id', $userId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
