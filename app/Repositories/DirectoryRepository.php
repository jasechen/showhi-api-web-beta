<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DirectoryRepository
 * @package namespace App\Repositories;
 */
interface DirectoryRepository extends RepositoryInterface
{
    //
}
