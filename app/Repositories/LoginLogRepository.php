<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LoginLogRepository
 * @package namespace App\Repositories;
 */
interface LoginLogRepository extends RepositoryInterface
{
    //
}
