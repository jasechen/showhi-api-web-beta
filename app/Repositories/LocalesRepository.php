<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LocalesRepository
 * @package namespace App\Repositories;
 */
interface LocalesRepository extends RepositoryInterface
{
    //
}
