<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\FileRepository;
use App\Entities\File;
use App\Validators\FileValidator;

/**
 * Class FileRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FileRepositoryEloquent extends BaseRepository implements FileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return File::class;
    }

    /**
     * Insert
     *
     * @param  array  $fields
     * @return \App\Entities\File|false
     */
    public function initImageFile($directoryId, $boardId, $filename, $extension, $mimeType, $size, $isCover, $ownerId, $title = '')
    {
        $fields = [
            'directory_id' => $directoryId,
            'board_id'  => $boardId,
            'filename'  => $filename,
            'extension' => $extension,
            'mime_type' => $mimeType,
            'size'      => $size,
            'title'     => empty($title) ? $filename : $title,
            'description' => '',
            'type'      => 'image',
            'is_cover'  => $isCover,
            'owner_id'  => $ownerId,
            'creater_id' => $ownerId,
        ];

        $this->create($fields);

        return $this->findByFilename($filename);
    } // END function

    /**
     *
     */
    public function findByFilename($filename)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('filename', $filename);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
