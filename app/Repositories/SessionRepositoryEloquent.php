<?php

namespace App\Repositories;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Session;
use App\Validators\SessionValidator;
use App\Repositories\SessionRepository;

use App\Repositories\UserRepositoryEloquent;

/**
 * Class SessionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SessionRepositoryEloquent extends BaseRepository implements SessionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Session::class;
    }

    /**
     *
     */
    public function initCode()
    {
        do {
            $code = md5(Uuid::generate(4)->string);
            $session = $this->findByCode($code);
        } while ($session->isNotEmpty());

        return $code;
    } // END function

    /**
     *
     */
    public function isValidCodeAndToken($code, $token)
    {
        $this->model->setConnection('read-main');


        $userRepository = app(UserRepositoryEloquent::class);

        $user = $userRepository->findByToken($token);

        if ($user->isEmpty()) {
            return false;
        } // END if


        $session = $this->findByCodeAndOwnerId($code, $user->first()->id);

        return $session->isEmpty() ? false : true;
    } // END function

    /**
     *
     */
    public function isAlive($code)
    {
        $this->model->setConnection('read-main');

        $session = $this->findByCode($code);

        if ($session->isEmpty()) {
            return false;
        } // END if

        $now = Carbon::now();
        $deadline = $session->first()->updated_at->timestamp + $session->first()->expire_time;

        return ($deadline < $now->timestamp) ? false : true;
    } // END function

    /**
     *
     */
    public function init($deviceId, $remoteAddress)
    {
        $code = $this->initCode();

        $fields = [
            'code'      => $code,
            'device_id' => $deviceId,
            'expire_time' => config('session.lifetime'),
            'remote_address' => $remoteAddress,
        ];

        $this->create($fields);

        return $this->findByCode($code);
    } // END function

    /**
     *
     */
    public function findByCodeAndOwnerId($code, $ownerId)
    {
        $this->model->setConnection('read-main');

        $where = ['code' => $code, 'owner_id' => $ownerId];

        return $this->findWhere($where);
    } // END function

    /**
     *
     */
    public function findByCode($code)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('code', $code);
    } // END function

    /**
     *
     */
    public function findByDeviceId($deviceId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('device_id', $deviceId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
