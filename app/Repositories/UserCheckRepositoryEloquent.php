<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserCheckRepository;
use App\Entities\UserCheck;
use App\Validators\UserCheckValidator;

/**
 * Class UserCheckRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserCheckRepositoryEloquent extends BaseRepository implements UserCheckRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserCheck::class;
    }

    /**
     *
     */
    public function resetCodeAndStatus($id)
    {
        $fields = [
            'code'    => mt_rand(100000, 999999),
            'status'  => 'init',
        ];

        $this->update($fields, $id);

        return $this->findWhere(['id' => $id]);
    } // END function

    /**
     *
     */
    public function initOption($userId, $option)
    {
        $fields = [
            'user_id' => $userId,
            'option' => $option,
            'code' => mt_rand(100000, 999999),
            'status' => 'init',
        ];

        $this->create($fields);

        return $this->findByUserIdAndOption($userId, $option);
    } // END function

    /**
     *
     */
    public function findByUserIdAndOption($userId, $option)
    {
        $this->model->setConnection('read-main');

        $where = ['user_id' => $userId, 'option' => $option];

        return $this->findWhere($where);
    } // END function

    /**
     *
     */
    public function findByUserId($userId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('user_id', $userId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
