<?php

namespace App\Repositories;

use Webpatser\Uuid\Uuid;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Device;
use App\Validators\DeviceValidator;
use App\Repositories\DeviceRepository;

/**
 * Class DeviceRepositoryEloquent
 * @package namespace App\Repositories;
 */
class DeviceRepositoryEloquent extends BaseRepository implements DeviceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Device::class;
    }

    /**
     *
     */
    public function initCode()
    {
        do {
            $code = Uuid::generate(4)->string;
            $device = $this->findByCode($code);
        } while ($device->isNotEmpty());

        return $code;
    } // END function

    /**
     *
     */
    public function init($code, $os = 'macos', $type = 'notebook', $lang = 'en', $token = '')
    {
        $fields = [
            'code'  => $code,
            'os'    => $os,
            'type'  => $type,
            'lang'  => $lang,
            'token' => $token,
        ];

        $this->create($fields);

        return $this->findByCode($code);
    } // END function

    /**
     *
     */
    public function findByCode($code)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('code', $code);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
