<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserEducationRepository
 * @package namespace App\Repositories;
 */
interface UserEducationRepository extends RepositoryInterface
{
    //
}
