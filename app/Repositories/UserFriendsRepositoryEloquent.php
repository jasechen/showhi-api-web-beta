<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\userFriendsRepository;
use App\Entities\UserFriends;
use App\Validators\UserFriendsValidator;

/**
 * Class UserFriendsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserFriendsRepositoryEloquent extends BaseRepository implements UserFriendsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserFriends::class;
    }

    public function changeRead()
    {
        $this->model->setConnection('read-main');
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
