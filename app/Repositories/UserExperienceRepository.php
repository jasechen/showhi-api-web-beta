<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserExperienceRepository
 * @package namespace App\Repositories;
 */
interface UserExperienceRepository extends RepositoryInterface
{
    //
}
