<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\noticesRepository;
use App\Entities\Notices;
use App\Validators\NoticesValidator;

/**
 * Class NoticesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class NoticesRepositoryEloquent extends BaseRepository implements NoticesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notices::class;
    }

    public function init($senderId, $receiverId, $content,$parentType, $parentId)
    {
        $fields =[
            'sender_id' => $senderId,
            'receiver_id' => $receiverId,
            'content'  => $content,
            'parent_type'  => $parentType,
            'parent_id'  => $parentId
        ];
        return $this->create($fields);
    }

    public function changeRead()
    {
        $this->model->setConnection('read-main');
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
