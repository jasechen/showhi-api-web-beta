<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserCheckRepository
 * @package namespace App\Repositories;
 */
interface UserCheckRepository extends RepositoryInterface
{
    //
}
