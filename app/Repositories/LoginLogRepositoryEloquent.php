<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LoginLogRepository;
use App\Entities\LoginLog;
use App\Validators\LoginLogValidator;

/**
 * Class LoginLogRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LoginLogRepositoryEloquent extends BaseRepository implements LoginLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LoginLog::class;
    }

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
