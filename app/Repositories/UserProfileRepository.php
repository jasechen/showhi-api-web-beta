<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserProfileRepository
 * @package namespace App\Repositories;
 */
interface UserProfileRepository extends RepositoryInterface
{
    //
}
