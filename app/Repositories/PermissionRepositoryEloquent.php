<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Permission;
use App\Validators\PermissionValidator;
use App\Repositories\PermissionRepository;

/**
 * Class PermissionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PermissionRepositoryEloquent extends BaseRepository implements PermissionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }

    /**
     *
     */
    public function findByTypeAndName($type, $name)
    {
        $this->model->setConnection('read-main');

        $fields = ['type' => $type, 'name' => $name];

        return $this->findWhere($fields);
    } // END function

    /**
     *
     */
    public function findByName($name)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('name', $name);
    } // END function

    /**
     *
     */
    public function findByType($type)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('type', $type);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
