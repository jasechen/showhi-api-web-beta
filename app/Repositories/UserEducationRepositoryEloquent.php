<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserEducationRepository;
use App\Entities\UserEducation;
use App\Validators\UserEducationValidator;

/**
 * Class UserEducationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserEducationRepositoryEloquent extends BaseRepository implements UserEducationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserEducation::class;
    }

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
