<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserExperienceRepository;
use App\Entities\UserExperience;
use App\Validators\UserExperienceValidator;

/**
 * Class UserExperienceRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserExperienceRepositoryEloquent extends BaseRepository implements UserExperienceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserExperience::class;
    }

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
