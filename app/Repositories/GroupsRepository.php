<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GroupsRepository
 * @package namespace App\Repositories;
 */
interface GroupsRepository extends RepositoryInterface
{
    //
}
