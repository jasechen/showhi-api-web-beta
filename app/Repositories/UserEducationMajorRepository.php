<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserEducationMajorRepository
 * @package namespace App\Repositories;
 */
interface UserEducationMajorRepository extends RepositoryInterface
{
    //
}
