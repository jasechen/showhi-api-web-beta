<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Text;
use App\Validators\TextValidator;
use App\Repositories\TextRepository;

/**
 * Class TextRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TextRepositoryEloquent extends BaseRepository implements TextRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Text::class;
    }

    /**
     *
     */
    public function init($boardId, $createrId = 0)
    {
        $fields = [
            'board_id' => $boardId,
            'content'  => 'Welcome!! This is your FIRST text.',
            'creater_id' => $createrId
        ];

        $this->create($fields);

        return $this->findByBoardId($boardId);
    } // END function

    /**
     *
     */
    public function findByBoardId($boardId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('board_id', $boardId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
