<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TextRepository
 * @package namespace App\Repositories;
 */
interface TextRepository extends RepositoryInterface
{
    //
}
