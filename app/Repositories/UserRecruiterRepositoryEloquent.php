<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRecruiterRepository;
use App\Entities\UserRecruiter;
use App\Validators\UserRecruiterValidator;

/**
 * Class UserRecruiterRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRecruiterRepositoryEloquent extends BaseRepository implements UserRecruiterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserRecruiter::class;
    }

    /**
     *
     */
    public function init($userId, $qrcodeId, $recruiterId, $note = 'web')
    {
        $fields = [
            'user_id' => $userId,
            'recruiter_id' => $recruiterId,
            'qrcode_id' => $qrcodeId,
            'note' => $note
        ];

        $this->create($fields);

        return $this->findWhere(['user_id' => $userId, 'recruiter_id' => $recruiterId]);
    } // END function

    /**
     *
     */
    public function findByUserIdAndRecruiterid($userId, $recruiterId)
    {
        $this->model->setConnection('read-main');

        return $this->findWhere(['user_id' => $userId, 'recruiter_id' => $recruiterId]);
    } // END function

    /**
     *
     */
    public function findByUserId($userId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('user_id', $userId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
