<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GroupUsersRepository
 * @package namespace App\Repositories;
 */
interface GroupUsersRepository extends RepositoryInterface
{
    //
}
