<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\GroupsRepository;
use App\Entities\Groups;
use App\Validators\GroupsValidator;

/**
 * Class GroupsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class GroupsRepositoryEloquent extends BaseRepository implements GroupsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Groups::class;
    }

    public function changeRead()
    {
        $this->model->setConnection('read-main');
    }

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
