<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserEducationMajorRepository;
use App\Entities\UserEducationMajor;
use App\Validators\UserEducationMajorValidator;

/**
 * Class UserEducationMajorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserEducationMajorRepositoryEloquent extends BaseRepository implements UserEducationMajorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserEducationMajor::class;
    }

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
