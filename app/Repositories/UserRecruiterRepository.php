<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRecruiterRepository
 * @package namespace App\Repositories;
 */
interface UserRecruiterRepository extends RepositoryInterface
{
    //
}
