<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Blog;
use App\Validators\BlogValidator;
use App\Repositories\BlogRepository;

/**
 * Class BlogRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BlogRepositoryEloquent extends BaseRepository implements BlogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Blog::class;
    }

    /**
     *
     */
    public function init($ownerId, $createrId = 0)
    {
        $fields = [
            'intro' => '',
            'cover' => '',
            'owner_id' => $ownerId,
            'creater_id' => $createrId
        ];

        $this->create($fields);

        return $this->findByOwnerId($ownerId);
    } // END function

    /**
     *
     */
    public function findByOwnerId($ownerId, $columns = ['*'])
    {
        $this->model->setConnection('read-main');

        return $this->findByField('owner_id', $ownerId, $columns);
    } // END function

    /**
     *
     */
    public function findById($id, $columns = ['*'])
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id, $columns);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
