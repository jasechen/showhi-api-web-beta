<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NoticesRepository
 * @package namespace App\Repositories;
 */
interface NoticesRepository extends RepositoryInterface
{
    //
}
