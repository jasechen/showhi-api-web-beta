<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\LocalesContent;
use App\Validators\LocalesContentValidator;
use App\Repositories\LocalesContentRepository;

/**
 * Class LocalesContentRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LocalesContentRepositoryEloquent extends BaseRepository implements LocalesContentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LocalesContent::class;
    }

    /**
     *
     */
    public function isLangExist($lang)
    {
        $this->model->setConnection('read-main');

        $localeContents = $this->findByLang($lang);

        return $localeContents->isEmpty() ? false : true;
    } // END function

    /**
     *
     */
    public function findByLang($lang)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('lang', $lang);
    } // END function

    /**
     *
     */
    public function findByLangAndContent($lang, $content)
    {
        $this->model->setConnection('read-main');

        $where = ['lang' => $lang, 'content' => $content];

        return $this->findWhere($where);
    } // END function

    /**
     *
     */
    public function findByLocalesIdAndLang($localesId, $lang)
    {
        $this->model->setConnection('read-main');

        $where = ['locales_id' => $localesId, 'lang' => $lang];

        return $this->findWhere($where);
    } // END function

    /**
     *
     */
    public function findByLocalesId($localesId, $sorts = [], $page = -1, $limit = 20)
    {

        $this->model->setConnection('read-main');

        return $this->findByField('locales_id', $localesId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
