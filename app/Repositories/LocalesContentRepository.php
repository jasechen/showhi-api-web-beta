<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LocalesContentRepository
 * @package namespace App\Repositories;
 */
interface LocalesContentRepository extends RepositoryInterface
{
    //
}
