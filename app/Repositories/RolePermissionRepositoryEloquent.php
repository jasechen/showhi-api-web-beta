<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\RolePermission;
use App\Validators\RolePermissionValidator;
use App\Repositories\RolePermissionRepository;

/**
 * Class RolePermissionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RolePermissionRepositoryEloquent extends BaseRepository implements RolePermissionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RolePermission::class;
    }

    /**
     *
     */
    public function findByRoleIdAndPermissionId($roleId, $permissionId)
    {
        $this->model->setConnection('read-main');

        $where = ['role_id' => $roleId, 'permission_id' => $permissionId];

        return $this->findWhere($where);
    } // END function

    /**
     *
     */
    public function findByPermissionId($permissionId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('permission_id', $permissionId);
    } // END function

    /**
     *
     */
    public function findByRoleId($roleId)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('role_id', $roleId);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
