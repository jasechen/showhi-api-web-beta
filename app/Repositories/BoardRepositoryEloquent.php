<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Board;
use App\Validators\BoardValidator;
use App\Repositories\BoardRepository;

/**
 * Class BoardRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BoardRepositoryEloquent extends BaseRepository implements BoardRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Board::class;
    }

    /**
     *
     */
    public function init($blogId, $createrId = 0)
    {
        $fields = [
            'parent_type' => 'blog',
            'parent_id'   => $blogId,
            'text_id' => 0,
            'directory_id' => 0,
            'has_file' => false,            
            'creater_id'  => $createrId
        ];

        $this->create($fields);

        return $this->findByParentTypeAndParentId('blog', $blogId);
    } // END function

    /**
     *
     */
    public function findByParentTypeAndParentId($parentType, $parentId)
    {
        $this->model->setConnection('read-main');

        return $this->findWhere(['parent_type' => $parentType, 'parent_id' => $parentId]);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
