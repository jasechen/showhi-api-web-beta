<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\CountryProp;
use App\Validators\CountryPropValidator;
use App\Repositories\CountryPropRepository;

/**
 * Class CountryPropRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CountryPropRepositoryEloquent extends BaseRepository implements CountryPropRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CountryProp::class;
    }

    public function findCallingCodes()
    {
        $this->model->setConnection('read-main');

        return $this->findWhere(['calling_code' => ['calling_code', '!=', '']], ['code', 'calling_code']);
    } // END function

    /**
     *
     */
    public function findByCode($code)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('code', $code);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
