<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SessionRepository
 * @package namespace App\Repositories;
 */
interface SessionRepository extends RepositoryInterface
{
    //
}
