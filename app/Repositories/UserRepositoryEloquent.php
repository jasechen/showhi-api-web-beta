<?php

namespace App\Repositories;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\User;
use App\Validators\UserValidator;
use App\Repositories\UserRepository;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     *
     */
    public function initToken($account = '')
    {
        $now = Carbon::now();
        $account = empty($account) ? mt_rand(10000000, 99999999) : $account;

        do {
            $tokenName = $account . "_" . $now->timestamp;
            $token = Uuid::generate(5, $tokenName, Uuid::NS_DNS)->string;

            $user = $this->findByToken($token);
        } while ($user->isNotEmpty());

        return $token;
    } // END function

    /**
     *
     */
    public function init($account, $password, $introducerId, $type = 'member', $generateQrcode = false)
    {
        $fields = [
            'account' => $account,
            'password' => password_hash($password, PASSWORD_BCRYPT),
            'status' => 'init',
            'token' => $this->initToken($account),
            'type' => $type,
            'generate_qrcode' => $generateQrcode,
            'introducer_id' => $introducerId,
        ];

        $this->model->create($fields);

        return $this->findByAccount($account);
    } // END function

    /**
     *
     */
    public function findByAccount($account)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('account', $account);
    } // END function

    /**
     *
     */
    public function findByToken($token)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('token', $token);
    } // END function

    /**
     *
     */
    public function findById($id)
    {
        $this->model->setConnection('read-main');

        return $this->findByField('id', $id);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
