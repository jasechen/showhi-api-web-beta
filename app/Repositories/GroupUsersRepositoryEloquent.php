<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\GroupUsersRepository;
use App\Entities\GroupUsers;
use App\Validators\GroupUsersValidator;

/**
 * Class GroupUsersRepositoryEloquent
 * @package namespace App\Repositories;
 */
class GroupUsersRepositoryEloquent extends BaseRepository implements GroupUsersRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return GroupUsers::class;
    }

    public function changeRead()
    {
        $this->model->setConnection('read-main');
    }

    /**
     *
     */
    public function findByGroupIdAndUserId($groupId, $userId)
    {
        $this->model->setConnection('read-main');

        $where = [
            'group_id' => $groupId,
            'user_id' => $userId
        ];

        return $this->findWhere($where);
    } // END function

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
