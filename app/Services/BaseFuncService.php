<?php
namespace App\Services;
use Illuminate\Http\Request;
use App\Traits\JsonResponseTrait;

use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;

class BaseFuncService
{
    use JsonResponseTrait;
    public function __construct(){
        $this->sessionRepository  = app(SessionRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
    }

    /**
     * idToString 指定欄位數字轉文字
     * @param array    $returnArr   returnArr
     * @param array    $tmpArr      checkArr
     *
     */

    public function idToString($returnArr,$tmpArr){
        foreach($returnArr as $key =>$value ){
            foreach ($tmpArr as $key1 =>$value2 ){
                $returnArr[$key][$value2] = (string) $value[$value2];
            }
        }
        return $returnArr;
    }

    //確認類似type 這種欄位有無亂打
    public function checkTypeFiled($type,$tmpList){
        $tmpArr = explode(",",$tmpList);
        if($type != null)
        if(!in_array($type,$tmpArr)){
            return false;
        }
        return true;
    }

    public function checkEmptyFiled($filedArr,$tmpArr){
        foreach ($tmpArr as $value){
            if(!array_key_exists($value,$filedArr)){
                $code ='400';
                $comment =$value.' is empty';
                $this->failResponse($comment,$code);
            }
        }
        return true;
    }

    //確認session沒有過期 並且傳回 user_id
    public function getSessionUserId($request){
        //get session 不能為空
        $code = 400;
        $sessionCode  = $request->header('session');
        if($sessionCode != null){
            //確認session 沒有過期
            $session = $this->sessionRepository->findByCode($sessionCode);
            if ($session->isEmpty()) {
                $code = 404;
                $comment = 'session is emtpy';
                $this->failResponse($comment, $code);
            } // END if

            $isAlive = $this->sessionRepository->isAlive($sessionCode);
            if (empty($isAlive)) {
                $code = 403;
                $comment = 'session expire time';
                $this->failResponse($comment, $code);
            } // END if

                //抓取create_id
                $seesionArr =$session->toArray();
                $owner_id = $seesionArr['0']['owner_id'];
            //上面常用判斷
            return $owner_id;
        }
        return '';

    }

    //抓user資料
    public function getUserByAcoount($account){
        $user = $this->userRepository->findWhere(
            [
                'account'=>$account
            ]);
        $userArr = $user->toArray();
        if(count($userArr) !=1){
            $code = 404;
            $comment = 'User not here';
            $this->failResponse($comment, $code);
        }
        if($userArr['0']['status'] !='enable'){
            $code = 401;
            $comment = 'User no Permissions';
            $this->failResponse($comment, $code);
        }
        return $userArr;
    }

    //return userArr
    public function getSessionUserArr($request){
        //get session 不能為空
        $code ='400';
        $sessionCode  = $request->header('session');
        if($sessionCode == null){
            $comment ='session is empty';
            $this->failResponse($comment,$code);
        }
            //確認session 沒有過期
        $session = $this->sessionRepository->findByCode($sessionCode);
                if ($session->isEmpty()) {
                    $code = 404;
                    $comment = 'session error';
                    $this->failResponse($comment, $code);
                } // END if

                $isAlive = $this->sessionRepository->isAlive($sessionCode);
                if (empty($isAlive)) {
                    $code = 403;
                    $comment = 'session expire time';
                    $this->failResponse($comment, $code);
                } // END if

            //抓取create_id
        $seesionArr =$session->toArray();
        $owner_id = $seesionArr['0']['owner_id'];

            //抓取account
        $user = $this->userRepository->findWhere([
            'id' =>$owner_id
            ]);
        if (count($user) ==0) {
            $code = 404;
            $comment = 'user error';
            $this->failResponse($comment, $code);
        } // END if
        $userArr =$user->toArray();
        //學生不能產生建立群組
        if($userArr['0']['type'] =='member'){
            $code = 401;
            $comment = 'type no permissions';
            $this->failResponse($comment, $code);
        }
        return $userArr;
    }

}