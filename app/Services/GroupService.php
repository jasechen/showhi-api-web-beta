<?php
namespace App\Services;
use Illuminate\Http\Request;
use App\Traits\JsonResponseTrait;

use App\Repositories\GroupsRepositoryEloquent;
use App\Repositories\GroupUsersRepositoryEloquent;
use App\Repositories\SessionRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;

class GroupService
{
    use JsonResponseTrait;
    public function __construct(){
        $this->groupsRepository  = app(GroupsRepositoryEloquent::class);
        $this->groupUsersRepository  = app(GroupUsersRepositoryEloquent::class);
        $this->sessionRepository  = app(SessionRepositoryEloquent::class);
        $this->userRepository  = app(UserRepositoryEloquent::class);
    }

    //確認成員是否在裡面 並返回資訊
    public function checkGroupUser($groupId,$userId){
        $user = $this->groupUsersRepository->findWhere(
            [
                'user_id'=>$userId,
                'group_id'=>$groupId
            ]);

        if (count($user) ==0) {
            $code = 404;
            $comment = 'groupUser error';
            $this->failResponse($comment, $code);
        } // END if
        $userArr = $user->toArray();

        return $userArr;
    }
    //如果user 在group 裡面或的相關資訊
    public function getGroupUser($groupId,$userId){
        $user = $this->groupUsersRepository->findWhere(
            [
                'user_id'=>$userId,
                'group_id'=>$groupId
            ]);
        $userArr = $user->toArray();
        return $userArr;
    }



    //取得group擁有者id
    public function getGroupOwnerId($groupId){
        //取得group ownerId
        $tmp = $this->groupsRepository->findWhere(
            [
                'id'=>$groupId,
                ['status','!=','delete']
            ]);
        if (count($tmp) ==0) {
            $code = 404;
            $comment = 'group error or delete';
            $this->failResponse($comment, $code);
        } // END if
        $tmpArr = $tmp->toArray();
        return $tmpArr['0']['owner_id'];
    }






}