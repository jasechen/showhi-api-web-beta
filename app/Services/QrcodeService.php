<?php
namespace App\Services;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use Illuminate\Http\Request;
use App\Traits\JsonResponseTrait;
class QrcodeService
{
    use JsonResponseTrait;
    public function __construct(){
        $this->qrcode = new BaconQrCodeGenerator;
    }

    /**
     * 建立QRcode
     * @param string    $id          default qrcode id
     * @param string    $parentType  default blog or group
     * @param string    $parentId    default   blog id or group id
     *
     */

    public function create($id,$parentType,$parentId,$baseUrl =null , $encoding =null ,$size =null , $smlSize = null){
        $savePath = base_path('public/qrcode/'.$parentType.'_'.$parentId.'/');
        $fileName = null;
        $baseUrl   = $baseUrl?$baseUrl:config('qrcodes.QRCODES_BASE_URL');
        $encoding = $encoding?$encoding:config('qrcodes.QRCODES_ENCODING');
        $size    = $size?$size:config('qrcodes.QRCODES_SIZE');
        $smlSize    = $smlSize?$smlSize:config('qrcodes.QRCODES_SML_SIZE');

        //建立隨機檔名
        $timeStamp = strtotime("now");
        $retult['fileName'] =$timeStamp;

        //建立驗證ＵＲＬ
        $retult['url'] = $baseUrl.$id;
        $retult['savePath'] = $savePath;
        //使用Qrcode

        //大圖
        $this->qrcode->format('png')->encoding($encoding)->size($size)->
        generate($retult['url'],$savePath.$retult['fileName'].'_n.png');

        //小圖
        $this->qrcode->format('png')->encoding($encoding)->size($smlSize)->
        generate($retult['url'],$savePath.$retult['fileName'].'_sq.png');
        return $retult;
    }

    public function getImageLink($qrcode_arr){
        $filePath    = config('qrcodes.QRCODES_FILE_PATH');
        $sizeNameArr    = config('qrcodes.QRCODES_SIZE_NAME_ARR');

        foreach($qrcode_arr as $key => $value){
            foreach($sizeNameArr as $key1 =>$value1){
                $qrcode_arr[$key]['path'][] = $filePath.$value['parent_type'].'_'.$value['parent_id'].'/'.$value['filename'].'_'.$value1.'.'.'pmg';
            }
        }
        return $qrcode_arr;
    }

}