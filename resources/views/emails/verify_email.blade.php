<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <container>
        <header></header>
        <main>
            <p>Hi {{ $account }},</p>
            <p>
            歡迎你進入 ShowHi 學海網「開啟帳號」流程，為了開啟你的帳號，請點選下方連結<br />
            <a href='{{ $link }}'/>{{ $link }}</a><br />
            進入「開啟帳號頁」後，輸入此驗證碼「{{ $code }}」。
            </p>
        </main>
        <footer>
            <p><strong>ShowHi 學海網團隊</strong> 敬上</p>
            <p>-- 此信件由系統自動發信通知，請勿直接回覆 --</p>
        </footer>
        </container>
    </body>
</html>

