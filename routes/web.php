<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'blog'], function () use ($router) {
    $router->get('{id}', 'BlogsController@find');

    $router->put('{id}/intro', ['middleware'=>'permission:general,blog_update', 'uses' => 'BlogsController@changeIntro']);
    $router->put('{id}/cover', ['middleware'=>'permission:general,blog_update', 'uses' => 'BlogsController@uploadCover']);
});

$router->group(['prefix' => 'board'], function () use ($router) {
    $router->post('text',  ['middleware'=>'permission:general,text_create', 'uses' => 'BoardsController@createText']);
    $router->post('image', ['middleware'=>'permission:general,file_create', 'uses' => 'BoardsController@uploadImage']);
});

$router->group(['prefix' => 'country'], function () use ($router) {
    $router->get('{lang}', 'CountryController@findByLang');
    $router->get('callings/{lang}', 'CountryController@findCallingCodesByLang');
});

$router->group(['prefix' => 'groups'], function ()  use ($router) {
    $router->put('{id}',['middleware'=>'permission:group,group_update','as' =>'id', 'uses' => 'GroupsController@update']);
    $router->put('{id}/cover', ['middleware'=>'permission:group,group_update','as' =>'id', 'uses' => 'GroupsController@uploadCover']);
    $router->put('{id}/avatar',['middleware'=>'permission:group,group_update','as' =>'id', 'uses' => 'GroupsController@uploadAvatar']);

    $router->delete('{id}',['middleware'=>'permission:group,group_delete','as' =>'id', 'uses' => 'GroupsController@delete']);

    $router->get('','GroupsController@list');
    $router->get('myOwnList','GroupsController@myOwnList');
    $router->get('myAddList','GroupsController@myAddList');
    $router->post('','GroupsController@create');
    $router->get('{id}','GroupsController@findIdInfo');
    //要加權限

});

$router->group(['prefix' => 'groupUsers'], function ()  use ($router) {
    $router->post('',['middleware'=>'permission:group,member_create','as' =>'groupId', 'uses' => 'groupUsersController@create']);
    $router->delete('{groupId}/delete/{id}',['middleware'=>'permission:group,member_delete','as' =>'groupId', 'uses' => 'groupUsersController@delete']);
    $router->put('{groupId}/allow/{id}',['middleware'=>'permission:group,member_create','as' =>'groupId', 'uses' => 'groupUsersController@allow']);
    $router->put('{groupId}/role/{id}',['middleware'=>'permission:group,member_update_permission','as' =>'groupId', 'uses' => 'groupUsersController@role']);
    $router->put('{groupId}/meetingEnable/{id}',['middleware'=>'permission:group,member_update_meeting','as' =>'groupId', 'uses' => 'groupUsersController@meetingEnable']);

    $router->put('{groupId}/liveEnable/{id}',['middleware'=>'permission:group,member_update_live','as' =>'groupId', 'uses' => 'groupUsersController@liveEnable']);
    $router->put('{groupId}/changeOwner/{id}',['middleware'=>'permission:group,group_update_owner','as' =>'groupId', 'uses' => 'groupUsersController@changeOwner']);

    $router->get('{id}/list','groupUsersController@list');
    $router->post('join','groupUsersController@join');
    $router->delete('{groupId}/selfDelete/{id}','groupUsersController@selfDelete');
    $router->get('{id}','groupUsersController@findIdInfo');

    //要加權限


});

$router->get('interest/{lang}', 'InterestsController@findByLang');

$router->get('language/{lang}', 'LanguagesController@findByLang');

$router->group(['prefix' => 'locales'], function () use ($router) {
    $router->get('langs',         'LocalesController@findLangs');
    $router->get('lang/{lang}',   'LocalesController@findByLang');
    $router->get('code/{code}',   'LocalesController@findByCode');
    $router->get('{lang}/{code}', 'LocalesController@findByLangAndCode');

    $router->post('',       ['middleware'=>'permission:admin,locales_create', 'uses' => 'LocalesController@create']);
    $router->put('{id}',    ['middleware'=>'permission:admin,locales_update', 'uses' => 'LocalesController@update']);
    $router->delete('{id}', ['middleware'=>'permission:admin,locales_delete', 'uses' => 'LocalesController@delete']);
    $router->put('lang/{lang}/makefile', ['middleware'=>'permission:admin,locales_update', 'uses' => 'LocalesController@makeFilesByLang']);
});

$router->post('login', 'LoginController@index');

$router->post('logout', 'LogoutController@index');

$router->group(['prefix' => 'notices'], function ()  use ($router) {
    $router->get('{parentType}/list/{status}','noticesController@list');
    $router->post('','noticesController@create');
    $router->delete('{id}','noticesController@delete');
    $router->put('{id}/readed','noticesController@readed');
    $router->get('{id}','noticesController@findIdInfo');
    $router->put('{id}','noticesController@update');
});

$router->group(['prefix' => 'qrcodes'], function ()  use ($router) {
    $router->get('','QrcodesController@list');
    $router->get('{groupId}/groupList','QrcodesController@groupList');
    $router->post('','QrcodesController@create');
    $router->delete('{id}','QrcodesController@delete');
    $router->put('{id}/updatenote','QrcodesController@updateNote');
    $router->put('{id}/updateusernote','QrcodesController@updateUserNote');
    $router->get('{id}/check','QrcodesController@check');
    $router->get('{id}','QrcodesController@findIdInfo');
});

$router->post('register', 'RegisterController@index');

$router->group(['prefix' => 'role'], function () use ($router) {
    $router->get('{id}',          'RolesController@find');
    $router->get('type/{type}',   'RolesController@findByType');
    $router->get('name/{name}',   'RolesController@findByName');
    $router->get('{type}/{name}', 'RolesController@findByTypeAndName');

    $router->post('',       ['middleware'=>'permission:admin,role_create', 'uses' => 'RolesController@create']);
    $router->put('{id}',    ['middleware'=>'permission:admin,role_update', 'uses' => 'RolesController@update']);
    $router->delete('{id}', ['middleware'=>'permission:admin,role_delete', 'uses' => 'RolesController@delete']);
});

$router->group(['prefix' => 'session'], function () use ($router) {
    $router->post('init', 'SessionsController@init');
    $router->get('{code}', 'SessionsController@findByCode');
});

$router->group(['prefix' => 'user'], function () use ($router) {
    $router->get('{id}/email/notify', 'UsersController@sendVerifyEmailNotify');
    $router->put('{id}/email/verify', 'UsersController@verifyEmail');

    $router->get('{id}/mobile/notify', 'UsersController@sendVerifyMobileNotify');
    $router->put('{id}/mobile/verify', 'UsersController@verifyMobile');

    $router->put('{id}/qrcode/verify', 'UsersController@verifyQRCode');

    $router->post('password/notify', 'UsersController@sendForgotPasswordNotify');
    $router->put('{id}/password/reset', 'UsersController@resetPassword');

    $router->get('token/{token}', 'UsersController@findByToken');
    $router->get('{id}', 'UsersController@find');
    $router->get('{id}/role', 'UsersController@findRole');
    $router->put('{id}/role', ['middleware'=>'permission:admin,user_update', 'uses' => 'UsersController@updateRole']);

    $router->put('{id}/password', ['middleware'=>'permission:general,member_update', 'uses' => 'UsersController@changePassword']);

    $router->put('{id}/avatar',  ['middleware'=>'permission:general,member_update', 'uses' => 'UserProfilesController@uploadAvatar']);
    $router->put('{id}/profile', ['middleware'=>'permission:general,member_update', 'uses' => 'UserProfilesController@update']);
});

$router->group(['prefix' => 'userFriends'], function ()  use ($router) {
    $router->post('','userFriendsController@create');
    $router->get('{status}/list','userFriendsController@list');
    $router->get('{id}','userFriendsController@findIdInfo');
    $router->put('{id}/agreeFriends','userFriendsController@agreeFriends');
    $router->delete('{id}','userFriendsController@delete');
});