<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);


$app->configure('app');
$app->configure('country');
$app->configure('country_callings');
$app->configure('database');
$app->configure('default_country_props');
$app->configure('default_locales_contents');
$app->configure('default_locales');
$app->configure('device');
$app->configure('filesystems');
$app->configure('genders');
$app->configure('image');
$app->configure('interest');
$app->configure('languages');
$app->configure('locales');
$app->configure('mail');
$app->configure('notices');
$app->configure('pets');
$app->configure('qrcodes');
$app->configure('repository');
$app->configure('role');
$app->configure('role_permissions');
$app->configure('session');
$app->configure('snowflake');
$app->configure('twilio');



$app->withFacades();

$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);


/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
    App\Http\Middleware\CorsMiddleware::class,
    Illuminate\Session\Middleware\StartSession::class

]);

$app->routeMiddleware([
    // 'auth' => App\Http\Middleware\Authenticate::class,
    'permission' => App\Http\Middleware\CheckPermission::class
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// $app->make('queue');

$app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);
$app->register(App\Providers\RepositoryProvider::class);
$app->register(Illuminate\Mail\MailServiceProvider::class);

$app->register(Illuminate\Session\SessionServiceProvider::class);
$app->alias('session', 'Illuminate\Session\SessionManager');

$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);
$app->alias('filesystem', 'Illuminate\Session\FilesystemManager');

$app->register(Flipbox\LumenGenerator\LumenGeneratorServiceProvider::class);
$app->register(Kra8\Snowflake\SnowflakeServiceProvider::class);
$app->register(Prettus\Repository\Providers\LumenRepositoryServiceProvider::class);
$app->register(Propaganistas\LaravelPhone\PhoneServiceProvider::class);
$app->register(SimpleSoftwareIO\QrCode\QrCodeServiceProvider::class);
$app->register(Wn\Generators\CommandsServiceProvider::class);

$app->register(Intervention\Image\ImageServiceProvider::class);
$app->alias('Image', 'Intervention\Image\Facades\Image');

$app->register(Aloha\Twilio\Support\Laravel\ServiceProvider::class);
$app->alias('Twilio', 'Aloha\Twilio\Support\Laravel\Facade');

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/


$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

return $app;
