<?php

$interests = [
    'anime',
    'art',
    'book',
    'business',
    'car',
    'design',
    'education',
    'entertainment',
    'esports',
    'estate',
    'family',
    'fashion',
    'finance',
    'food',
    'gardening',
    'health',
    'history',
    'international',
    'language',
    'movie',
    'music',
    'news',
    'psychic',
    'religion',
    'science',
    'shopping',
    'software',
    'sports',
    'technology',
    'travel',
    'others'
];


return $interests;
