<?php

$roles = [

    [
        'type' => 'general',
        'name' => 'admin',
        'description' => '網站管理員',
        'permissions' => [
            ['type' => 'admin', 'name' => 'role_create', 'description' => '新增角色'],
            ['type' => 'admin', 'name' => 'role_update', 'description' => '更新角色'],
            ['type' => 'admin', 'name' => 'role_delete', 'description' => '刪除角色'],

            ['type' => 'admin', 'name' => 'locales_create', 'description' => '新增語系'],
            ['type' => 'admin', 'name' => 'locales_update', 'description' => '更新語系'],
            ['type' => 'admin', 'name' => 'locales_delete', 'description' => '刪除語系'],

            ['type' => 'admin', 'name' => 'user_create', 'description' => '新增使用者'],
            ['type' => 'admin', 'name' => 'user_update', 'description' => '更新使用者'],
            ['type' => 'admin', 'name' => 'user_delete', 'description' => '刪除使用者'],


        ]
    ],
    [
        'type' => 'general',
        'name' => 'member',
        'description' => '網站會員',
        'permissions' => [

            ['type' => 'general', 'name' => 'member_update', 'description' => '更新自己資料'],
            ['type' => 'general', 'name' => 'blog_update',   'description' => '更新部落格資料'],

            ['type' => 'general', 'name' => 'text_create', 'description' => '新增文章'],
            ['type' => 'general', 'name' => 'text_update', 'description' => '更新文章'],
            ['type' => 'general', 'name' => 'text_delete', 'description' => '刪除文章'],

            ['type' => 'general', 'name' => 'directory_create', 'description' => '新增資料夾(相簿)'],
            ['type' => 'general', 'name' => 'directory_update', 'description' => '更新資料夾(相簿)'],
            ['type' => 'general', 'name' => 'directory_delete', 'description' => '刪除資料夾(相簿)'],

            ['type' => 'general', 'name' => 'file_create', 'description' => '新增檔案(相片)'],
            ['type' => 'general', 'name' => 'file_update', 'description' => '更新檔案(相片)'],
            ['type' => 'general', 'name' => 'file_delete', 'description' => '刪除檔案(相片)'],

            ['type' => 'general', 'name' => 'comment_create', 'description' => '新增回應'],
            ['type' => 'general', 'name' => 'comment_update', 'description' => '更新回應'],
            ['type' => 'general', 'name' => 'comment_delete', 'description' => '刪除回應'],

            ['type' => 'general', 'name' => 'friend_create', 'description' => '新增好友'],
            ['type' => 'general', 'name' => 'friend_update', 'description' => '更新好友'],
            ['type' => 'general', 'name' => 'friend_delete', 'description' => '刪除好友'],

        ]
    ],

    [
        'type' => 'group',
        'name' => 'owner',
        'description' => '群組擁有者',
        'permissions' => [
            ['type' => 'group', 'name' => 'publish_create', 'description' => '新增群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_update', 'description' => '更新群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_delete', 'description' => '刪除群組文章/圖片/影片/直播'],

            ['type' => 'group', 'name' => 'member_create', 'description' => '新增群組會員'],
            ['type' => 'group', 'name' => 'member_delete', 'description' => '刪除群組會員'],
            ['type' => 'group', 'name' => 'member_update_permission',  'description' => '更新群組會員權限'],
            ['type' => 'group', 'name' => 'member_update_live',     'description' => '更新群組會員直播'],
            ['type' => 'group', 'name' => 'member_update_meeting',     'description' => '更新群組會員會議'],
            ['type' => 'group', 'name' => 'group_update_owner',     'description' => '更新群組擁有者'],
            ['type' => 'group', 'name' => 'group_delete',     'description' => '刪除群組'],

            ['type' => 'group', 'name' => 'group_update', 'description' => '更新群組資訊'],
            ['type' => 'group', 'name' => 'report_read', 'description' => '觀看洞察報告'],

            ['type' => 'general', 'name' => 'comment_create', 'description' => '新增回應'],
            ['type' => 'general', 'name' => 'comment_update', 'description' => '更新回應'],
            ['type' => 'general', 'name' => 'comment_delete', 'description' => '刪除回應'],

        ]
    ],
    [
        'type' => 'group',
        'name' => 'admin',
        'description' => '群組管理員',
        'permissions' => [
            ['type' => 'group', 'name' => 'publish_create', 'description' => '新增群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_update', 'description' => '更新群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_delete', 'description' => '刪除群組文章/圖片/影片/直播'],

            ['type' => 'group', 'name' => 'meeting_create', 'description' => '新增會議'],
            ['type' => 'group', 'name' => 'meeting_update', 'description' => '更新會議'],
            ['type' => 'group', 'name' => 'meeting_delete', 'description' => '刪除會議'],

            ['type' => 'group', 'name' => 'member_create', 'description' => '新增群組會員'],
            ['type' => 'group', 'name' => 'member_delete', 'description' => '刪除群組會員'],
            ['type' => 'group', 'name' => 'member_update_permission',  'description' => '更新群組會員權限'],
            ['type' => 'group', 'name' => 'member_update_meeting',     'description' => '更新群組會員會議'],
            ['type' => 'group', 'name' => 'member_update_live',     'description' => '更新群組會員直播'],
            ['type' => 'group', 'name' => 'member_update_make_qrcode', 'description' => '更新群組會員 QRCode'],
            ['type' => 'group', 'name' => 'member_update_recruiter',   'description' => '更新群組會員招募人'],

            ['type' => 'group', 'name' => 'group_update', 'description' => '更新群組資訊'],
            ['type' => 'group', 'name' => 'report_read', 'description' => '觀看洞察報告'],

            ['type' => 'general', 'name' => 'comment_create', 'description' => '新增回應'],
            ['type' => 'general', 'name' => 'comment_update', 'description' => '更新回應'],
            ['type' => 'general', 'name' => 'comment_delete', 'description' => '刪除回應'],

        ]
    ],
    [
        'type' => 'group',
        'name' => 'recruiter',
        'description' => '群組招募者',
        'permissions' => [
            ['type' => 'group', 'name' => 'publish_create', 'description' => '新增群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_update', 'description' => '更新群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_delete', 'description' => '刪除群組文章/圖片/影片/直播'],

            ['type' => 'group', 'name' => 'meeting_create', 'description' => '新增會議'],
            ['type' => 'group', 'name' => 'meeting_update', 'description' => '更新會議'],
            ['type' => 'group', 'name' => 'meeting_delete', 'description' => '刪除會議'],

            ['type' => 'group', 'name' => 'member_create', 'description' => '新增群組會員'],
            ['type' => 'group', 'name' => 'member_update_make_qrcode', 'description' => '更新群組會員 QRCode'],
            ['type' => 'group', 'name' => 'member_update_recruiter',   'description' => '更新群組會員招募人'],

            ['type' => 'group', 'name' => 'report_read', 'description' => '觀看洞察報告'],

            ['type' => 'general', 'name' => 'comment_create', 'description' => '新增回應'],
            ['type' => 'general', 'name' => 'comment_update', 'description' => '更新回應'],
            ['type' => 'general', 'name' => 'comment_delete', 'description' => '刪除回應'],

        ]
    ],
    [
        'type' => 'group',
        'name' => 'editor',
        'description' => '群組小編',
        'permissions' => [
            ['type' => 'group', 'name' => 'publish_create', 'description' => '新增群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_update', 'description' => '更新群組文章/圖片/影片/直播'],
            ['type' => 'group', 'name' => 'publish_delete', 'description' => '刪除群組文章/圖片/影片/直播'],

            ['type' => 'group', 'name' => 'report_read', 'description' => '觀看洞察報告'],

            ['type' => 'general', 'name' => 'comment_create', 'description' => '新增回應'],
            ['type' => 'general', 'name' => 'comment_update', 'description' => '更新回應'],
            ['type' => 'general', 'name' => 'comment_delete', 'description' => '刪除回應']

        ]
    ],
    [
        'type' => 'group',
        'name' => 'member',
        'description' => '群組會員',
        'permissions' => [
            ['type' => 'general', 'name' => 'comment_create', 'description' => '新增回應'],
            ['type' => 'general', 'name' => 'comment_update', 'description' => '更新回應'],
            ['type' => 'general', 'name' => 'comment_delete', 'description' => '刪除回應']

        ]
    ]


];


return $roles;