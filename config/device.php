<?php
$device_settings = [

    'DEVICE_OS_IOS'     => 'ios',
    'DEVICE_OS_ANDRIOD' => 'andriod',
    'DEVICE_OS_MACOS'   => 'macos',
    'DEVICE_OS_WIN'     => 'win',
    'DEVICE_OS_OTHERS'  => 'others',

    'DEVICE_TYPE_PHONE'     => 'phone',
    'DEVICE_TYPE_PAD'       => 'pad',
    'DEVICE_TYPE_DESKTOP'   => 'desktop',
    'DEVICE_TYPE_NOTEBOOK'  => 'notebook',
    'DEVICE_TYPE_OTHERS'    => 'others',

    'DEVICE_LANG_TW' => 'zh-tw',
    'DEVICE_LANG_CN' => 'zh-cn',
    'DEVICE_LANG_EN' => 'en',
    'DEVICE_LANG_JP' => 'jp',

];


$device_settings['DEFAULT_DEVICE_OS'] = $device_settings['DEVICE_OS_OTHERS'];
$device_settings['DEVICE_OS'] = [
    $device_settings['DEVICE_OS_IOS'],
    $device_settings['DEVICE_OS_ANDRIOD'],
    $device_settings['DEVICE_OS_MACOS'],
    $device_settings['DEVICE_OS_WIN'],
    $device_settings['DEVICE_OS_OTHERS']
];

$device_settings['DEFAULT_DEVICE_TYPE'] = $device_settings['DEVICE_TYPE_OTHERS'];
$device_settings['DEVICE_TYPES'] = [
    $device_settings['DEVICE_TYPE_PHONE'],
    $device_settings['DEVICE_TYPE_PAD'],
    $device_settings['DEVICE_TYPE_DESKTOP'],
    $device_settings['DEVICE_TYPE_NOTEBOOK'],
    $device_settings['DEVICE_TYPE_OTHERS']
];

$device_settings['DEFAULT_DEVICE_LANG'] = $device_settings['DEVICE_LANG_EN'];
$device_settings['DEVICE_LANGS'] = [
    $device_settings['DEVICE_LANG_TW'],
    $device_settings['DEVICE_LANG_CN'],
    $device_settings['DEVICE_LANG_EN'],
    $device_settings['DEVICE_LANG_JP']
];


return $device_settings;
