<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'imagick',

    'sizes' => [
        'k'  => ['width' => 2048, 'height' => null],
        'h'  => ['width' => 1600, 'height' => null],
        'l'  => ['width' => 1024, 'height' => null],
        'c'  => ['width' => 800,  'height' => null],
        'z'  => ['width' => 640,  'height' => null],
        'n'  => ['width' => 320,  'height' => null],
        'q'  => ['width' => 150,  'height' => 150],
        'sq' => ['width' => 75,   'height' => 75]
    ],

];
