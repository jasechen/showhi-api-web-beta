<?php

$role_settings = [

    'ROLE_TYPE_GENERAL' => 'general',
    'ROLE_TYPE_GROUP'   => 'group',

    'PERMISSION_TYPE_GENERAL' => 'general',
    'PERMISSION_TYPE_GROUP'   => 'group',
    'PERMISSION_TYPE_ADMIN'   => 'admin',

];


$role_settings['DEFAULT_ROLE_TYPE'] = $role_settings['ROLE_TYPE_GENERAL'];
$role_settings['ROLE_TYPES'] = [
    $role_settings['ROLE_TYPE_GENERAL'],
    $role_settings['ROLE_TYPE_GROUP']
];

$role_settings['DEFAULT_PERMISSION_TYPE'] = $role_settings['PERMISSION_TYPE_GENERAL'];
$role_settings['PERMISSION_TYPES'] = [
    $role_settings['PERMISSION_TYPE_GENERAL'],
    $role_settings['PERMISSION_TYPE_GROUP'],
    $role_settings['PERMISSION_TYPE_ADMIN']
];


return $role_settings;