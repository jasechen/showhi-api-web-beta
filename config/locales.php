<?php

$locales_settings = [

    'LOCALES_LANG_EN' => 'en',
    'LOCALES_LANG_TW' => 'tw',
    'LOCALES_LANG_CN' => 'cn',

];

$locales_settings['DEFAULT_LOCALES_LANG'] = $locales_settings['LOCALES_LANG_EN'];

$locales_settings['CURRENT_LOCALES_LANGS'] = [
    $locales_settings['LOCALES_LANG_EN'],
    $locales_settings['LOCALES_LANG_TW'],
    $locales_settings['LOCALES_LANG_CN']
];


return $locales_settings;
