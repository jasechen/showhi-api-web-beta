<?php

$models = [
    'blog',
    'board',
    'countryProp',
    'device',
    'directory',
    'file',
    'groups',
    'groupUsers',
    'localesContent',
    'locales',
    'loginLog',
    'notices',
    'permission',
    'qrcodes',
    'rolePermission',
    'role',
    'session',
    'text',
    'userCheck',
    'userEducationMajor',
    'userEducation',
    'userExperience',
    'userProfile',
    'userRecruiter',
    'user',
    'userRole'
];


return $models;
