<?php
$qrcodes_settings['QRCODES_BASE_URL'] = 'https://showhi.co/register/';
$qrcodes_settings['QRCODES_ENCODING'] = 'UTF-8';
$qrcodes_settings['QRCODES_SIZE'] = '320';
$qrcodes_settings['QRCODES_SML_SIZE'] = '75';
$qrcodes_settings['QRCODES_SIZE_NAME_ARR'] = ['n','sq'];
$qrcodes_settings['QRCODES_FILE_PATH'] = '/qrcode/';
$qrcodes_settings['EXPIRETIME'] = 86400;

return $qrcodes_settings;